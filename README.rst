======
Sanico
======

Sanico from Sanidentalgroup project

Instalation
===========

Requeriments:

* Django 1.6 or later
* Mezzanine 3.1.5 or later
* MySQL as database
* Virtuelenv for python

First you need create a new virtualenv ``virtualenv --distribute venv``
and use it ``source venv/bin/activate``.

Create and configure a file named ``local_settings.py`` insede of config folder,
and add the configuration of the database, like said in the
`documentation <https://docs.djangoproject.com/en/dev/ref/databases/>`_. Example:

.. code-block:: python

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'sanico',
            'USER': 'user',
            'PASSWORD': 'password',
            'HOST': 'localhost',
            'PORT': '3306',
        }
    }

Run the command for create the database: ``python manage.py createdb``,
all tables will be created, and will ask if you want to add a root account,
configure a superuser account and continue, don't accept when the wirzard ask about
install all the templates pages.

Run the commant ``python manage.py runserver_plus`` to run the local project at port
8000 in debug mode.

Indexation of sanico structure
==============================

Sanico main menu:

* `Home page <docs/home.rst>`_
* `About Us <docs/about-us.rst>`_
* `Destinations <docs/destinations.rst>`_
* `Medical Procedures <docs/medical-procedures.rst>`_
* `Blog <docs/blog.rst>`_
* `FAQs <docs/faqs.rst>`_
* `Contact Us <docs/contact-us.rst>`_

