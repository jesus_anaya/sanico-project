from django.conf.urls import patterns, url, include
from rest_framework import routers

from .rest_views import (ClinicProcedureViewSet, ProcedureViewSet,
                        ClinicRelationTreatmentViewSet)

router = routers.SimpleRouter(trailing_slash=False)
router.register(r'clinic-procedures', ClinicProcedureViewSet)
router.register(r'procedures', ProcedureViewSet)
router.register(r'treatment-categories', ClinicRelationTreatmentViewSet)

urlpatterns = patterns(
    "api.views",
    url(r"^get_cities/$", 'get_cities', name='get_cities'),
    url(r"^get_looking_for/$", 'get_looking_for', name='get_looking_for'),
    url(r"^request_password/$", 'request_password', name='request_password'),
    url(r'^', include(router.urls))
)