from rest_framework import viewsets
from profile.models import (ClinicProcedure, Procedure,
                            ClinicRelationTreatment)

from .serializers import (ClinicProcedureSerializer, ProcedureSerializer,
                            ClinicRelationTreatmentSerializer)

from .helpers import add_clinic_to_request


class CollBaseViewSet(viewsets.ModelViewSet):
    """
    """
    def create(self, request, *args, **kwargs):
        request = add_clinic_to_request(request)
        return super(CollBaseViewSet, self).create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        request = add_clinic_to_request(request)
        return super(CollBaseViewSet, self).update(request, *args, **kwargs)


class ClinicProcedureViewSet(CollBaseViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """

    serializer_class = ClinicProcedureSerializer
    queryset = ClinicProcedure.objects.all()

    def get_queryset(self):
        """
        This view should return a list of all the purchases
        for the currently authenticated user.
        """

        clinic_id = self.request.session.get('clinic')
        return ClinicProcedure.objects.filter(clinic=clinic_id)


class ProcedureViewSet(CollBaseViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """

    serializer_class = ProcedureSerializer
    queryset = Procedure.objects.all()


class ClinicRelationTreatmentViewSet(CollBaseViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """

    serializer_class = ClinicRelationTreatmentSerializer
    queryset = ClinicRelationTreatment.objects.all()

    def get_queryset(self):
        """
        Filter treatment categories by clinic
        """

        clinic_id = self.request.session.get('clinic')
        procedures = ClinicProcedure.objects.filter(clinic=clinic_id)
        ids = list(set([x.procedure.category.id for x in procedures]))

        return self.queryset.filter(clinic=clinic_id, category__in=ids)
