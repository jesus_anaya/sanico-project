# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PasswordCode'
        db.create_table(u'api_passwordcode', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('enable', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'api', ['PasswordCode'])


    def backwards(self, orm):
        # Deleting model 'PasswordCode'
        db.delete_table(u'api_passwordcode')


    models = {
        u'api.passwordcode': {
            'Meta': {'object_name': 'PasswordCode'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'enable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['api']