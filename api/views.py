from django.utils import simplejson as json
from django.http import HttpResponse
from profile.models import Clinic, ClinicCategory, City, Procedure, ClinicTag
from sanico.models import SanicoUser
from .models import PasswordCode
from .helpers import get_code


def get_cities(request):
    if request.GET.get('term'):
        cities = City.objects.filter(
            name__icontains=request.GET.get('term'))
    else:
        cities = City.objects.all()
    cities = map(lambda x: {'value': unicode(x.name)}, cities)[:12]

    return HttpResponse(
        json.dumps({'results': cities}), 'application/json')


def get_looking_for(request):
    q = request.GET.get('term', '')

    categories = ClinicCategory.objects.filter(
        name__icontains=q).values_list('name', flat=True).distinct()

    tags = ClinicTag.objects.filter(
        name__icontains=q).values_list('name', flat=True).distinct()

    clinics = Clinic.objects.filter(
        name__icontains=q).values_list('name', flat=True).distinct()

    procedures = Procedure.objects.filter(
        name__icontains=q).values_list('name', flat=True).distinct()

    results = list(clinics) + list(categories) + list(procedures) + list(tags)

    data = json.dumps({'results': [{'value': x} for x in results[:10]]})
    return HttpResponse(data, 'application/json')


def request_password(request, email):
    result = False
    try:
        user = SanicoUser.objects.get(email=email.lower())
        if user.is_active:
            pass_code = PasswordCode()
            pass_code.code = get_code()
            pass_code.save()
            result = True
    except SanicoUser.DoesNotExist:
        print "Error getting user by email %s" % email
    return HttpResponse(json.dumps({'success': result}), 'application/json')
