from hashlib import sha1
from random import randrange


def generate_random():
    string = ""
    for i in range(10):
        irand = randrange(0, 40)
        string += chr(80 + irand)
    print 'random: ', string
    return string


def get_code():
    return sha1(generate_random().encode('utf-8')).hexdigest()


def add_clinic_to_request(request):
    try:
        request.DATA = request.DATA.dict()
    except AttributeError:
        pass
    request.DATA['clinic'] = request.session.get('clinic')
    return request
