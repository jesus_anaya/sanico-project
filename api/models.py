from django.db import models


class PasswordCode(models.Model):
    code = models.CharField(max_length=60)
    enable = models.BooleanField(default=True)

    def __unicode__(self):
        return unicode(self.code)
