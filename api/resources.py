from rest_framework.resources import ModelResource
from profile.models import Clinic


class ClinicResource(ModelResource):
    """Resource for model PointOfSale
    """
    model = Clinic

