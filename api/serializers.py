from rest_framework import serializers
from profile.models import (ClinicProcedure, Procedure, ClinicRelationTreatment,
                            TreatmentCategory)


class TreatmentCategorySerializer(serializers.ModelSerializer):
    """
    """
    class Meta:
        model = TreatmentCategory
        fields = ('id', 'name')


class ProcedureSerializer(serializers.ModelSerializer):
    """
    """
    category = TreatmentCategorySerializer()

    class Meta:
        model = Procedure
        fields = ('id', 'name', 'category')



class ClinicProcedureField(serializers.WritableField):
    def to_native(self, value):
        return ProcedureSerializer(value).data

    def from_native(self, data):
        try:
            return Procedure.objects.get(id=data)
        except:
            return ProcedureSerializer(data).data


class ClinicProcedureSerializer(serializers.ModelSerializer):
    """
    """
    procedure = ClinicProcedureField()
    clinic = serializers.PrimaryKeyRelatedField(required=False)

    class Meta:
        model = ClinicProcedure
        fields = ('clinic', 'description', 'procedure',
                'id', 'price', 'duration', 'position')


class ClinicRelationTreatmentSerializer(serializers.ModelSerializer):
    """
    """
    category = TreatmentCategorySerializer()

    class Meta:
        model = ClinicRelationTreatment
        fields = ('id', 'category', 'position')
