from copy import deepcopy

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from mezzanine.conf import settings
from mezzanine.core.admin import DisplayableAdmin, OwnableAdmin
from podcast.models import Podcast

podcast_fieldsets = deepcopy(DisplayableAdmin.fieldsets)
podcast_fieldsets[0][1]["fields"].insert(1, "categories")
podcast_fieldsets[0][1]["fields"].extend(["content", "allow_comments"])
podcast_list_display = ["front_image", "title", "user", "status", "admin_link"]

podcast_fieldsets = list(podcast_fieldsets)
podcast_fieldsets.insert(1, (_("Other podcasts"), {
    "classes": ("collapse-closed",),
    "fields": ("related_posts",)}))

podcast_fieldsets[0][1]["fields"].insert(-2, "video")


class PodcastPostAdmin(DisplayableAdmin, OwnableAdmin):
    """
    Admin class for blog posts.
    """

    fieldsets = podcast_fieldsets
    list_display = podcast_list_display
    filter_horizontal = ("categories", "related_posts",)

    def front_image(self, obj):
        return '<img src="%s" width="80" />' % obj.get_thumbnail()
    front_image.allow_tags = True


admin.site.register(Podcast, PodcastPostAdmin)
