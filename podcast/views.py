######### PODCAST ###############
from calendar import month_name
from collections import defaultdict

from django.shortcuts import get_object_or_404
from mezzanine.conf import settings
from mezzanine.utils.views import render, paginate
from mezzanine.generic.models import AssignedKeyword, Keyword
from mezzanine.blog.models import BlogCategory
from mezzanine.utils.models import get_user_model
from django.contrib.contenttypes.models import ContentType
from podcast.models import Podcast
from django import VERSION

User = get_user_model()


def podcast_list(request, tag=None, year=None, month=None, username=None,
                   category=None, template="podcast/podcast_list.html"):
    """
    Display a list of blog posts that are filtered by tag, year, month,
    author or category. Custom templates are checked for using the name
    ``blog/blog_post_list_XXX.html`` where ``XXX`` is either the
    category slug or author's username if given.
    """

    settings.use_editable()
    templates = []
    podcasts = Podcast.objects.published(for_user=request.user)

    if tag is not None:
        tag = get_object_or_404(Keyword, slug=tag)
        podcasts = podcasts.filter(keywords__in=tag.assignments.all())

    if year is not None:
        podcasts = podcasts.filter(publish_date__year=year)
        if month is not None:
            podcasts = podcasts.filter(publish_date__month=month)
            month = month_name[int(month)]

    if category is not None:
        category = get_object_or_404(BlogCategory, slug=category)
        podcasts = podcasts.filter(categories=category)
        templates.append(u"podcast/podcast_list_%s.html" %
                          unicode(category.slug))
    author = None
    if username is not None:
        author = get_object_or_404(User, username=username)
        podcasts = podcasts.filter(user=author)
        templates.append(u"podcast/podcast_list_%s.html" % username)

    # We want to iterate keywords and categories for each blog post
    # without triggering "num posts x 2" queries.
    #
    # For Django 1.3 we create dicts mapping blog post IDs to lists of
    # categories and keywords, and assign these to attributes on each
    # blog post. The Blog model then uses accessor methods to retrieve
    # these attributes when assigned, which will fall back to the real
    # related managers for Django 1.4 and higher, which will already
    # have their data retrieved via prefetch_related.

    podcasts = podcasts.select_related("user")
    if VERSION >= (1, 4):
        podcasts = podcasts.prefetch_related("categories",
                                                "keywords__keyword")
    else:
        categories = defaultdict(list)
        if podcasts:
            ids = ",".join([str(p.id) for p in podcasts])
            for cat in BlogCategory.objects.raw(
                "SELECT * FROM blog_blogcategory "
                "JOIN blog_blogpost_categories "
                "ON blog_blogcategory.id = blogcategory_id "
                "WHERE blogpost_id IN (%s)" % ids):
                categories[cat.blogpost_id].append(cat)

        keywords = defaultdict(list)
        blogpost_type = ContentType.objects.get(app_label="blog",
                                                model="blogpost")
        assigned = AssignedKeyword.objects.filter(blogpost__in=podcasts,
                        content_type=blogpost_type).select_related("keyword")
        for a in assigned:
            keywords[a.object_pk].append(a.keyword)
        for i, podcast in enumerate(podcasts):
            setattr(podcasts[i], "_categories", categories[podcast.id])
            setattr(podcasts[i], "_keywords", keywords[podcast.id])

    podcasts = paginate(podcasts, request.GET.get("page", 1),
                          settings.BLOG_POST_PER_PAGE,
                          settings.MAX_PAGING_LINKS)
    context = {"podcasts": podcasts, "year": year, "month": month,
               "tag": tag, "category": category, "author": author}
    templates.append(template)
    return render(request, templates, context)


def podcast_detail(request, slug, year=None, month=None, day=None,
                     template="podcast/podcast_detail.html"):

    podcasts = Podcast.objects.published(
        for_user=request.user).select_related()

    podcast = get_object_or_404(podcasts, slug=slug)
    context = {"podcast": podcast, "editable_obj": podcast}
    templates = [u"blog/podcast_detail_%s.html" % unicode(slug), template]
    return render(request, templates, context)
