from django.conf.urls import patterns, url
#from django.views.generic.simple import redirect_to

urlpatterns = patterns("",

    #### Podcast links ###
    url(r"^$", 'podcast.views.podcast_list', name='podcast_list'),
    url(r"^category/(?P<category>.*)/$", "podcast.views.podcast_list", name="podcast_list_category"),
    url(r"^tag/(?P<tag>.*)/$", "podcast.views.podcast_list", name="podcast_list_tag"),
    url(r"^(?P<slug>.*)/$", 'podcast.views.podcast_detail', name='podcast_detail'),
)
