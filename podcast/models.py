from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse_lazy

from mezzanine.core.models import Displayable, Ownable, RichText
from mezzanine.generic.fields import CommentsField, RatingField
from mezzanine.utils.models import AdminThumbMixin
from mezzanine.blog.models import BlogCategory
from embed_video.fields import EmbedVideoField
from embed_video.backends import detect_backend


class Podcast(Displayable, Ownable, RichText, AdminThumbMixin):
    """
    A Podcast.
    """
    class Meta:
        verbose_name = _("Podcast")
        verbose_name_plural = _("Podcasts")
        ordering = ("-publish_date",)

    categories = models.ManyToManyField(BlogCategory,
        verbose_name=_("Categories"),
        blank=True, related_name="podcasts")

    allow_comments = models.BooleanField(verbose_name=_("Allow comments"),
        default=True)

    comments = CommentsField(verbose_name=_("Comments"))
    rating = RatingField(verbose_name=_("Rating"))

    related_posts = models.ManyToManyField("Podcast",
        verbose_name=_("Related podcast"), blank=True)

    # Youtuve data
    video = EmbedVideoField(verbose_name=_("Youtube Link"), null=True)

    def get_absolute_url(self):
        return reverse_lazy("podcast_detail", args=(self.slug,))

    def category_list(self):
        """
        These methods are wrappers for keyword and category access.
        For Django 1.3, we manually assign keywords and categories
        in the blog_post_list view, since we can't use Django 1.4's
        prefetch_related method. Once we drop support for Django 1.3,
        these can probably be removed.
        """
        return getattr(self, "_categories", self.categories.all())

    def keyword_list(self):
        try:
            return self._keywords
        except AttributeError:
            keywords = [k.keyword for k in self.keywords.all()]
            setattr(self, "_keywords", keywords)
            return self._keywords

    def get_thumbnail(self):
        if self.video:
            video = detect_backend(self.video)
            return video.get_thumbnail_url()
        return ""
