# Include project.txt
-r project.txt

# Packages list
gunicorn>=0.18.0
gevent>=0.13.8
boto>=2.13.3
django-storages>=1.1.8
python-memcached==1.31

# disable Newrellic Temporally
# newrelic
