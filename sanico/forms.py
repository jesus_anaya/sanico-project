# -*- encoding: utf-8 -*-
from django import forms
from django.conf import settings
from .models import Contest, Sponsor
from profile.models import City


class TinyMceWidget(forms.Textarea):
    """ This allows us to use our own TinyMCE setup script. """

    class Media:
        js = (
            "%ssanico/tiny_mce/tiny_mce.js" % settings.STATIC_URL,
            "%ssanico/js/tinyconfigure.js" % settings.STATIC_URL,
        )

    def __init__(self, *args, **kwargs):
        super(TinyMceWidget, self).__init__(*args, **kwargs)
        self.attrs["class"] = "mceEditor"


from django.contrib.auth.forms import UserCreationForm, UserChangeForm, AdminPasswordChangeForm
from django.contrib.admin.widgets import FilteredSelectMultiple


class SanicoUserChangeForm(UserChangeForm):
    telephone = forms.CharField(required=False)


class SanicoUserCreationForm(UserCreationForm):
    telephone = forms.CharField(required=False)


class SanicoAdminPasswordChangeForm(AdminPasswordChangeForm):
    pass


class ContestForm(forms.ModelForm):
    class Meta:
        model = Contest


class SponsorForm(forms.ModelForm):
    class Meta:
        model = Sponsor

    cities = forms.ModelMultipleChoiceField(
        queryset=City.objects.all(),
        widget=FilteredSelectMultiple("Cities", False))


class LandingPageForm(forms.Form):
    page = forms.CharField()
    name = forms.CharField()
    email = forms.EmailField()
    gender = forms.CharField(required=False)
    date_of_birth = forms.DateField(required=False)
    telephone = forms.CharField(required=False)
    search_engine = forms.CharField(required=False)
    message = forms.CharField()


class FeedbackForm(forms.Form):
    first_name = forms.CharField()
    last_name = forms.CharField()
    email = forms.EmailField()
    features = forms.IntegerField()
    usability = forms.IntegerField()
    design = forms.IntegerField()
    message = forms.CharField(required=False)
