from django import template
from django.contrib.sites.models import Site
from django.conf import settings
from mezzanine.blog.models import BlogPost
from podcast.models import Podcast
from profile.models import Clinic, ClinicPhoto, Review
from sanico.models import UserProfile, Sponsor
from hashlib import md5
register = template.Library()


@register.inclusion_tag("blog/slidecontainer.html")
def related_posts(blog_post):
    keword_names = map(lambda x: x.keyword, blog_post.keywords.all())

    related_posts = BlogPost.objects.published().exclude(
        id=blog_post.id).filter(keywords__keyword__in=keword_names).distinct()

    keywords = lambda post: set(map(lambda x: x.keyword, post.keywords.all()))
    num_common = lambda p: len(keywords(p) & set(keword_names))

    try:
        related_post = sorted(related_posts, key=num_common)[0]
    except IndexError:
        related_post = None

    return {
        'MEDIA_URL': settings.MEDIA_URL,
        'post': related_post
    }


@register.inclusion_tag("tags/render_sociallike.html")
def render_sociallike(post):
    site = 'http://' + str(Site.objects.get_current())
    post_url = site + str(post.get_absolute_url())
    return {'post_url': post_url}


@register.inclusion_tag('tags/render_socialshare.html')
def render_socialshare(post):
    site = 'http://' + str(Site.objects.get_current())
    post_url = site + post.get_absolute_url()
    return {'post_url': post_url}


@register.inclusion_tag('tags/result_clinic_info.html')
def render_clinic_info(clinic):
    testimonial_name, testimonial = clinic.get_testimonial()
    return {
        'MEDIA_URL': settings.MEDIA_URL,
        'clinic': clinic,
        'testimonial_name': testimonial_name,
        'testimonial': testimonial,
        'num_photos': ClinicPhoto.objects.filter(clinic=clinic.id).count(),
        'num_reviews': Review.objects.filter(clinic=clinic.id, enable=True).count()
    }


@register.simple_tag
def sanico_gravatar_url(email, size=32):
    """
Return the full URL for a Gravatar given an email hash.
"""
    email_hash = md5(email.lower()).hexdigest()
    return "http://www.gravatar.com/avatar/%s?s=%s&d=identicon&r=PG" % (email_hash, size)


@register.inclusion_tag('tags/render_profile.html')
def render_profile(blog_post):
    try:
        user = blog_post.user
        profile = UserProfile.objects.get(user__id=user.id)
    except KeyError, UserProfile.ObjectDoesNotExist:
        profile = None
    return {
        'MEDIA_URL': settings.MEDIA_URL,
        'profile': profile,
        'blog_post': blog_post
    }


@register.inclusion_tag('tags/home_recent_posts.html')
def render_recent_posts():
    return {
        'MEDIA_URL': settings.MEDIA_URL,
        'recent_posts': BlogPost.objects.all()[:5]
    }


@register.inclusion_tag('tags/home_podcast.html')
def render_podcasts():
    podcasts = []
    main_podcast = None
    posdcast_num = Podcast.objects.count()

    if posdcast_num > 0:
        if posdcast_num > 1:
            podcasts = Podcast.objects.all()[1:4]
        main_podcast = Podcast.objects.all()[0]

    return {
        'MEDIA_URL': settings.MEDIA_URL,
        'podcasts': podcasts,
        'main_podcast': main_podcast
    }


@register.inclusion_tag('tags/clinics.html')
def render_clinics():
    clinics = Clinic.objects.filter(enable=True)[:3]

    return {
        'MEDIA_URL': settings.MEDIA_URL,
        'clinics': clinics
    }


@register.inclusion_tag('tags/recently_added.html')
def recently_added():
    clinics = Clinic.objects.filter(enable=True).order_by('-created')[:3]

    return {
        'MEDIA_URL': settings.MEDIA_URL,
        'clinics': clinics
    }


@register.inclusion_tag('tags/sponsors_home.html')
def render_sponsors():
    sponsors = Sponsor.objects.all()
    return {
        'MEDIA_URL': settings.MEDIA_URL,
        'sponsors': sponsors
    }


@register.inclusion_tag('tags/sponsors_profile.html')
def render_sponsors_profile(clinic):
    sponsors = Sponsor.objects.filter(cities__name__iexact=clinic.city)
    return {
        'MEDIA_URL': settings.MEDIA_URL,
        'sponsors': sponsors
    }
