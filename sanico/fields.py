from django.db.models import ImageField
from .helpers import post_upload_to


class PostImageField(ImageField):

    def __init__(self, *args, **kwargs):
        kwargs['upload_to'] = post_upload_to(kwargs.get('upload_to'))
        return super(PostImageField, self).__init__(*args, **kwargs)
