from django.utils.translation import ugettext_lazy as _
from django.contrib.sites.models import Site
from widget import widget_pool
from widget.models import WidgetClassBase
from mezzanine.blog.models import BlogPost


class SocialWidget(WidgetClassBase):
    """
    Social Widget
    """

    template = "widgets/social_widget.html"

    def render(self, context, slot, queryset, **kwargs):
        return context

    class Meta:
        name = _("Social Widget")
        author = 'Jesus Anaya'


class SimpleSearch(WidgetClassBase):
    """
    Simple Search Widget
    """

    template = "widgets/simple_search.html"

    def render(self, context, slot, queryset, **kwargs):
        context['site'] = Site.objects.get_current()
        return context

    class Meta:
        name = _("Simple Search")
        author = 'Jesus Anaya'


class SearchPodcast(WidgetClassBase):
    """
    Simple Search Widget
    """

    template = "widgets/search_podcast.html"

    def render(self, context, slot, queryset, **kwargs):
        context['site'] = Site.objects.get_current()
        return context

    class Meta:
        name = _("Search Podcast")
        author = 'Jesus Anaya'


class RecentPostsWidget(WidgetClassBase):
    """
    Recent Posts Widget
    """

    template = "widgets/recent_posts.html"

    def render(self, context, slot, queryset, **kwargs):
        context['posts'] = BlogPost.objects.all()[:5]
        return context

    class Meta:
        name = _("Recent Posts")
        author = 'Jesus Anaya'


class SectionsWidget(WidgetClassBase):
    """
    SectionsWidget Widget
    """

    template = "widgets/sections_widget.html"

    def render(self, context, slot, queryset, **kwargs):
        return context

    class Meta:
        name = _("Sections Widget")
        author = 'Jesus Anaya'


class Quote(WidgetClassBase):
    """
    Quote Widget
    """

    template = "widgets/quote.html"

    def render(self, context, slot, queryset, **kwargs):
        return context

    class Meta:
        name = _("Quote")
        author = 'Jesus Anaya'


class Newsletter(WidgetClassBase):
    """
    Newsletter Widget
    """

    template = "widgets/newsletter.html"

    def render(self, context, slot, queryset, **kwargs):
        return context

    class Meta:
        name = _("Newsletter")
        author = 'Jesus Anaya'


# add widgets
widget_pool.register_widget(SimpleSearch)
widget_pool.register_widget(RecentPostsWidget)
widget_pool.register_widget(SectionsWidget)
widget_pool.register_widget(Quote)
widget_pool.register_widget(Newsletter)
widget_pool.register_widget(SearchPodcast)
widget_pool.register_widget(SocialWidget)
