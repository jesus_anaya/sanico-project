from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.template import RequestContext
#from django.views.decorators.csrf import csrf_protect
from django.core.context_processors import csrf
from django.shortcuts import render_to_response
from django.utils import simplejson as json
from django.conf import settings
from django.contrib import admin
from django.db.models import get_model
from django.utils.translation import ugettext_lazy as _
from django.contrib.admin.options import ModelAdmin
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.core.urlresolvers import reverse
from mezzanine.core.forms import get_edit_form
from mezzanine.pages.views import page as mezzanine_page
from mezzanine.pages.models import Page
from mezzanine.utils.views import is_editable
from mezzanine.utils.sites import has_site_permission
#from profile.models import Clinic
from profile.search import search_results, get_cities
from datetime import datetime
from .forms import ContestForm, LandingPageForm, FeedbackForm
from .helpers import validate_form, staff_required
from .models import Newsletter
from .email import SendEmail
from .feeds import SanicoRSS, SanicoAtom
import csv


def page_context(request, slug):
    try:
        page = Page.objects.with_ascendants_for_slug(slug)[0]
    except:
        raise Http404

    extra_context = RequestContext(request)
    extra_context["page"] = page
    extra_context.update(csrf(request))

    return extra_context


def home(request):
    context = page_context(request, "home")
    context['VIDEO_HOME_AUTOPLAY'] = settings.VIDEO_HOME_AUTOPLAY
    return mezzanine_page(
        request, slug="home", extra_context=context)


@login_required
def edit(request):
    """
    Process the inline editing form.
    """
    model = get_model(request.POST["app"], request.POST["model"])
    obj = model.objects.get(id=request.POST["id"])
    form = get_edit_form(obj, request.POST["fields"], data=request.POST,
                         files=request.FILES)
    if not (is_editable(obj, request) and has_site_permission(request.user)):
        response = _("Permission denied")
    elif form.is_valid():
        form.save()
        model_admin = ModelAdmin(model, admin.site)
        message = model_admin.construct_change_message(request, form, None)
        model_admin.log_change(request, obj, message)
        response = ""
    else:
        response = form.errors.values()[0][0]
    return HttpResponse(unicode(response))


def results(request):
    extra_context = page_context(request, "results")
    context = {"clinics": None}

    if request.method == 'GET':
        where = request.GET.get('where')
        search = request.GET.get('search')

        request.session['search'] = search
        request.session['where'] = where

        if request.GET.get('manual_selector'):
            request.session['cities_list'] = get_cities(search)
            return HttpResponseRedirect('/cities/')

        if not search:
            clinics = []
        else:
            clinics = search_results(search, where)
        context["clinics"] = clinics

    return render_to_response("pages/results.html", context, context_instance=extra_context)


def renew_password(request):
    return mezzanine_page(
        request, slug="renew_password",
        extra_context=page_context(request, "renew_password"))


def process_logout(request):
    request.session['clinic'] = None
    logout(request)
    return HttpResponseRedirect(
        reverse("page", kwargs={"slug": 'login'}))


def process_login(request):
    if request.method == "POST":
        user = authenticate(
            username=request.POST.get('username'),
            password=request.POST.get('password'))
        if user is not None:
            # the password verified for the user
            if user.is_active:
                print("User is valid, active and authenticated")
                # if Remember me checkbox is not actived,
                # expire session with close browser
                if not request.POST.get('rememberme', None):
                    request.session.set_expiry(0)
                # login valid user
                login(request, user)

                #redirect to before tryed link
                if request.POST.get('next'):
                    return HttpResponseRedirect(request.POST.get('next'))

                # go to collaborator page
                return HttpResponseRedirect(reverse("collaborator:general"))
            else:
                print("The password is valid, but the account has been disabled!")
        else:
            # the authentication system was unable to verify the username and password
            print("The username and password were incorrect.")

    return HttpResponseRedirect(reverse("page", kwargs={"slug": 'login'}) + '?error=1')


def newsletter(request):
    success = False
    if request.method == 'GET':
        name = request.GET.get('name')
        mail = request.GET.get('email')
        user = Newsletter.objects.get_or_create(name=name, email=mail)
        if user:
            # send main to marketing@sanimedicaltourism.com
            email = SendEmail()
            email.notify_message(
                'Sanico - Newsletter added',
                "emails/sanico-newsletter-form.html", {
                    'name': name,
                    'email': mail
                })

            #send Newsletter message
            success = email.send()
        else:
            print "[Sanico]: Error saving Newsletter"

    return HttpResponse(json.dumps({'success': success}), mimetype="application/json")


def add_clinic(request):
    success = False
    if request.method == 'POST' and request.is_ajax():
        email_cotext = {
            'clinic_country': request.POST.get('clinic_country'),
            'contact_last_name': request.POST.get('contact_last_name'),
            'clinic_website': request.POST.get('clinic_website'),
            'clinic_name': request.POST.get('clinic_name'),
            'contact_comment': request.POST.get('contact_comment'),
            'contact_email': request.POST.get('contact_email'),
            'clinic_email': request.POST.get('clinic_email'),
            'clinic_desire_appointment': request.POST.get('clinic_desire_appointment'),
            'contact_phone': request.POST.get('contact_phone'),
            'clinic_phone': request.POST.get('clinic_phone'),
            'contact_first_name': request.POST.get('contact_first_name')
        }

        try:
            #Notify message
            email = SendEmail()
            email.notify_message(
                'New Clinic Added',
                "emails/response_add_clinic_form.html",
                email_cotext)

            #send user response
            email.to_user_message(
                'Thank you for submitting your information!',
                "emails/doctor-clinica.html",
                request.POST.get('contact_email'))

            #send admin notification
            success = email.send()
        except:
            pass

    return HttpResponse(json.dumps({'success': success}), mimetype="application/json")


def contest_contact(request):
    success = False
    if request.method == 'POST' and request.is_ajax():
        contest = ContestForm(request.POST, request.FILES)

        if contest.is_valid():
            contest_obj = contest.save()
            email_cotext = {'contest': contest_obj}

            try:
                #Notify message
                email = SendEmail()

                # set recipe emails
                email.set_recipes(
                    settings.EMAIL_COMMUNITY_MANAGER,
                    settings.EMAI_CONTACT_COPY + settings.EMAI_CONTACT)

                email.notify_message(
                    'Contest Information Send!',
                    "emails/contest_information.html",
                    email_cotext)

                #send user response
                email.to_user_message(
                    'Thank you for sharing your story!',
                    "emails/contest_thanks.html",
                    contest_obj.email)

                #send admin notification
                success = email.send()
            except:
                pass

    return HttpResponse(json.dumps({'success': success}), mimetype="application/json")


def landing_page_form(request):
    return validate_form(
        request, LandingPageForm,
        #Sanico email format
        {'subject': 'Landing Information Send!',
            'template': 'landing_page.html'},
        #Client email format
        {'subject': 'Thanks for sending us your information!',
            'template': 'clinic-contact-freequote.html'})


def feedback_form(request):
    return validate_form(
        request, FeedbackForm,
        #Sanico email format
        {'subject': 'Feedback Information Send!',
            'template': 'sanico-email-feedback.html'})


def about_form(request):
    return validate_form(
        request, LandingPageForm,
        #Sanico email format
        {'subject': 'About Information Send!',
            'template': 'sanico-about-form.html'},
        #Client email format
        {'subject': 'Thanks for sending us your information!',
            'template': 'generic-email-about-us.html'})


@staff_required(login_url='/login/')
def export_newsletters(request):
    response = HttpResponse(mimetype='text/csv')
    date_now = datetime.now().strftime("%Y/%m/%d-%H:%M:%S")
    response['Content-Disposition'] = 'attachment;filename="newsletter-%s.csv"' % date_now
    writer = csv.writer(response, dialect='excel')

    for newsletter in Newsletter.objects.all().values('email'):
        writer.writerow([unicode(newsletter['email'])])

    return response


def sanico_feed(request, format, **kwargs):
    """
    Blog posts feeds - maps format to the correct feed view.
    """
    try:
        return {'rss': SanicoRSS, 'atom': SanicoAtom}[format](**kwargs)(request)
    except KeyError:
        raise Http404()


def robots(request):
    return render_to_response('robots.txt', mimetype="text/plain")


def humans(request):
    return render_to_response('humans.txt', mimetype="text/plain")


def bing_site_auth(request):
    return render_to_response('BingSiteAuth.xml', mimetype="text/xml")
