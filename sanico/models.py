# -*- encoding: utf-8 -*-
from django.db import models
from mezzanine.pages.models import Page
from mezzanine.utils.urls import slugify
from filebrowser_safe.fields import FileBrowseField
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from datetime import datetime
from .helpers import upload_to, sponsor_upload_to


class SanicoUser(User):
    telephone = models.CharField(max_length=25, blank=True, default='')

    def save(self, *args, **kwargs):
        if self.email:
            self.email = self.email.lower()
        super(SanicoUser, self).save(*args, **kwargs)


class Newsletter(models.Model):
    class Meta:
        verbose_name = _("Newsletter")
        verbose_name_plural = _("Newsletter")
        ordering = ["-register_date"]

    name = models.CharField(max_length=100)
    email = models.EmailField()
    register_date = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return unicode("%s - %s" % (self.name, self.email))


#profile class
class UserProfile(models.Model):
    user = models.OneToOneField(User, unique=True)
    image = FileBrowseField(
        "Image", max_length=200, directory="profiles/",
        extensions=[".jpg", ".jpeg", ".png", ".bmp", ".gif"], blank=True, null=True)
    website = models.URLField(blank=True, null=True)
    facebook = models.URLField(blank=True, null=True)
    twitter = models.URLField(blank=True, null=True)
    google_plus = models.URLField(blank=True, null=True)
    bio = models.TextField(blank=True, null=True, verbose_name=_("Biographical Info"))

    def __unicode__(self):
        return unicode("Profile of %s" % self.user.get_full_name())


class Sponsor(models.Model):
    class Meta:
        ordering = ['-extra_position', '-created']

    cities = models.ManyToManyField("profile.City")
    enable = models.BooleanField(default=True)
    name = models.CharField(max_length=100)
    image = models.ImageField(upload_to=sponsor_upload_to("uploads/champagne/"))
    extra_position = models.IntegerField(default=0, blank=True)
    url = models.URLField()
    created = models.DateTimeField(auto_now_add=True, default=datetime.now())

    def __unicode__(self):
        return unicode("%s" % self.name)


class Country(models.Model):
    name = models.CharField(max_length=100)
    shortcut = models.CharField(max_length=10)


class Contest(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField()
    phone = models.CharField(max_length=25)
    birth_date = models.DateField()
    city = models.CharField(max_length=40)
    treatment = models.CharField(max_length=60)
    photo = models.ImageField(upload_to=upload_to("uploads/contest/"))
    story = models.TextField()

    def __unicode__(self):
        return unicode("%s %s" % (self.first_name, self.last_name))


### PAGES ###


class BlankPage(Page):
    class Meta:
        verbose_name = "Blank Page"
        verbose_name_plural = "Blank Pages"

    static_slug = models.CharField(max_length=100, blank=True, null=True)

    def save(self, *args, **kwargs):
        if self.static_slug:
            self.static_slug = slugify(self.static_slug)
            self.slug = self.static_slug
        super(BlankPage, self).save(*args, **kwargs)

    def get_slug(self):
        """
        Recursively build the slug from the chain of parents.
        """
        if self.static_slug and self.static_slug != "":
            slug = self.static_slug
        else:
            slug = slugify(self.title)

        if self.parent is not None:
            return "%s/%s" % (self.parent.slug, slug)
        return slug


class WaitContacEmail(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField()
    datetime = models.DateTimeField(auto_now_add=True)
    send = models.BooleanField(default=False)

    def __unicode__(self):
        return unicode("%s - %s" % (self.name, self.email))
