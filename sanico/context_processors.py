from sanico.models import UserProfile
from django.contrib.sites.models import Site


def user_profile(request):
    try:
        profile = UserProfile.objects.get(user__id=request.user.id)
    except:
        profile = None
    return {
        'profile': profile,
        'site': "http://" + str(Site.objects.get_current())
    }
