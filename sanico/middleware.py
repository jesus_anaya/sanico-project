from django.http import HttpResponsePermanentRedirect
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.contrib.auth import logout
from django.contrib import admin
from mezzanine.conf import settings
from mezzanine.core.models import SitePermission
from mezzanine.blog.views import blog_post_detail
from mezzanine.utils.sites import current_site_id, templates_for_host
import urlparse
import cgi
import re


class WWWRedirectMiddleware(object):
    def process_request(self, request):
        if request.META['HTTP_HOST'].startswith('www.'):
            return HttpResponsePermanentRedirect('http://' + str(Site.objects.get_current()))


class SitePermissionMiddleware(object):
    """
    Marks the current user with a ``has_site_permission`` which is
    used in place of ``user.is_staff`` to achieve per-site staff
    access.
    """
    def process_view(self, request, view_func, view_args, view_kwargs):
        has_site_permission = True
        if request.user.is_superuser:
            has_site_permission = True
        elif request.user.is_staff:
            lookup = {"user": request.user, "sites": current_site_id()}
            try:
                SitePermission.objects.get(**lookup)
            except SitePermission.DoesNotExist:
                admin_index = reverse("admin:index")
                if request.path.startswith(admin_index):
                    logout(request)
                    view_func = admin.site.login
                    extra_context = {"no_site_permission": True}
                    return view_func(request, extra_context=extra_context)
            else:
                has_site_permission = True
        request.user.has_site_permission = has_site_permission


class SearchReferrerMiddleware(object):
    SEARCH_ENGINES = (
        'google',
        'bing',
        'yahoo',
    )

    @classmethod
    def parse_search(self, url):
        """
        """
        if url is not None:
            for engine in self.SEARCH_ENGINES:
                if engine in url.lower():
                    return engine
        return "another"

    def process_view(self, request, view_func, view_args, view_kwargs):
        referrer = request.META.get('HTTP_REFERER')
        engine = self.parse_search(referrer)
        request.META['SEARCH_ENGINE'] = engine


# class RootSlugBlogPost(object):
#     def process_request(self, request):
#         pass
#         if request.path.startswith('/comment/'):
#            setattr(request, '_dont_enforce_csrf_checks', True)

#         if request.path.startswith('/blog-'):
#            slug = request.path.replace('/blog-', '').replace('/', '')
#            return blog_post_detail(request, slug)
