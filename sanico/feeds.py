from django.conf import settings
from django.utils.safestring import mark_safe
from mezzanine.blog.feeds import PostsRSS, PostsAtom


def get_image_url(item):
    return settings.MEDIA_URL + unicode(item.image)


class SanicoRSS(PostsRSS):
    ''' Sanico RSS Cunstom blog with image'''

    def item_description(self, item):
        desc = '<p><img class="center" src="%s" width="700" height="280" alt="%s"/></p>' % (get_image_url(item), item.image_description)
        content = "<p>%s</p>" % item.content
        return mark_safe("%s%s" % (desc, content))
    item_description.allow_tags = True


class SanicoAtom(PostsAtom):
    ''' Sanico RSS Cunstom blog with image'''

    def item_description(self, item):
        desc = '<p><img class="center" src="%s" width="700" height="280" alt="%s"/></p>' % (get_image_url(item), item.image_description)
        content = "<p>%s</p>" % item.content
        return mark_safe("%s%s" % (desc, content))
    item_description.allow_tags = True
