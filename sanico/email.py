from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.conf import settings
import sys


class SendEmail():
    mail_to = settings.EMAI_CONTACT if not settings.DEBUG else settings.EMAIL_DEBUG_CONTACT
    mail_copy = settings.EMAI_CONTACT_COPY
    mail_bcc = settings.EMAI_CONTACT_BCC

    message = None
    thanks_message = None
    context = {}

    def send(self):
        try:
            if self.message:
                self.message.send()

                if self.thanks_message:
                    self.thanks_message.send()
                return True
        except:
            print "Error send a email ", sys.exc_info()[0]
        return False

    def set_recipes(self, to=None, cc=None, bcc=None):
        self.mail_to = to if to is not None else self.mail_to
        self.mail_copy = cc if cc is not None else self.mail_copy
        self.mail_bcc = bcc if bcc is not None else self.mail_bcc

    def to_user_message(self, subject, template, to):
        self.thanks_message = self.__prepare_mail(subject, template, [to])

    def notify_message(self, subject, template, context={}):
        self.context = context
        self.message = self.__prepare_mail(subject, template)

    def __prepare_mail(self, subject, template, mail_to=None):
        try:
            html_content = render_to_string(template, self.context)
            text_content = strip_tags(html_content)

            from_email = settings.DEFAULT_FROM_EMAIL

            if mail_to:
                message = EmailMultiAlternatives(
                    subject=subject,
                    body=text_content,
                    from_email=from_email,
                    to=mail_to,
                    bcc=[],
                    cc=[])
            else:
                message = EmailMultiAlternatives(
                    subject=subject,
                    body=text_content,
                    from_email=from_email,
                    to=self.mail_to,
                    bcc=self.mail_bcc,
                    cc=self.mail_copy)

            message.attach_alternative(html_content, "text/html")
            return message
        except:
            print "Error send email: %s: %s" % (subject, sys.exc_info()[0])
            return None
