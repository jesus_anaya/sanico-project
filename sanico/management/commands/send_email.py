from django.core.management.base import BaseCommand
from django.utils import timezone
from sanico.models import WaitContacEmail
from sanico.helpers import send_contact_email
import datetime


class Command(BaseCommand):
    def handle(self, *args, **options):
        emails = WaitContacEmail.objects.all()
        if emails.exists():
            for email in emails:
                if timezone.now() >= email.datetime + datetime.timedelta(minutes=3):
                    if send_contact_email(email.email):
                        print "send email for %s" % unicode(email)
                        email.delete()
                    else:
                        print "Error send email for %s" % unicode(email)
        else:
            print "No emails to sent"
