# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.conf import settings
from profile.models import TreatmentCategory, Procedure, ClinicCategory
import os


class Command(BaseCommand):
    def handle(self, *args, **options):

        clinic_category = 0
        treatment_category = None

        f = open(os.path.join(settings.PROJECT_ROOT, "treatments.txt"), 'r')

        for line in f.readlines():
            if line[0] == '+':
                clinic_category = ClinicCategory.objects.get(name=line.strip('+').strip('\n'))
                print u"Into clinc category %s" % line.strip("+")
            elif line[0] == '-':
                name =  line.strip("-").decode('utf8').encode('ascii', 'xmlcharrefreplace')

                tc = TreatmentCategory()
                tc.clinic_category = clinic_category
                tc.name = name
                tc.save()

                treatment_category = tc
                print u"Add treatment category: %s" % name
                print u"Into treatment category: %s" % name

            elif line[0] != '\n':
                name = line.decode('utf8').encode('ascii', 'xmlcharrefreplace')

                pr = Procedure()
                pr.category = treatment_category
                pr.name = name
                pr.save()

                print u"Add procedure: %s" % name
