# -*- encoding: utf-8 -*-

from django.http import HttpResponse
from django.utils import simplejson as json
from django.utils.html import strip_tags
from django.core.mail import EmailMultiAlternatives
from django.contrib.auth.decorators import user_passes_test
from django.template.loader import render_to_string
from django.db.models import Q
from django.conf import settings
from sanico.email import SendEmail
from hashlib import md5
import datetime


KEYWORDS = {
    'Dental': 'dental-tourism',
    'Medical & Cosmetic Surgery': 'health-tourism',
    'Optical': 'medical-tourism',
    'Hospital': 'health-tourism'}


def change_image_name(name, instance, keyword=None):
    try:
        format = name[name.rfind('.'):]
        date = datetime.datetime.now().strftime("%Y-%m-%d-%H%M%S")

        if not keyword:
            keyword = KEYWORDS[unicode(instance.category.name)]

        return "%s-%s%s" % (keyword, date, format)
    except:
        print "Error in file name"
        return name


def clinic_upload_to(path):
    """
    Change the name for all images in clinics
    """
    def upload_callback(instance, filename):
        name = change_image_name(filename, instance)
        return '%s%s' % (path, name)
    return upload_callback


def sponsor_upload_to(path):
    """
    Change the name for all images in sponsor section
    """
    def upload_callback(instance, filename):
        name = change_image_name(filename, instance, 'medical-travel')
        return '%s%s' % (path, name)
    return upload_callback


def post_upload_to(path):
    """
    Change the name for all images in posts
    """
    def upload_callback(instance, filename):
        try:
            if instance.categories.filter(title='dental').exists():
                keyword = 'dental-tourism'
            elif instance.categories.filter(Q(title='hospital') | Q(title='medical and cosmetic surgery')).exists():
                keyword = 'health-tourism'
            else:
                keyword = 'medical-tourism'
        except:
            keyword = 'medical-tourism'

        name = change_image_name(filename, instance, keyword)
        return '%s%s' % (path, name)
    return upload_callback


def upload_to(path):
    """
    Change the name for all images
    """
    def upload_callback(instance, filename):
        name = md5(filename.encode('utf-8')).hexdigest()
        return '%s%s%s' % (path, name, filename[filename.rfind('.'):])
    return upload_callback


def validate_form(request, form, email_info={}, email_thanks=None):
    result = False
    if request.method == "POST" and request.is_ajax():
        form_obj = form(request.POST)
        if form_obj.is_valid():
            try:
                #Notify message
                email = SendEmail()

                email.notify_message(
                    email_info['subject'],
                    "emails/%s" % email_info['template'],
                    {'form': form_obj.cleaned_data})

                #send user response
                if email_thanks:
                    email.to_user_message(
                        email_thanks['subject'],
                        "emails/%s" % email_thanks['template'],
                        form_obj.cleaned_data['email'])

                result = email.send()
            except:
                print "Error sendding email %s" % email_info['template']
        else:
            print form_obj.errors
    return HttpResponse(json.dumps({'success': result}), "application/json")


def staff_required(login_url=None):
    return user_passes_test(lambda u: u.is_staff, login_url=login_url)


def send_contact_email(mail_to):
    subject = "Thanks for contact us"
    template = "emails/clinic-contact.html"
    from_email = settings.DEFAULT_FROM_EMAIL
    contact_email = mail_to = settings.EMAI_CONTACT if not settings.DEBUG else settings.EMAIL_DEBUG_CONTACT

    html_content = render_to_string(template, {})
    text_content = strip_tags(html_content)

    email = EmailMultiAlternatives(
        subject=subject,
        body=text_content,
        from_email=from_email,
        to=mail_to,
        bcc=contact_email,
        cc=[])

    email.attach_alternative(html_content, "text/html")

    return email.send()
