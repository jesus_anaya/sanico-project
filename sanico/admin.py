from copy import deepcopy
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.template.defaultfilters import slugify
from mezzanine.blog.admin import BlogPostAdmin
from mezzanine.blog.models import BlogPost
from mezzanine.pages.admin import PageAdmin
from sanico.models import BlankPage, SanicoUser, Newsletter, Sponsor
from profile.helpers import hide_model
from .forms import SanicoUserChangeForm, SanicoUserCreationForm, SponsorForm


blog_fieldsets = deepcopy(BlogPostAdmin.fieldsets)
blog_fieldsets[0][1]["fields"].insert(-2, "image")
blog_fieldsets[0][1]["fields"].insert(-2, "image_description")


class SanicoBlogPostAdmin(BlogPostAdmin):
    fieldsets = blog_fieldsets

    def save_model(self, request, obj, form, change):
        super(SanicoBlogPostAdmin, self).save_model(request, obj, form, change)
        if not obj.image_description or obj.image_description == '':
            try:
                image_name = str(obj.featured_image).split('/')[-1].split('.')[0].lower()
                obj.image_description = slugify(image_name)
            except:
                print "error save BlogPost"
            obj.save()


user_fieldsets = deepcopy(UserAdmin.fieldsets)
user_fieldsets[1][1]["fields"] = list(user_fieldsets[1][1]["fields"]) + ['telephone']


class SanicoUserAdmin(UserAdmin):
    def in_menu(self):
        return hide_model('sanico.SanicoUser')

    fieldsets = user_fieldsets
    form = SanicoUserChangeForm
    add_form = SanicoUserCreationForm


class NewsletterAdmin(admin.ModelAdmin):
    list_display = ['name', 'email', 'register_date']

    def in_menu(self):
        return hide_model('sanico.Newsletter')


class SponsorAdmin(admin.ModelAdmin):
    form = SponsorForm
    list_display = ['name', 'enable', 'url']
    list_editable = ['enable', 'url']

    def in_menu(self):
        return hide_model('sanico.Newsletter')


admin.site.unregister(BlogPost)
admin.site.register(BlogPost, SanicoBlogPostAdmin)
admin.site.register(BlankPage, PageAdmin)
admin.site.register(SanicoUser, SanicoUserAdmin)
admin.site.register(Newsletter, NewsletterAdmin)
admin.site.register(Sponsor, SponsorAdmin)
