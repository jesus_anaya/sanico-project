from django.conf.urls import patterns, url
from django.views.generic import RedirectView

urlpatterns = patterns(
    "sanico.views",
    url(r"^home/$", RedirectView.as_view(url='/')),
    url(r"^newsletter/$", 'newsletter', name='newsletter'),
    url(r"^add_clinic/$", 'add_clinic', name='add_clinic'),
    url(r"^contest_contact/$", 'contest_contact', name='contest_contact'),
    url(r"^landing_page_form/$", 'landing_page_form', name='landing_page_form'),
    url(r"^about-form/$", 'about_form', name='about_form'),
    url(r"^feedback_form/$", 'feedback_form', name='feedback_form'),
    url(r"^process_login/$", 'process_login', name='process_login'),
    url(r"^process_logout/$", 'process_logout', name='process_logout'),
    url(r"^results/$", 'results', name='results'),
    url(r"^renew_password/$", 'renew_password', name='renew_password'),
    url(r"^sanico/(?P<format>.*)$", 'sanico_feed', name='sanico_feed'),
    url(r"^accounts/profile/$", RedirectView.as_view(url='/admin/')),
    url(r"^accounts/login/$", RedirectView.as_view(url='/login/')),
    url(r"^admin/export-newsletters/$", 'export_newsletters', name='export_newsletters'),
    #url(r'^robots\.txt$', 'robots', name='robots'),
    #url(r'^humans\.txt$', 'humans', name='humans'),
    #url(r'^BingSiteAuth\.xml$', 'bing_site_auth', name='bing_site_auth'),
)
