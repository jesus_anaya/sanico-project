
colors = ['#53D39C', '#0DD1FF', '#662D91', 'Hospital'];

$(document).on('ready', function(){
    $(".results-header.results-simple-clinic").each(function(){
        try{
            $(this).css('background-color', colors[$(this).attr('data') - 1]);
            $(this).children('h3').css('color', 'white');
            //$(this).children('p').css('color', 'white');
        } catch(err){
            console.log('error in bg color');
        }
    });

    var result_counter = 0;
    $(".span9.results").each(function()
    {
        if (result_counter++ < 5)
        {
            $(this).addClass('results-top');
        }
    });

});
