var Browser = {
    Version: function() {
        var version = 999; // we assume a sane browser
        if (navigator.appVersion.indexOf("MSIE") != -1) {
            // bah, IE again, lets downgrade version number
            version = parseFloat(navigator.appVersion.split("MSIE")[1]);
        }
        return version;
    }
};

$(document).on("ready", function(){
    if (Browser.Version() <= 9) {
        $('input[type=text]').focus(function(){
            if($(this).val() === $(this).attr('placeholder')){
                $(this).val('');
            }
        });

        $('input[type=text]').blur(function(){
            if($(this).val() === ''){
                $(this).val($(this).attr('placeholder'));
            }
        });

        $('input[type=text]').each(function(){
            if($(this).val() === ''){
                $(this).val($(this).attr('placeholder'));
            }
        });

        //placeholder
        $(':input').click(function() {
            temp = $(this).attr('placeholder');
            $(this).attr('placeholder','');
            $(this).blur(function() {
                $(this).attr('placeholder',temp);
            });
        });
    }
});
