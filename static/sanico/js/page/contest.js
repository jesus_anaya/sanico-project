
// Send information using Ajax methos to Contest backend
$(document).on('ready', function(){
    //datepicker
    $( "#birthdaypicker" ).datepicker({
           dateFormat : 'yy-mm-dd',
           changeMonth : true,
           changeYear : true,
           yearRange: '-100y:c+nn'
       });

    $("#contest-form").validate({
        submitHandler: function(form) {
            // open popup
            popup_open();

            $(form).ajaxSubmit({
                dataType:  'json',
                success: function(response, statusText, xhr, $form)  {
                    if(response['success'] === true) {
                        popup_message("Thank you for sharing your story!");
                        $form.clearForm();
                        window.location.href = "/";
                    } else {
                        popup_error("Error sending your information!");
                    }
                },
                error: function(response, statusText, xhr, $form){
                    popup_error("Error sending your information!");
                }
            });
        },
        rules: {
            email: {
                required: true,
                email: true
            }
        }
    });

     // Input type="file" work-around
    $(".browse-file").change(function() {
        // fix idiotic HTML5 spec wanting C:\fakepath\ prepended
        var val = $(this).attr("value").replace(/C:\\fakepath\\/, '');
        $(".browse-input input").attr("value", val);
    });
});
