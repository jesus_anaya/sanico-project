
$(document).on('ready', function(){
    $("#contact-clinic-form").validate({
        submitHandler: function(form) {
            // open popup
            popup_open();
            popup_redirect("/free-quote-thanks/");

            $(form).ajaxSubmit({
                dataType: 'json',
                success: function(response, statusText, xhr, $form)
                {
                    if(response['success'] === true) {
                        popup_redirect_now();
                    } else {
                        popup_error("Error sending your information!");
                    }
                },
                error: function(response, statusText, xhr, $form){
                    popup_error("Error sending your information!");
                }
            });
        },
        rules: {
            email: {
                required: true,
                email: true
            },
            termsConditions: "required",
            desire: {
                required: function(element){
                    return ($("#find-list").val() === 'acquaintance');
                }
            }
        }
    });

    // Form Reward
    $('#find-list').bind('change', function (e) {
        if( $('#find-list').val() == "acquaintance") {
            $('#acquaintance-name').show();
        }
        else{
            $('#acquaintance-name').hide();
        }
    });

    $("#birthdaypicker").datepicker({
        dateFormat : 'yy-mm-dd',
        changeMonth : true,
        changeYear : true,
        yearRange: '-100y:c+nn'
    });

    $("#desire_appointment_date").datepicker({
        dateFormat : 'yy-mm-dd'
    });
});