
$(document).on('ready', function(){

  $("#add-clinic-id").validate({
      invalidHandler: function(event, validator) {
        $(".loading-image").hide();
      },
      submitHandler: function(form) {
        // open popup
        popup_open();
        popup_redirect("/add-clinic-thanks/");

        $(form).ajaxSubmit({
          dataType: 'json',
          success: function(response, statusText, xhr, $form){
            if(response['success'] === true) {
              popup_redirect_now();
            } else {
              popup_error("Error adding your clinic!");
            }
          },
          error: function(response, statusText, xhr, $form){
            popup_error("Error adding your clinic!");
          }
        });
      },
      rules: {
          clinic_email: {
              required: true,
              email: true
          },

          contact_email: {
              required: true,
              email: true
          }
      }
  });
});
