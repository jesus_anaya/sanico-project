//If user clicks to close button, setting IsPopupClosed variable to true
//Show that popup should not be visible on scroll up or down
var IsPopupClosed = false;
var reachSlide = {
    showSlider: function () {
        var container = $("#slidecontainer");
        if (!IsPopupClosed) {
            if (!container.is(":visible")) {
                // Show - slide down.
                container.slideDown(1500);
            }
        }
    },
    hideSlider: function () {
        var container = $("#slidecontainer");
        // visibility.
        if (container.is(":visible")) {
            // Hide - slide up.
            container.slideUp(1500);
        }
    }
};

//On page load attached scroll and close button event
//Suppose there is no scrollbar in the page than setTimeout will show popup
//if user stays on page for 20 second
$(window).load(function () {
    $(window).scroll(function () {
        var totalHeight = $(document).height();
        var topPos = $(this).scrollTop() + $(window).height();
        var article = $("article.span12.archive");
        var end_post_pos = article.position().top + article.height() - 43 || 800;

        //logic to show popup when scroll position form to is greater than 800 or
        //one third of document height
        if (topPos > end_post_pos) {
            reachSlide.showSlider();
        } else {
            reachSlide.hideSlider();
        }
    });

    setTimeout(function () {
        reachSlide.showSlider();
    }, 20000);

    $('#btnClose').click(function () {
        IsPopupClosed = true;
        reachSlide.hideSlider();
    });
});