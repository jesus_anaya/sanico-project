
function ratingStars(){
    var inputs = [];
    var count = 0;

    function changeStars($this, e, active, input){
        var x =  e.pageX - $this.offset().left;
        if (active) {
            var start = 0;
            if(x > 7){
                start = parseInt((x + 20) / 20, 10) * 20;
            }

            console.log(start);

            $this.find('.editable.star-rating').css({
                'width': start
            });
            input.val(start / 10);
        }
    }

    $(".vIntegerField").each(function(){
        inputs.push($(this));
    });

    $(".star-holder").each(function(){
        var active = false;
        var input = inputs[count++];

        $(this).on('mousemove', function(e){
            changeStars($(this), e, active, input);
        });

        $(this).on('mousedown', function(e){
            active = true;
            changeStars($(this), e, active, input);
        });

        $(this).on('mouseup', function(){
            active = false;
        });
    });
}

$(document).on('ready', function(){

    var ids = '#id_quality, #id_service, #id_communication, #id_cleanliness,' +
        '#id_price, #id_comfort, #id_value, #id_location, #id_satisfaction';

    $(ids).each(function(){
        var parent = $(this).parent();
        $(this).css('display', 'none');
        parent.append('<div class="star-holder"><div class="editable star-rating" style="width:' + $(this).val() + '0px;"></div></div>');
    });

    if(window.location.href.indexOf('/add/') !== -1){
        ratingStars();
    }
});