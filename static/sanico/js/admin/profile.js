
function getCities() {
    $.get('/api/get_cities/').done(function(data){
        var city = $("#id_city");
        var city_text = city.val();

        var results = data.results;
        var str = '<select id="id_city" name="city">';
        for(var x = 0; x < results.length; x++) {
            if(city_text == results[x].value) {
                str += '<option value="' + results[x].value + '" selected>' + results[x].value + '</option>';
            } else {
                str += '<option value="' + results[x].value + '">' + results[x].value + '</option>';
            }
        }
        str += '</select>';
        city.replaceWith(str);
    });
}

function formOpeningDays(){
    var x = 0;
    var text = "";
    $(".Clinic.Options > .col1 > .inline-group.inline-tabular").find('select').each(function(){
        $(this).find('option').each(function(){
            if($(this).val() === x.toString()){
                text = $(this).text();
            }
        });
        var name = $(this).attr('name');
        $(this).replaceWith("<input type='hidden' name='" + name + "'' value='" + x + "' />" +
            "<input type='text' value='" + text + "'' readonly />");
        x++;
    });
}

function timeField(){
    try{
        $(".vTimeField").each(function(){
            var start = $(this).kendoTimePicker({
                format: "HH:mm:ss",
                interval: 30
            }).data("kendoTimePicker");

            //define min/max range
            start.min("6:00:00");
            start.max("22:00:00");
        });
    } catch(err) {
        console.log("Error with Time Picker");
    }
}

function gallery(item){
    if($(".items.ui-sortable").find('input:checkbox').filter(':checked').size() > 3){
        alert('Can only select three images');
        item.attr('checked', false);
    }
}

function load_map(){
    try{
        var geocoder = new google.maps.Geocoder();

        var latlng = $('#coordinates').val().split(',');
        var latLng = new google.maps.LatLng(latlng[0], latlng[1]);

        var mapOptions = {
            scaleControl: true,
            zoom: 17,
            center: latLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById('mapCanvas'), mapOptions);
        var marker = new google.maps.Marker({
            position: latLng,
            title: 'Point A',
            map: map,
            draggable: false
        });
        marker.setMap(map);
    } catch(err) {
        console.log('Error render the map');
    }
}

function customLocation(){
    var map_div = "<input type='hidden' value='' id='coordinates'><div id='mapCanvas'></div>";
    $(".Location > .col2").append(map_div);
    if($("#id_location_set-0-link").val().length > 1) {
        $("#coordinates").val($("#id_location_set-0-link").val().split('ll=')[1].split('&')[0]);
        load_map();
    }

    $("#id_location_set-0-link").on('change', function(){
        $("#coordinates").val($("#id_location_set-0-link").val().split('ll=')[1].split('&')[0]);
        load_map();
    });
}

$(document).on('ready', function(){
    formOpeningDays();
    timeField();
    gallery();
    try{
        customLocation();
    } catch(err) {
        console.log("Error open location");
    }
    getCities();

    $('.item.form-cell.in_front > input:checkbox').on('change', function(){
        gallery($(this));
    });
});
