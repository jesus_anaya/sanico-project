// script to Sanico

// using jQuery

function resize_content(){
  if($(window).height() > $("body").height()){
    var height = $('section.container').height();
    $('section.container').css('min-height', height + 1 + $(window).height() - $("body").height() + 'px');
  }
}

function creazy_egg(){
    setTimeout(function(){
        var a = document.createElement("script");
        var b = document.getElementsByTagName("script")[0];
        a.src = document.location.protocol + "//dnn506yrbagrg.cloudfront.net/pages/scripts/0017/0857.js?" + Math.floor(new Date().getTime() / 3600000);
        a.async = true;
        a.type = "text/javascript";
        b.parentNode.insertBefore(a, b);
    }, 1);
}

function topbar_autofixed(){

    var topbar = $(".navbar-inner");
    var pos = topbar.position();

    $(window).scroll(function(){
        try {
            if($(this).scrollTop() > pos.top + topbar.height() && !topbar.hasClass('header-float')){
                topbar.fadeOut('fast', function(){
                    $(this).addClass('header-float').fadeIn('fast');
                });
            } else if($(this).scrollTop() <= pos.top && topbar.hasClass('header-float') || $(this).scrollTop() === 0){
                topbar.removeClass('header-float');
            }
        } catch(err) {
            console.log("Error in window scroll: " + err.toString());
        }
    });

    $(".alert.beta-alert.fade.in > .close").on('click', function(){
        setTimeout(function(){
            pos = topbar.offset();
        }, 1000);
    });
}

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        // or any other URL that isn't scheme relative or absolute i.e relative.
        !(/^(\/\/|http:|https:).*/.test(url));
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== "") {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$(document).on('ready', function() {

    // Set CSRFToken to session
    var csrftoken = getCookie('csrftoken');
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                // Send the token to same-origin, relative URLs only.
                // Send the token only if the method warrants CSRF protection
                // Using the CSRFToken value acquired earlier
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    $("input[name='csrfmiddlewaretoken']").each(function(){
        $(this).val(csrftoken);
    });

    //load newsletter box
    $(".newsletter").css('display', 'block');

    $("#accordion").accordion({
        event: "mouseover"
    });

    $("#comment").validate({
        rules: {
            email: {
                required: true,
                email: true
            }
        }
    });

    $('.middleinput:text, textarea').addClass('xlarge');
    $('.control-group label').addClass('control-label');

    var cities = new AutoSuggest('search_cities', {
        script: "/api/get_cities/?",
        varname: "term",
        json: true,
        timeout: 4000,
        minchars: 2,
        maxentries: 12
    });

    var category = new AutoSuggest('search_category', {
        script: "/api/get_looking_for/?",
        varname: "term",
        json: true,
        timeout: 4000,
        minchars: 2,
        maxentries: 12
    });

    /* Alert */
    $(".alert").alert();

    $("#feedback-form").validate({
        submitHandler: function(form) {
            // open popup
            $("#feedback-modal").modal('hide');
            popup_open();
            popup_redirect("/medical-tourism-thanks/");
            // add redirec url here

            $(form).ajaxSubmit({
                dataType: 'json',
                success: function(response, statusText, xhr, $form){
                    if(response['success'] === true) {
                        popup_redirect_now();
                    } else {
                        popup_error("Error sending your information!");
                    }
                    $form.clearForm();
                },
                error: function(response, statusText, xhr, $form){
                    popup_error("Error sending your information!");
                }
            });
        },
        rules: {
            email: {
                required: true,
                email: true
            },
            features: {
                required: true
            },
            usability: {
                required: true
            },
            design: {
                required: true
            }
        }
    });

    resize_content();

    // cookie for aler beta bar
    if(document.cookie.indexOf("beta_alert") >= 0) {
        $(".beta-alert").hide();
    }

    $(".alert.beta-alert.fade.in > .close").on('click', function(){
        var date = new Date();
        var day = (60 * 60 * 24 * 1000);

        date.setTime(date.getTime() + day);
        var expires = "; expires=" + date.toGMTString();

        document.cookie = "beta_alert=true" + expires + ";";
        console.log("Alert close");
    });

    //Crazy egg Analitycs
    creazy_egg();

    //Auto fix effect in top bar
    topbar_autofixed();

    //newsletter
  var slideleft = $('#slideleft button');
  var $lefty = slideleft.next();

  $lefty.css('left', '-' + slideleft.outerWidth() + 1 + 'px');

  slideleft.click(function() {
    $lefty.animate({
      left: parseInt($lefty.css('left'), 10) === 0 ?
        -$lefty.outerWidth() :
        0
    });
  });
});
