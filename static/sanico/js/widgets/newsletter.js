// post-submit callback

(function($) {
  $(".form-newsletter").each(function(){
    var parent = $(this).parent().parent();
    $(this).validate({
      submitHandler: function(form) {
          parent.children(".newsletter-box").css('display', 'none');
          parent.children(".newsletter-loading").css('display', 'inline-block');
          $(form).ajaxSubmit({
            dataType: 'json',
            success: function(response, statusText, xhr, $form)  {
                if(response['success'] === true) {
                    alert("Thank you for Subscribing!");
                    window.location.href = "/newsletter-thanks/";
                } else {
                    alert("Error sending your subscription!");
                    parent.children(".newsletter-box").css('display', 'inline-block');
                    parent.children(".newsletter-loading").css('display', 'none');
                }
                $form.clearForm();
            }
          });
      },
      rules: {
          email: {
              required: true,
              email: true
          }
      }
    });
  });
})($);