
//flag for redirect after to popup is show
var popup_redirection = false;
var popup_no_redirect = false;
//method for open the popup window
function popup_redirect(redirect_to){
  $("#redirect-form").val(redirect_to);
}

function popup_redirect_now(){
  window.location.href = $("#redirect-form").val();
}

function popup_message(message){
  $(".loading-image").hide();
  $("#contact-message-text").text(message);
  $("message-pop-close").show();
  popup_redirection = true;
}

function popup_error(message){
  popup_message(message);
  popup_redirection = false;
}

function popup_open(){
  //clear the text message
  $("#contact-message-text").text("");

  //open popup window
  $("#message-pop").modal();
  $(".loading-image").show();

  // reset redirection flag
  popup_redirection = false;
}

function popup_noredirect(){
  popup_no_redirect = true;
}

$(document).on('ready', function(){

  // get the static url fron meta data in base.html
  var static_url = $("meta[name='static_url']").attr('data');

  // popup template
  var popup = '' +
  '<div class="modal hide fade" id="message-pop">' +
    '<div class="modal-header">' +
      '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
      '<h3>Message</h3>' +
    '</div>' +
    '<div class="modal-body" style="height:130px">' +
      '<div id="contact-message-text"></div>' +
      '<img src="' + static_url + 'sanico/img/loading_2.gif" class="loading-image">' +
    '</div>' +
    '<div class="modal-footer">' +
      '<a href="#!" class="btn btn-small btn-primary" id="message-pop-close">Close</a>' +
    '</div>' +
  '</div><input type="hidden" id="redirect-form" value="">';

  // inset the popup in the html body
  $('body').append(popup);

  // set a default redirect
  popup_redirect("/");

  $("message-pop-close").hide();

  $('#message-pop-close').on('click', function(){
    //loading hide
    $(".loading-image").hide();
    // hide the popup
    $('#message-pop').modal('hide');
    // redirect to spesific url
    if(popup_redirection && !popup_no_redirect){
      setTimeout(function(){
        window.location.href = $("#redirect-form").val();
      }, 1000);
    }
  });
});
