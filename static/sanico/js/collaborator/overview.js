// add the filter to your application module
angular.module('OverviewApp', ['filters']);

/**
 * Truncate Filter
 * @Param text
 * @Param length, default is 10
 * @Param end, default is "..."
 * @return string
 */
angular.module('filters', []).
    filter('truncate', function () {
        return function (text, length, end) {
            if (isNaN(length))
                length = 10;

            if (end === undefined)
                end = "...";

            if (text.length <= length || text.length - end.length <= length) {
                return text;
            }
            else {
                return String(text).substring(0, length-end.length) + end;
            }

        };
    });

function OverviewController($scope, $http){
    var csrfToken = $('input[name=csrfmiddlewaretoken]').val();
    $http.defaults.headers.common['X-CSRFToken'] = csrfToken;
    $scope.default_offer = "Agregar Oferta";
    $scope.edit_offer = false;
    $scope.testimonial = "";

    var config = {
        headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
    };

    $http.get("/collaborator/rest/overview").success(function(data) {
        $scope.offer = data.offer || $scope.default_offer;
        $scope.offer_edited = data.offer || "";
        $scope.testimonial = data.testimonial;
        $scope.testimonial_name = data.testimonial_name;
    });

    $scope.change_overview = function(){
        var overview_data = {
            'offer': $scope.offer_edited
        };

        $http.put("/collaborator/rest/overview", overview_data).success(function(data) {
            $scope.offer = data.offer || $scope.default_offer;
            $scope.offer_edited = data.offer || "";
            $scope.edit_offer = false;
        });
    };

    $scope.change_testimonial = function(){
        var review = {
            'review_selected': $scope.review_selected
        };

        $http.post("/collaborator/rest/testimonial", $.param(review), config).success(function(data) {
            $scope.testimonial = data.testimonial;
            $scope.testimonial_name = data.testimonial_name;
        });
    };

    $scope.cancel = function(){
        $scope.offer_edited = $scope.offer;
        $scope.edit_offer = false;
    };
}