
var render_images_url = "";

function start(){
    render_images_profile();
}

function update_overview_image(){
    $("#overview-image").attr('src', $("#profile-image").attr('src'));
}

/**** Ajax Methods *******/

function send_heade_images() {
    $(".file-images").change(function(){});
    $('#images-form').ajaxSubmit({
        dataType: 'json',
        success: function(response, statusText, xhr, $form) {
            if(response['success'] === true){
                //render new profile images
                render_images_profile();
            } else {
                alert('Error enviando informacion!');
            }
        }
    });
}

function render_images_profile(){
    $.get(render_images_url).done(function(data){
        $("#header-images-content").html(data);
        //update overview image with new image uploades
        try {
            update_overview_image();
            touch_events();
        } catch(err) {}
        // update touch or click events
    });
}

/*** listen clinc events *****/
function touch_events(){

    $(".edit-header-image").on('click', function(){
        $("#field-header-image").trigger('click');
        $("#field-header-image").change(function(){
            console.log("name: " + $("#field-header-image").val());
            if($("#field-header-image").val() !== '') {
                $('.header-image-loading').css('display', 'inline');
                send_heade_images();
            }
        });
    });

    $(".edit-profile-image").on('click', function(){
        $("#field-profile-image").trigger('click');
        $("#field-profile-image").change(function(){
            console.log("name: " + $("#field-profile-image").val());
            if($("#field-profile-image").val() !== '') {
                $('.header-image-loading').css('display', 'inline');
                send_heade_images();
            }
        });
    });
}

/***** Popup ******/

$(document).on('ready', function(){
    // get url from django template
    render_images_url = $('meta[name="render_heade_images"]').attr('data');
    start();
});