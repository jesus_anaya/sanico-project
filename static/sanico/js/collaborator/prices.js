/*
 *
 */
function PricesController($scope, $http, $element, Restangular) {
    var csrfToken = $('input[name=csrfmiddlewaretoken]').val();
    $http.defaults.headers.common['X-CSRFToken'] = csrfToken;
    $scope.treatments = [];

    $scope.update_treatments = function() {
        // get categories list
        Restangular.all('treatment-categories').getList().then(function(treatments) {

            // get clinic procedures list
            Restangular.all('clinic-procedures').getList().then(function(procedures) {

                // generate a tree with relationship between treatments and procedures
                var treatment_position = 1;
                for(var x = 0; x < treatments.length; x++) {
                    var treatment = treatments[x];

                    // Create new procedures list into the tratment
                    treatment.procedures = [];

                    // starts generate procedures tree
                    var procedure_position = 1;
                    for(var i = 0; i < procedures.length; i++) {
                        if (procedures[i].procedure.category.id === treatment.category.id) {
                            var procedure = procedures[i];

                            // validate if is necessary assign position value at backend
                            if (procedure.position !== procedure_position) {
                                procedure.patch({position: procedure_position});
                            }

                            procedure.position = procedure_position++;
                            treatment.procedures.push(procedure);
                        }
                    }

                    // validate if is necessary assign position value at backend
                    if (treatment.position !== treatment_position) {
                        treatment.patch({position: treatment_position});
                    }
                    treatment.position = treatment_position++;
                    treatment.last_position = procedure_position;
                }
                $scope.treatments = treatments;
            });
        });
    };
    $scope.update_treatments();

    $scope.delete_treatment = function(treatment) {
        if (confirm("Desea borrar el tratamiento y sus procedimientos?")) {
            // Remove all procedures from backend
            for(var x = 0; x < treatment.procedures.length; x++) {
                treatment.procedures[x].remove();
            }

            // delete treatment and procedures from the list
            $scope.treatments.splice($scope.treatments.indexOf(treatment), 1);
        }
    };

    $scope.edit_price = function(procedure) {
        procedure.price_edited = true;
        procedure.price = (procedure.price === "Undefined" ? "" : procedure.price);
    };

    $scope.edit_duration = function(procedure) {
        procedure.duration_edited = true;
        procedure.duration = (procedure.duration === "Undefined" ? "" : procedure.duration);
    };

    $scope.position_up = function(list, obj){
        // Declare relative names
        var index = list.indexOf(obj);
        var first = list[index - 1];
        var second = list[index];

        // reordering positions
        var temp = first;
        first = second;
        second = temp;

        // increase position counter
        first.position--;
        second.position++;

        // syncronize object in the server
        first.patch({position: first.position});
        second.patch({position: second.position});

        // syncronize object in the objects list
        list[index - 1] = first;
        list[index] = second;
    };

    $scope.position_down = function(list, obj){
        // Declare relative names
        var index = list.indexOf(obj);
        var first = list[index + 1];
        var second = list[index];

        // reordering positions
        var temp = first;
        first = second;
        second = temp;

        // increase position counter
        first.position++;
        second.position--;

        // syncronize object in the server
        first.patch({position: first.position});
        second.patch({position: second.position});

        // syncronize object in the objects list
        list[index + 1] = first;
        list[index] = second;
    };

    $scope.close_modal = function() {
        setTimeout(function(){
            $scope.edit_mode = false;

            $(".procedure").each(function(){
                $(this).attr('checked', false);
            });
        },
        1000);
    };

    $scope.send_procedures = function() {
        var procedures = [];
        $(".procedure").each(function(){
            if($(this).attr('checked')) {
                var pid = parseInt($(this).val(), 10);

                Restangular.all('clinic-procedures').post({
                    description: "",
                    procedure: pid,
                    price: "Undefined",
                    duration: "Undefined",
                    position: 0
                });
            }
        });

        //Close Modal
        $('#myModal').modal('hide');
        $scope.close_modal();

        // Jesus Anaya Experiment
        setTimeout($scope.update_treatments, 1000);
    };

    $scope.send_add_treatment = function() {
        var config = {
            headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
        };

        if($scope.procedure_name && $scope.procedure_description) {
            var url = $('meta[name="add_treatment_url"]').attr('content');
            $scope.sending = true;

            var send_data = {
                name: $scope.procedure_name,
                description: $scope.procedure_description,
            };

            $http.post(url, $.param(send_data), config).success(function(data) {
                $scope.sending = false;

                if(data.success) {
                    alert("Su informacion se envio exitosamente");
                } else {
                    alert("Error enviando su informacion, intente de nuevo mas tarde");
                }

                //Close Modal
                $('#myModal').modal('hide');
                $scope.close_modal();
            });
        } else {
            alert("Se necesitan ambos campos para enviar la solicitud");
        }
    };

    $scope.clone_procedure = function(treatment, procedure){
        var index = treatment.procedures.indexOf(procedure);

        Restangular.all('clinic-procedures').post({
            description: procedure.description,
            procedure: procedure.procedure.id,
            price: procedure.price || "Undefined",
            duration: procedure.duration || "Undefined",
            position: treatment.last_position
        })
        .then(function(item){
            treatment.procedures.splice(index, 0, item);
            treatment.last_position++;
        });
    };

    $scope.save_procedure = function(procedure){
        procedure.patch({
            description: procedure.description,
            price: procedure.price || "Undefined",
            duration: procedure.duration || "Undefined",
        })
        .then(function(item){
            procedure.price = procedure.price || "Undefined";
            procedure.duration = procedure.duration || "Undefined";
        });

        procedure.description_edited = false;
        procedure.price_edited = false;
        procedure.duration_edited = false;
    };

    $scope.del_procedure = function(treatment, procedure) {
        var index = treatment.procedures.indexOf(procedure);

        if (confirm("Desea borrar el procedimiento?")) {
            procedure.remove();
            treatment.procedures.splice(index, 1);

            if (treatment.procedures.length === 0) {
                $scope.treatments.splice($scope.treatments.indexOf(treatment), 1);
            }
        }
    };

    $scope.no_description = function(procedure) {
        return (procedure.description_edited === false && procedure.description === "");
    };

    $scope.bool = function(val){
        return (val === 0 ? false : true);
    };
}
