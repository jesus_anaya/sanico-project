
var CollaboratorApp = angular.module('CollaboratorApp', []);

CollaboratorApp.directive('jform', function() {
    return function(scope, element, attrs) {
        element.submit(function() {
            $(this).find('.doctor-info-loading').css('display', 'inline');
            $(this).ajaxSubmit({
                dataType: 'json',
                success: function showResponse(response, status, xhr, $form) {
                    $(this).find('.doctor-info-loading').css('display', 'none');
                    scope.doctors[scope.doctors.indexOf(scope.doctor)] = response;
                    scope.$apply();
                },
                error: function showResponse(response, status, xhr, $form) {
                    $(this).find('.doctor-info-loading').css('display', 'none');
                    alert("Error guardando datos, verifique que todos los campos sean correctos");
                }
            });
            return false;
        });
        scope.$watch(attrs.jform, function(value) {
            scope.doctor = value;
        });
    };
});

CollaboratorApp.filter('resume', function() {
  return function(input) {
    var parts = input.replace('\\', '/').split('/');
    return parts[parts.length - 1];
  };
});

function DoctorsController($scope, $http){
    $scope.csrf_token = $('input[name="csrfmiddlewaretoken"]').val();
    $http.defaults.headers.common['X-CSRFToken'] = $scope.csrf_token;

    $scope.media_url = $("meta[name='media_url']").attr('data');
    $scope.static_url = $("meta[name='static_url']").attr('data');

    $http.get("/collaborator/rest/doctor").success(function(data) {
        $scope.doctors = data;
    });

    $scope.bool = function(val){
        return (val === 0 ? false : true);
    };

    $scope.add_doctor = function(){
        this.doctors.push({
            id: 0,
            fisrt_name: '',
            last_name: '',
            education: '',
            specialisations: '',
            languages: ''
        });
    };

    $scope.del_doctor = function(doctor){
        if (doctor.id !== 0){
            if(confirm("desea borrar este doctor?")){
                $http.delete("/collaborator/rest/doctor/" + doctor.id).success(function(data) {
                    $scope.doctors.splice($scope.doctors.indexOf(doctor), 1);
                });
            }
        } else {
            $scope.doctors.splice(this.doctors.indexOf(doctor), 1);
        }
    };

    $scope.get_form_url = function(doctor){
        return "/collaborator/rest/doctor" + (doctor.id === 0 ? "" : "/" + doctor.id);
    };
}
