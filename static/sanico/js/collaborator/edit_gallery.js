$(document).on('ready', function() {
    // $(".span3.editable-image").each(function(){
    //    $(this).children('div').html("<h1>Ola ke ase</h1>");
    // });

    var csrf_token = $('input[name="csrfmiddlewaretoken"]').attr('value');
    var url_del = $("meta[name='url_del']").attr('data');
    var url_render = $("meta[name='url_render']").attr('data');
    var url_add = $("meta[name='url_add']").attr('data');
    var add_to_front = $("meta[name='add_to_front']").attr('data');

    function csrfToken(event) {
        if (csrf_token !== undefined){
            event.data = {csrfmiddlewaretoken: csrf_token};
        }
    }

    function afterSucces() {
        $.get(url_render, function(data){
            $("#profile-gallery").html(data);
            closePhotos();
            checouts_validation();
        });
    }

    function closePhotos() {
        $(".gallery-close-button").on('click', function(){
            var id = $(this).children('span').attr('data');
            var context = {
                'csrfmiddlewaretoken': csrf_token,
                'image': id
            };

            if(confirm("Delete photo?")){
                $.post(url_del, context).done(function(data){
                    if(data['success'] === true){
                        afterSucces();
                    } else {
                        alert("Error deleting photo");
                    }
                });
            }
        });
    }

    function gallery(item){
        if($(".span3.editable-image").find('input:checkbox').filter(':checked').size() > 3){
            alert('Can only select three images');
            item.attr('checked', false);
            return false;
        }
        return true;
    }

    function checouts_validation(){
        $("input:checkbox").on("click", function(){
            if(gallery($(this))){

                var id = $(this).attr('data');
                var context = {
                    'csrfmiddlewaretoken': csrf_token,
                    'image': id
                };
                if($(this).is(':checked'))
                    context['in_front'] = 1;

                $.post(add_to_front, context).done(function(data){
                    if(data['success'] === true){
                        //afterSucces();
                    } else {
                        alert("Error update data");
                    }
                });
            }
        });
    }

    $("#add-photo").kendoUpload({
        async: {
            saveUrl: url_add
        },
        upload: csrfToken,
        success: afterSucces
    });

    // Change text in gallery upload button
    $(".k-button.k-upload-button").children("span").text("Agregar...");

    closePhotos();
    checouts_validation();
});
