function options_pressed(){
    $(".doctor").each(function(){

        $(this).find("h3.name").on('click', function(){
            $(this).css('display', 'none');
            $(this).parent().find(".name-edit").css('display', 'inline');
        });

        $(this).find("span.education").on('click', function(){
            $(this).css('display', 'none');
            $(this).parent().find(".education-edit").css('display', 'inline');
        });

        $(this).find("span.specialisations").on('click', function(){
            $(this).css('display', 'none');
            $(this).parent().find(".specialisations-edit").css('display', 'inline');
        });

        $(this).find("span.languages").on('click', function(){
            $(this).css('display', 'none');
            $(this).parent().find(".languages-edit").css('display', 'inline');
        });

        $(this).find(".edit-doctor-image").on('click', function(){
            $(this).parent().find("#field-doctor-image").trigger('click');
        });

        $(this).find("#del-doctor").on('click', function(){
            if(confirm("Desea borrar el doctor?")){
                delete_doctor($(this));
            }
        });
    });

    $("#add-doctor").on('click', function(){
        $(".new-doctor-form").css('display', 'inline');
        $(this).css('display', 'none');
    });
}

function reset_new_form() {
    $('.new-doctor-form').each(function(){
        this.reset();
        $(this).css('display', 'none');
        $("#add-doctor").css('display', 'inline');
    });
}

function delete_doctor(doctor) {
    var csrf_token = $('input[name="csrfmiddlewaretoken"]').attr('value');
    var data = {
        'doctor_id': doctor.attr('data'),
        'csrfmiddlewaretoken': csrf_token
    };

    $.post($("meta[name='delete_doctor_url']").attr('data'), data).done(function(response){
        if(response['success'] === true){
            render_doctors();
        } else {
            alert("Error enviado datos!");
        }
    });
}

function render_doctors() {
    $.get($('meta[name="render_doctors_url"]').attr('data')).done(function(data) {
        $('.doctors-list').html(data);
        options_pressed();
        send_doctor();
        reset_new_form();
    });
}

function send_doctor() {
    $('.doctor-form').submit(function() {
        $(this).ajaxSubmit({
            dataType: 'json',
            success: function showResponse(response, statusText, xhr, $form) {
                if(response['success'] === true){
                    render_doctors();
                } else {
                    alert('Error enviando informacion!');
                }
            }
        });
        return false;
    });
}

$(document).on('ready', function(){
    render_doctors();
});