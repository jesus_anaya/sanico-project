// post-submit callback

function AboutController($scope, $http){
    var csrfToken = $('input[name=csrfmiddlewaretoken]').val();
    $http.defaults.headers.common['X-CSRFToken'] = csrfToken;

    $scope.complementary_services = [];
    $scope.special_promotions = [];

    var config = {
        headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
    };

    $http.get("/collaborator/rest/complementary_service").success(function(data) {
        $scope.complementary_services = data;
    });

    $http.get("/collaborator/rest/special_promotion").success(function(data) {
        $scope.special_promotions = data;
    });

    $scope.unable = function(id, value_open, value_close){
        if ($scope['cancel_' + id]) {
            $scope['open_' + id] = '';
            $scope['close_' + id] = '';
        } else {
            $scope['open_' + id] = value_open;
            $scope['close_' + id] = value_close;
        }
    };

    $scope.enable_init = function(id){
        $scope['cancel_' + id] = ($scope['open_' + id] === '' && $scope['close_' + id] === '');
    };

    $scope.add_service = function(){
        $scope.complementary_services.push({name: '', id: 0});
    };

    $scope.add_promotion = function(){
        $scope.special_promotions.push({name: '', id: 0});
    };

    $scope.save_service = function(service){
        var index = $scope.complementary_services.indexOf(service);
        if(service.id === 0){
            $http.post("/collaborator/rest/complementary_service", $.param(service), config).success(function(data) {
                $scope.complementary_services[index] = data;
            });
        } else {
            $http.put("/collaborator/rest/complementary_service/" + service.id, service).success(function(data) {
                $scope.complementary_services[index] = data;
            });
        }
    };

    $scope.save_promotion = function(promotion){
        var index = $scope.special_promotions.indexOf(promotion);
        if(promotion.id === 0){
            $http.post("/collaborator/rest/special_promotion", $.param(promotion), config).success(function(data) {
                $scope.special_promotions[index] = data;
            });
        } else {
            $http.put("/collaborator/rest/special_promotion/" + promotion.id, promotion).success(function(data) {
                $scope.special_promotions[index] = data;
            });
        }
    };

    $scope.del_service = function(service){
        if(service.id !== 0)  {
            if(confirm("Desea borrar el servicio?")) {
                $http.delete("/collaborator/rest/complementary_service/" + service.id).success(function(data) {
                    $scope.complementary_services.splice($scope.complementary_services.indexOf(service), 1);
                });
            }
        } else {
            $scope.complementary_services.splice($scope.complementary_services.indexOf(service), 1);
        }
    };

    $scope.del_promotion = function(promotion){
        if(promotion.id !== 0)  {
            if(confirm("Desea borrar la promocion?")) {
                $http.delete("/collaborator/rest/special_promotion/" + promotion.id).success(function(data) {
                    $scope.special_promotions.splice($scope.special_promotions.indexOf(promotion), 1);
                });
            }
        } else {
            $scope.special_promotions.splice($scope.special_promotions.indexOf(promotion), 1);
        }
    };

    $scope.bool = function(val){
        return (val === 0 ? false : true);
    };
}

function timeField(){
    $(".about-hours").each(function(){
        var start = $(this).kendoTimePicker({
            change: function() {
                var startTime = start.value();
                if (startTime) {
                    startTime = new Date(startTime);
                    startTime.setMinutes(startTime.getMinutes() + this.options.interval);
                }
            },
            format: "HH:mm:ss",
            interval: 30
        }).data("kendoTimePicker");

        //define min/max range
        start.min("6:00 AM");
        start.max("10:00 PM");
    });

}

$(document).on('ready', function(){
    $(".about_form").submit(function() {
        tinyMCE.triggerSave();
        $(this).ajaxSubmit({
            dataType:  'json',
            success: function(response, statusText, xhr, $form)  {
                if(response['success'] === true) {
                    alert("Seccion \"About\" actualizada");
                } else {
                    alert("Error actualizando esta seccion!");
                }
            }
        });
        return false;
    });

    timeField();
});