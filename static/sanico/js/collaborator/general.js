
function GeneralController($scope, $http){
    $scope.send_change_required = function(){
        $http.get($('meta[name="url_requires_change"]').attr('data')).success(function(data){
            alert("Mensaje enviado!");
        }).error(function(data){
            alert("Error enviando mensaje!");
        });
    };
}

// post-submit callback
function passwordResponse(response, statusText, xhr, $form)  {
    if(response['success'] === true) {
        alert("La contraseña fue cambiada con exito!");
    } else {
        alert("Error cambiando su contraseña!");
    }
    $form.resetForm();
    hide_password_form();
}

var options_password = {
    success: passwordResponse,  // post-submit callback
    dataType:  'json'
};

function hide_password_form(){
    $(".password").css('display', 'inline');
    $(".password-edit").css('display', 'none');
}

function show_password_form(){
    $(".password").css('display', 'none');
    $(".password-edit").css('display', 'inline');
}

$(document).on('ready', function(){
    $("#change_password").validate({
        submitHandler: function(form) {
            $(form).ajaxSubmit(options_password);
        },
        rules: {
            password :"required",
            password_confirm:{
                equalTo: "#password"
            }
        },
        messages: {
            password: "Ingrese su contraseña",
            password_confirm: "Ingrese su contraseña de nuevo"
        }
    });

    var change_password = "";
    $(".password").on('click', function(){
        show_password_form();
    });
});
