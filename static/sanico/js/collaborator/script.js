$(document).on('ready', function(){
    $('.nav.nav-list > li').each(function(){
        if($(this).attr('data') == $("meta[name='page']").attr("data")){
            $(this).addClass('active');
        }
    });

    var min_height = $(window).height() - ($(".header-collaborator").height() * 2) - ($("footer.row-fluid").height() * 2);
    $('.span9.profile').css('min-height', '' + min_height + 'px');

    $(".edit-section").css('display', 'block');
});