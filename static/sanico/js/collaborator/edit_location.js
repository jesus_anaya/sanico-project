
$(document).on('ready', function(){
    $('#location-form').submit(function() {
        $(this).ajaxSubmit({
            dataType: 'json',
            success: function showResponse(response, statusText, xhr, $form) {
                if(response['success'] === true){
                    alert('Informacion guardada!');
                } else {
                    alert('Error enviando informacion!');
                }
            }
        });
        return false;
    });

    $("#location-frame").on('change', function(){
        var code = $(this).val();
        $("#map-preview").html(code);
    });

    $("#map-preview").html($("#location-frame").val());

    $("#link-id").on('change', function(){
        $('#coordinates').val($("#link-id").val().split('ll=')[1].split('&')[0]);
        map_initialize();
    });
});