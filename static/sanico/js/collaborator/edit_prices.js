function options_pressed(){
    $(".tratament").each(function(){
        add_click_event($(this));
        add_procedure_click($(this));
    });

    $(".add-treatment").on('click', function(){
        add_treatment($(this));
    });
}

function add_procedure_click(obj) {
    obj.find(".add-procedure").on('click', function(){
        add_procedure($(this).parent());
    });
}

function add_click_event(obj){
    obj.find("h3.name").on('click', function(){
        $(this).css('display', 'none');
        $(this).parent().find(".name-edit").css('display', 'inline');
    });

    obj.find("span.procedure").on('click', function(){
        $(this).css('display', 'none');
        $(this).parent().find(".procedure-edit").css('display', 'inline');
        $(this).parent().parent().attr('data', 'edited');
    });

    obj.find("span.price").on('click', function(){
        $(this).css('display', 'none');
        $(this).parent().find(".price-edit").css('display', 'inline');
        $(this).parent().parent().attr('data', 'edited');
    });

    obj.find("span.duration").on('click', function(){
        $(this).css('display', 'none');
        $(this).parent().find(".duration-edit").css('display', 'inline');
        $(this).parent().parent().attr('data', 'edited');
    });

    obj.find(".price-delete").on('click', function(){
        delete_price($(this));
    });

    obj.find(".treatment-delete").on('click', function(){
        if(confirm("Desea borrar el tratamiento?")){
            delete_treatment($(this).parent().parent().parent());
        }
    });
}

function add_procedure(treatment) {
    var template = '<tr class="price" data="edited"><td><input type="hidden" name="price_id" value="">' +
                    '<input type="hidden" name="treatment_id" value="[treatment_id]">' +
                    '<span class="procedure" style="display:none"></span>' +
                    '<input type="text" name="procedure" class="procedure-edit input-edit-large">' +
                    '</td><td><span class="price" style="display:none"></span>' +
                    '<input type="text" name="price" class="price-edit input-edit-short">' +
                    '</td><td><span class="duration" style="display:none"></span>' +
                    '<input type="text" name="duration" class="duration-edit input-edit-short">' +
                    '</td><td><span class="btn btn-red password-button price-delete" price-id="" treatment-id="[treatment_id]">' +
                    'Borrar</span></td></tr>';

    var treatment_id = treatment.find(".treatment-table").find("input[name='treatment_id']").attr('value');
    template = template.replace('[treatment_id]', treatment_id).replace('[treatment_id]', treatment_id);
    treatment.find(".treatment-table").find("tbody").append(template);

    var tr = treatment.find(".treatment-table").find("tbody").find("tr").last();
    add_click_event(tr);
}

function add_treatment(treatment) {
    var template = $(".template-treatment").html();
    var div_treatment = treatment.parent().parent().find('.treatments');

    var csrf_token = $('input[name="csrfmiddlewaretoken"]').attr('value');
    var data = {
        'csrfmiddlewaretoken': csrf_token
    };

    $.post($("meta[name='save_treatment_url']").attr('data'), data).done(function(response){
        if(response.success !== false){
            template = template.replace('[id]', response.success);
            div_treatment.append(template);
            add_procedure_click(div_treatment.find(".tratament").last());
            add_click_event(div_treatment.find(".tratament").last());
            send_treatment();
        } else {
            alert("Error borrando datos!");
        }
    });
}

function reset_new_form() {
    $('.new-doctor-form').each(function(){
        this.reset();
        $(this).css('display', 'none');
        $("#add-doctor").css('display', 'inline');
    });
}

function delete_price(price) {
    if(price.attr('price-id') !== '') {
        if(confirm("Desea borrar el procedimiento?")){
            var csrf_token = $('input[name="csrfmiddlewaretoken"]').attr('value');
            var data = {
                'price_id': price.attr('price-id'),
                'treatment_id': price.attr('treatment-id'),
                'csrfmiddlewaretoken': csrf_token
            };

            $.post($("meta[name='delete_price_url']").attr('data'), data).done(function(response){
                if(response.success === true){
                    price.parent().parent().remove();
                } else {
                    alert("Error borrando datos!");
                }
            });
        }
    } else {
        price.parent().parent().remove();
    }
}

function delete_treatment(treatment) {
    var csrf_token = $('input[name="csrfmiddlewaretoken"]').attr('value');
    var treatment_id = treatment.find(".treatment-form").find("input[name='treatment_id']").attr('value');

    var data = {
        'treatment_id': treatment_id,
        'csrfmiddlewaretoken': csrf_token
    };

    $.post($("meta[name='delete_treatment_url']").attr('data'), data).done(function(response){
        if(response.success === true){
            treatment.remove();
        } else {
            alert("Error borrando datos!");
        }
    });
}

function send_treatment() {
    var treatment = null;
    $('.treatment-form').submit(function() {
        treatment = $(this).parent();
        $(this).ajaxSubmit({
            dataType: 'json',
            success: function showResponse(response, statusText, xhr, $form) {
                if(response.success !== false){
                    treatment.find('input[name="treatment_id"]').attr('value', response.success);
                    save_prices(treatment);
                } else {
                    alert('Error enviando informacion!');
                }
            }
        });
        return false;
    });
}

function save_prices(treatment) {
    var csrf_token = $('input[name="csrfmiddlewaretoken"]').attr('value');
    treatment_num = treatment.find(".treatment-table").find("tr.price").size();
    counter = 0;
    var this_treatment = null;

    treatment.find(".treatment-table").find("tr.price").each(function(){
        if($(this).attr('data') === 'edited' ){
            var data = {
                'csrfmiddlewaretoken': csrf_token,
                'price_id': $(this).find('input[name="price_id"]').attr('value'),
                'treatment_id': $(this).find('input[name="treatment_id"]').attr('value'),
                'procedure': $(this).find('input[name="procedure"]').attr('value'),
                'price': $(this).find('input[name="price"]').attr('value'),
                'duration': $(this).find('input[name="duration"]').attr('value')
            };
            this_treatment = $(this);
            $.post($("meta[name='save_price_url']").attr('data'), data).done(function(response){
                if(response.success !== false){
                    this_treatment.find('input[name="price_id"]').attr('value', response.success);
                    this_treatment.find('.price-delete').attr('price-id', response.success);
                } else {
                    alert("Error borrando datos!");
                }
            });
        }
        if(counter++ == treatment_num - 1){
            reset_form($(this).parent().parent().parent());
        }
    });

    if(treatment_num === 0){
        reset_form(treatment.parent());
    }
}

function reset_form(treatment) {
    treatment.find("h3.name").each(function(){
        $(this).css('display', 'inline');
        $(this).parent().find(".name-edit").css('display', 'none');
        $(this).text($(this).parent().find(".name-edit").val());
    });

    treatment.find("span.procedure").each(function(){
        $(this).css('display', 'inline');
        $(this).parent().find(".procedure-edit").css('display', 'none');
        $(this).parent().parent().attr('data', '');
        $(this).text($(this).parent().find(".procedure-edit").val());
    });

    treatment.find("span.price").each(function(){
        $(this).css('display', 'inline');
        $(this).parent().find(".price-edit").css('display', 'none');
        $(this).parent().parent().attr('data', '');
        $(this).text($(this).parent().find(".price-edit").val());
    });

    treatment.find("span.duration").each(function(){
        $(this).css('display', 'inline');
        $(this).parent().find(".duration-edit").css('display', 'none');
        $(this).parent().parent().attr('data', '');
        $(this).text($(this).parent().find(".duration-edit").val());
    });
}

$(document).on('ready', function(){
    options_pressed();
    send_treatment();
});