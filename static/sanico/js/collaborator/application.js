/*
 *
 */

var CollaboratorApp = angular.module( "CollaboratorApp", ["restangular"]);


CollaboratorApp.config(function(RestangularProvider) {

    // Set the url root from sanico's api
    RestangularProvider.setBaseUrl('/api');

    // Send all Content-Type requests as application/json
    RestangularProvider.setDefaultHeaders({'Content-Type': 'application/json'});
});

CollaboratorApp.directive('focusMe', function($timeout, $parse) {
    return {
        //scope: true,   // optionally create a child scope
        link: function(scope, element, attrs) {
            var model = $parse(attrs.focusMe);
            scope.$watch(model, function(value) {
                console.log('value=',value);
                if(value === true) {
                    $timeout(function() {
                        element[0].focus();
                    });
                }
            });
        }
    };
});
