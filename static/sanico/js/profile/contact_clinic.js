function showResponse(response, statusText, xhr, $form){
    if(response['success'] === true) {
        //popup_message("Thank you for contacting us!");
        popup_redirect_now();
    } else {
        popup_error("Error sending your information!");
    }
    $form.clearForm();
}

var options = {
    dataType:  'json',
    success: showResponse,  // post-submit callback
    error: function showResponse(response, statusText, xhr, $form){
        popup_error("Error sending your information!");
    }
};

$(document).on('ready', function(){
    popup_redirect("/contact-clinic-thanks/");
    $("#contact-clinic-form").validate({
        submitHandler: function(form) {
            // open popup
            popup_open();

            $(form).ajaxSubmit(options);
        },
        rules: {
            email: {
                required: true,
                email: true
            },
            termsConditions: "required",
            desire: {
                required: function(element){
                    return ($("#find-list").val() === 'acquaintance');
                }
            }
        }
    });
});
