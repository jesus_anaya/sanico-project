$(document).on('ready', function(){
    try{
        var clinic = $("meta[name='clinic']").attr('content');

        $.get('/profile/get-about/' + clinic + '/', function(){}).done(function(result){
            $("#about").html(result);
        });

    } catch(err) {}
});