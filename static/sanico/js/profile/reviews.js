function ratingStars(){
    var inputs = [];
    var count = 0;

    function changeStars($this, e, active, input){
        var x =  e.pageX - $this.offset().left;
        if (active) {
            var start = 0;
            if(x > 7){
                start = parseInt((x + 20) / 20, 10) * 20;
            }

            console.log(start);

            $this.find('.editable.star-rating').css({
                'width': start
            });
            input.val(start / 10);
        }
    }

    $(".qualification").each(function(){
        inputs.push($(this));
    });

    $(".star-holder").each(function(){
        var active = false;
        var input = inputs[count++];

        $(this).on('mousemove', function(e){
            changeStars($(this), e, active, input);
        });

        $(this).on('mousedown', function(e){
            active = true;
            changeStars($(this), e, active, input);
        });

        $(this).on('mouseup', function(){
            active = false;
        });
    });
}

function showResponse(response, statusText, xhr, $form)  {
    $(".review-loading-img").css('display', 'none');
    if(response['success'] === true) {
        window.location.href = '/profile/review-thanks/';
    } else {
        alert("Error sending your review!");
    }
    $form.clearForm();
}

var options = {
    success: showResponse,  // post-submit callback
    dataType:  'json'
};

$(document).on('ready', function(){
    ratingStars();

    // $("#date_appointment").datepicker({
    //     dateFormat : 'yy-mm-dd'
    // });

    // $("#review-form").validate({
    //     submitHandler: function(form) {
    //         $(".review-loading-img").css('display', 'block');
    //         $(form).ajaxSubmit(options);
    //     },
    //     rules: {
    //         email: {
    //             required: true,
    //             email: true
    //         }
    //     }
    // });
});