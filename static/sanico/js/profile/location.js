
var map;
function map_initialize(){
    try{
        var geocoder = new google.maps.Geocoder();

        var latlng = $('#coordinates').val().split(',');
        var latLng = new google.maps.LatLng(latlng[0], latlng[1]);

        var mapOptions = {
            scaleControl: true,
            zoom: 17,
            center: latLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById('mapCanvas'), mapOptions);
        var marker = new google.maps.Marker({
            position: latLng,
            title: 'Point A',
            map: map,
            draggable: false
        });
        marker.setMap(map);
    } catch(err) {
        console.log('Error render the map');
    }
}

google.maps.event.addDomListener(window, 'load', map_initialize);