
$(document).on('ready', function(){
    $('.nav-profile ul > li').each(function(){
        if($(this).text() == $("meta[name='page']").attr("data")){
            $(this).addClass('active');
        }
    });

    // Deleted slide effect because hinder with the float tob bar effect
    $('html, body').animate(
        {scrollTop: $('.row.header-timeline').position().top - 107},
        (($("meta[name='page']").attr("data") === 'About Us') ? 600 : 0)
    );

    // Form Reward
    $('#find-list').bind('change', function (e) {
        if( $('#find-list').val() == "acquaintance") {
            $('#acquaintance-name').show();
        }
        else{
            $('#acquaintance-name').hide();
        }
    });

    $("#birthdaypicker").datepicker({
        dateFormat : 'yy-mm-dd',
        changeMonth : true,
        changeYear : true,
        yearRange: '-100y:c+nn'
    });

    $("#desire_appointment_date").datepicker({
        dateFormat : 'yy-mm-dd'
    });

});