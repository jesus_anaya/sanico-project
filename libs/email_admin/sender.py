from django.core.mail import EmailMultiAlternatives
from django.template import Template, Context
from django.utils.html import strip_tags
from django.conf import settings
from .models import Email


class Sender(object):
    queryset = Email.objects.all()

    def send(self, token, context={}, to_address=None):
        try:
            # get the email to send
            email = self.queryset.get(token=token)

            # define email destiny
            if email.to_address is None and to_address is None:
                raise Exception("Specified a destiny address is necessary")

            if to_address is None:
                 to_address = email.to_address

            # sent MEDIA_URL as default in context
            context['MEDIA_URL'] = settings.MEDIA_URL

            # render the template with the context
            html_content = Template(email.content).render(Context(context))
            text_content = strip_tags(html_content)

            # send message
            message = EmailMultiAlternatives(
                # Define email subject and body
                subject=email.subject,
                body=text_content,
                from_email=email.from_address,
                to=[to_address],

                # define email copies
                cc=[u"{0}".format(x) for x in email.cc_address.all()],
                bcc=[u"{0}".format(x) for x in email.bcc_address.all()]
            )

            # Attach html content
            message.attach_alternative(html_content, "text/html")

            # send email
            return message.send()

        except Email.DoesNotExist:
            print "Error sending the email with token: %s" % token


# Create a sender object to will be used
sender = Sender()
