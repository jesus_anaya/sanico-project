from django import forms
from .models import Email, EmailAccount


class EmailForm(forms.ModelForm):
    class Meta:
        model = Email

    cc_address= forms.ModelMultipleChoiceField(
            queryset=EmailAccount.objects.all(),
            widget=forms.CheckboxSelectMultiple(),
            required=False)

    bcc_address= forms.ModelMultipleChoiceField(
            queryset=EmailAccount.objects.all(),
            widget=forms.CheckboxSelectMultiple(),
            required=False)