# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'EmailAccount'
        db.create_table(u'email_admin_emailaccount', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
        ))
        db.send_create_signal(u'email_admin', ['EmailAccount'])

        # Adding model 'Email'
        db.create_table(u'email_admin_email', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('token', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('from_address', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('to_address', self.gf('django.db.models.fields.EmailField')(max_length=75, null=True, blank=True)),
            ('subject', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('content', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'email_admin', ['Email'])

        # Adding M2M table for field cc_address on 'Email'
        db.create_table(u'email_admin_email_cc_address', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('email', models.ForeignKey(orm[u'email_admin.email'], null=False)),
            ('emailaccount', models.ForeignKey(orm[u'email_admin.emailaccount'], null=False))
        ))
        db.create_unique(u'email_admin_email_cc_address', ['email_id', 'emailaccount_id'])

        # Adding M2M table for field bcc_address on 'Email'
        db.create_table(u'email_admin_email_bcc_address', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('email', models.ForeignKey(orm[u'email_admin.email'], null=False)),
            ('emailaccount', models.ForeignKey(orm[u'email_admin.emailaccount'], null=False))
        ))
        db.create_unique(u'email_admin_email_bcc_address', ['email_id', 'emailaccount_id'])


    def backwards(self, orm):
        # Deleting model 'EmailAccount'
        db.delete_table(u'email_admin_emailaccount')

        # Deleting model 'Email'
        db.delete_table(u'email_admin_email')

        # Removing M2M table for field cc_address on 'Email'
        db.delete_table('email_admin_email_cc_address')

        # Removing M2M table for field bcc_address on 'Email'
        db.delete_table('email_admin_email_bcc_address')


    models = {
        u'email_admin.email': {
            'Meta': {'object_name': 'Email'},
            'bcc_address': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'2'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['email_admin.EmailAccount']"}),
            'cc_address': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'1'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['email_admin.EmailAccount']"}),
            'content': ('django.db.models.fields.TextField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'from_address': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'to_address': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'token': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        },
        u'email_admin.emailaccount': {
            'Meta': {'object_name': 'EmailAccount'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['email_admin']