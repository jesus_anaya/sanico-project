

tinymce.init({
    selector:'textarea#id_content',
    height: 400,
    width: 800,
    plugins: ["code"],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons | code",
});
