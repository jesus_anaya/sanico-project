from django.contrib import admin
from .models import EmailAccount, Email
from .helper import hide_model
from .forms import EmailForm


class EmailAdmin(admin.ModelAdmin):
    class Media:
        js = (
            '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js',
            '//tinymce.cachefly.net/4.0/tinymce.min.js',
            '/static/email_admin/js/tinymce.js'
        )
    form = EmailForm
    list_display = ('title', 'token', 'from_address', 'to_address', 'description')

class EmailAccountAdmin(admin.ModelAdmin):

    # Remove EmailAccount model from admin list
    def in_menu(self):
        return hide_model('email_admin.EmailAccount')


admin.site.register(EmailAccount, EmailAccountAdmin)
admin.site.register(Email, EmailAdmin)
