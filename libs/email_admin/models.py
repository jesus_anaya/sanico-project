from django.db import models


class EmailAccount(models.Model):
    email = models.EmailField()
    name = models.CharField(max_length=200, blank=True, null=True)

    def __unicode__(self):
        return self.email


class Email(models.Model):
    title = models.CharField(max_length=200)
    token = models.CharField(max_length=100, unique=True)
    description = models.TextField(blank=True, null=True)
    from_address = models.EmailField()
    to_address = models.EmailField(blank=True, null=True)

    cc_address = models.ManyToManyField(
        EmailAccount, blank=True, null=True, related_name="1")

    bcc_address = models.ManyToManyField(
        EmailAccount, blank=True, null=True, related_name="2")

    subject = models.CharField(max_length=200)
    content = models.TextField()

    def __unicode__(self):
        return self.title
