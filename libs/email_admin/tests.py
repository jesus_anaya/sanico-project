"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from .models import Email, EmailAccount

class SimpleTest(TestCase):
    def setUp(self):
        pass

    def test_valid_email_model(self):
        """
        Test saving a valid email model
        """
        e = Email.objects.create(
            title="Title of my email",
            token="token-email",
            description="Descriotion from my email",
            from_address="jesus.anaya.dev@gmail.com",
            subject="Subject of my email",
            content="<h1>Ola ke Ase</h1>")

        self.assertTrue(e.id is not None)
