from django.conf import settings


def hide_model(model_name):
    """
    Hide from the admin menu unless explicitly set in ``ADMIN_MENU_ORDER``.
    """
    for (name, items) in settings.ADMIN_MENU_ORDER:
        if model_name in items:
            return True
    return False