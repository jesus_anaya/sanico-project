from django.db import models
from django.utils.translation import ugettext_lazy as _

from mezzanine.pages.models import Page
from mezzanine.core.fields import RichTextField
from mezzanine.core.models import Orderable, RichText
import forms


class FormPlusPage(Page, RichText):
    """
    A user-built form with options added.
    """
    class Meta:
        verbose_name = _("Form Plus")
        verbose_name_plural = _("Forms Plus")

    button_text = models.CharField(
        _("Button text"), max_length=50, default=_("Submit"))

    response = RichTextField(_("Response"))

    send_email = models.BooleanField(
        _("Send email to user"), default=True,
        help_text=_("To send an email to the email address supplied in "
                    "the form upon submission, check this box."))

    email_from = models.EmailField(
        _("From address"), blank=True,
        help_text=_("The address the email will be sent from"))

    email_copies = models.CharField(
        _("Send email to others"), blank=True,
        help_text=_("Provide a comma separated list of email addresses "
                    "to be notified upon form submission. Leave blank to "
                    "disable notifications."), max_length=200)

    email_subject = models.CharField(_("Subject"), max_length=200, blank=True)

    email_message = models.TextField(
        _("Message"), blank=True,
        help_text=_("Emails sent based on the above options will contain "
                    "each of the form fields entered. You can also enter "
                    "a message here that will be included in the email."))

    # slug field
    static_slug = models.CharField(max_length=100, blank=True, null=True)


class Field(Orderable):
    form_page = models.ForeignKey(FormPlusPage)
    label = models.CharField(max_length=200)
    name = models.CharField(max_length=100)
    type_field = models.IntegerField(choices=forms.NAMES)
    required = models.BooleanField()
    visible = models.BooleanField()
    choices = models.CharField(max_length=255, blank=True, null=True)
    default_value = models.CharField(max_length=200, default="")
    help_text = models.CharField(max_length=255, default="")
