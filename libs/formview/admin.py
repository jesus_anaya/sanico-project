from django.contrib import admin
from mezzanine.pages.admin import PageAdmin
from mezzanine.core.admin import TabularDynamicInlineAdmin
from .models import FormPlusPage, Field


class FieldAdmin(TabularDynamicInlineAdmin):
    model = Field


class FormPlusAdmin(PageAdmin):
    inlines = [FieldAdmin]
    list_display = ["title", "status", "email_copies"]
    list_display_links = ["title"]


admin.site.register(FormPlusPage, FormPlusAdmin)
