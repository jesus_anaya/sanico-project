from mezzanine.pages.page_processors import processor_for
from .models import FormPlusPage, Field
from .forms import ValidateForm

@processor_for(FormPlusPage)
def form_processor(request, page):
    '''
    Process form page
    '''
    if request.method == 'POST':
        fields = Field.objects.filter(form_page=page.id)

        ValidateForm()
