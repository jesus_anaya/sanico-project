from django.http import HttpResponse
from django.shorcuts import render_to_response
from django.utils import simplejson as json


def process_form(request):
    return HttpResponse(json.dumps([]), 'application/json')
