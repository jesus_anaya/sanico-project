from django.conf.urls import patterns, url
#from django.views.generic import RedirectView

urlpatterns = patterns(
    "formview.views",
    url(r"^process_form/$", "process_form", name="process_form"),
    #url(r"^accounts/login/$", RedirectView.as_view(url='/login/')),
)
