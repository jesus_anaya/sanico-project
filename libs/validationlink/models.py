from django.db import models
from random import randrange
from hashlib import sha1


class CodeManager(models.Manager):
    def code_generator(self):
        """
        Genetare a code using sha1 starting from a random generated code
        """
        randon_code = ""
        for i in range(10):
            irand = randrange(0, 40)
            randon_code += chr(80 + irand)

        #return encripted code by sha1
        return sha1(randon_code.encode('utf-8')).hexdigest()

    def generate(self, key):
        """
        Register we unique code in the db and get this code
        """
        #generate a unique code and register
        code = self.code_generator()
        self.create(code=code, key=key)

        #return the code generated
        return code

    def is_valid_code(self, code):
        return bool(self.filter(code=code, expired=False).count() == 1)

    def get_key(self, code):
        return self.get(code=code, expired=False).key


class ValidCode(models.Model):
    """
    Validate CodeModels, here save all code relates with an assigned key,
    that call a method associated with the key..
    """
    code = models.CharField(max_length=100)
    key = models.CharField(max_length=20)
    expired = models.BooleanField(default=False)

    objects = CodeManager()

    def __unicode__(self):
        return unicode("%s %s" % (self.code, self.key))
