from django.conf import settings as django_settings
from django.utils.importlib import import_module
from django.utils.module_loading import module_has_submodule


def initial_modules():
    for app in django_settings.INSTALLED_APPS:
        if not app.startswith('django.'):
            module = import_module(app)
            try:
                import_module("%s.vlcallbacks" % app)
            except ImportError:
                if module_has_submodule(module, "vlcallbacks"):
                    raise

initial_modules()
