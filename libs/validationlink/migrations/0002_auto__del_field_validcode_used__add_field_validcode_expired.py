# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'ValidCode.used'
        db.delete_column(u'validationlink_validcode', 'used')

        # Adding field 'ValidCode.expired'
        db.add_column(u'validationlink_validcode', 'expired',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'ValidCode.used'
        db.add_column(u'validationlink_validcode', 'used',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Deleting field 'ValidCode.expired'
        db.delete_column(u'validationlink_validcode', 'expired')


    models = {
        u'validationlink.validcode': {
            'Meta': {'object_name': 'ValidCode'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'expired': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        }
    }

    complete_apps = ['validationlink']