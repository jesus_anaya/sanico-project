from django.conf.urls.defaults import patterns, url

urlpatterns = patterns(
    'validationlink.views',
    url('^read/$', 'read_link', name='read'),
    url('^succes/$', 'link_success', name='success'),
    url('^error/$', 'link_error', name='error')
)
