from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
import link


def read_link(request):
    if request.method == 'GET':
        code = request.GET.get('code')

        # validate that the code is included in the url
        if not code:
            raise Http404
        else:
            if link.code.is_valid_code(code):
                # convert get request to dict
                data = request.GET.dict()
                data.pop('code')

                #exires the code if the url not have noexpire flag
                noexpire = data.get('noexpire')
                if noexpire:
                    data.pop('noexpire')

                response = link.code.get_callback(code)(request, data)

                if not noexpire:
                    link.code.expire(code)

                if response:
                    return response
                else:
                    return HttpResponseRedirect(reverse('validation:success'))
            else:
                return HttpResponseRedirect(reverse('validation:error'))
    else:
        raise Http404


def link_success(request):
    return HttpResponse("<h1>Link Success</h1>")


def link_error(request):
    return HttpResponse("<h1>Link Error</h1>")
