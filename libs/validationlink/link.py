from django.core.urlresolvers import reverse
from django.contrib.sites.models import Site
from .models import ValidCode


class ListenCodeValidate(object):
    callback_list = []

    def key_exist(self, key):
        for item in self.callback_list:
            if item[0] == key:
                return True
        return False

    def expire(self, code):
        try:
            vcode = ValidCode.objects.get(code=code)
            vcode.expired = True
            vcode.save()
        except ValidCode.DoesNotExist:
            print "Error expired code: %s" % code

    def register(self, callback, key):
        if self.key_exist(key):
            raise NameError("Key %s is already used" % key)

        #if the key is not used register the new callback
        self.callback_list.append((key, callback))

    def is_valid_code(self, code):
        return ValidCode.objects.is_valid_code(code)

    def get_key(self, code):
        return ValidCode.objects.get_key(code)

    def generate_link(self, key, use_ssl=False, noexpire=False):
        # create a new code for we key in the db
        code = ValidCode.objects.generate(key)

        # get the validation link
        link = reverse('validation:read')

        #get the site url
        site = Site.objects.get_current()

        #return the validate simple link
        return "http%s://%s%s?code=%s%s" % (
            's' if use_ssl else '', site, link, code,
            '&noexpire=1' if noexpire else '')

    def get_callback(self, code):
        key = self.get_key(code)
        for item in self.callback_list:
            if item[0] == key:
                return item[1]
        raise KeyError("The key selected not exist")


code = ListenCodeValidate()
