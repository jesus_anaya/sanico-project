import os
from .models import  Widget
from .utilities import get_widget_model_queryset
from .widget_pool import get_widget

from django.template.loader import render_to_string, get_template_from_string
from django.template import Template


def render_template(context, template, raw=False):
    """
    Renders any type of template, text or file or whatever
    """
    rendered = ''
    if raw:
        template_object = get_template_from_string(template, name="custom_template_" + os.urandom(32))
        rendered = template_object.render(context)
    else:
        if isinstance(template, str):
            rendered = render_to_string(template, context)
        if isinstance(template, Template):
            rendered = template.render(context)
    return rendered


def render_widgets_for_slot(slot, widget_context):
    """
    Renders all widgets assigned to a WidgetSlot
    """

    page = widget_context.get("page", None)
    user = widget_context["request"].user
    #regular widgets
    slot_widgets = Widget.objects.published(user)\
                    .filter(widgetslot=slot,\
                        page=page, page_less=False).order_by('_order')
    #Some widgets are not bound to one page, logo widget
    page_less_widgets = Widget.objects.published(user)\
                .filter(widgetslot=slot, page_less=True)
    rendered_widgets = []
    "Render regular paged widgets and page less widgets (universal widgets)"
    widgets = slot_widgets | page_less_widgets
    for widget in widgets:
        widget_class = get_widget(widget.widget_class)
        widget_context.update({'widget': widget})
        if widget_class is not None:
            widget_options = None
            widget_class = widget_class()
            if hasattr(widget_class, "options"):
                try:
                    widget_options = dict(((o["name"], o["value"])\
                         for o in widget.options.values("name", "value")))
                    widget_context.update({"opts": widget_options})
                except Exception, e:
                    raise e
            queryset = get_widget_model_queryset(widget, widget_class)
            rendered_widget = render_template(widget_class\
                    ._render(widget_context, slot, queryset, widget_options),\
                              widget_class.template, raw=widget_class.raw)


            rendered_widgets.append({'widget': widget, \
                                    'meta': widget_class.Meta, \
                                    'content': rendered_widget})

    return rendered_widgets


