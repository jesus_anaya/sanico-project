# Create your views here.
import re


class Filter(object):
    FORVIDEN_WORDS = [
        'www', '.com', '.net', '.org', '.info', '.co', '.mx',
        '@', 'hotmail', 'gmail', 'email', 'cell', 'yahoo', 'aol',
        'av.', 'address', 'st.', 'phone', 'street', 'call', 'call us',
        'avenue', 'boulevard', 'blvd', 'more info', 'http'
    ]

    text = ""
    forbidden_words = []

    def set_text(self, text):
        self.text = "".join(text.split()).replace('-', '')

    # My number phone is: 6862-154054
    def find_phone_number(self):
        key = r"(\d{3}[-\.\s]??\d{3}[-\.\s]??\d{4}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4})"
        compiled = re.compile(key)
        result = bool(compiled.search(self.text.lower()))
        if result:
            self.forbidden_words += ['The text have a phone number.']
        return result

    def word_filter(self):
        self.forbidden_words = filter(lambda x: x in self.text, self.FORVIDEN_WORDS)
        return len(self.forbidden_words) > 0

    def is_invalid_text(self):
        return bool(self.find_phone_number() or self.word_filter())

    def get_forbidden_words(self):
        return self.forbidden_words

text_filter = Filter()
