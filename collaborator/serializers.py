from rest_framework import serializers
from profile.models import ClinicProcedure


class ClinicProcedureSerializer(serializers.HyperlinkedModelSerializer):
    """
    """
    class Meta:
        model = ClinicProcedure
        fields = ('clinic', 'description', 'procedure', 'price', 'duration', 'position')
