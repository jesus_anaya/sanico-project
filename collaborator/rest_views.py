from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.http import Http404
from rest_framework import viewsets
from rest_framework.response import Response
from profile.models import ClinicProcedure
from mezzanine.pages.models import Page
from .serializers import ClinicProcedureSerializer


class CollBaseViewSet(viewsets.ModelViewSet):
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        """
        Dispatch methods only when the user is logged
        """
        return super(CollBaseViewSet, self).dispatch(*args, **kwargs)

    def list(self, request, *args, **kwargs):
        serializer = self.serializer_class(self.get_queryset(), many=True)
        return Response(serializer.data)

    def get_serializer_context(self):
        try:
            page = Page.objects.with_ascendants_for_slug("collaborator")[0]
        except IndexError:
            raise Http404
        context = super(CollBaseViewSet, self).get_serializer_context()
        context['page'] = page

        return context


class ClinicProcedureViewSet(CollBaseViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    serializer_class = ClinicProcedureSerializer
    queryset = ClinicProcedure.objects.all()

    def get_queryset(self):
        """
        This view should return a list of all the purchases
        for the currently authenticated user.
        """
        clinic_id = self.request.session.get('clinic')
        if clinic_id:
            return ClinicProcedure.objects.filter(clinic=clinic_id)
        else:
            return ClinicProcedure.objects.none()


