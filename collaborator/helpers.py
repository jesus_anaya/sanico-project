from django.http import HttpResponse
from django.utils import simplejson as json
from profile.models import Clinic
from sanico.email import SendEmail


def get_session_clinic(request):
    try:
        if request.session['clinic']:
            return Clinic.objects.get(id=request.session['clinic'])
        return None
    except KeyError:
        return None
    except Clinic.DoesNotExist:
        return None


def return_result(result):
    return HttpResponse(
        json.dumps({'success': result}), 'application/json; charset=utf-8')


def send_forviden_words(clinic, words=[]):
    email = SendEmail()
    email.notify_message(
        'Forbidden words in clinic: %s' % clinic.name,
        'emails/send_forbidden_words.html',
        {'words': words, 'clinic_name': clinic.name})
    print "Email send: ", email.send()
