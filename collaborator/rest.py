from django.utils import simplejson as json
from django.http import HttpResponse, Http404
from django.views.generic import ListView
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect
from profile.models import TreatmentPrice, Treatment, Doctor, Clinic, Review, TreatmentCategory
from profile.models import ComplementaryService, SpecialPromotion, ClinicProcedure, Procedure
from profile.forms import DoctorForm, ClinicProcedureForm
from .angular import AngularBaseRest
from .helpers import get_session_clinic


class SanicoAngularBaseRest(AngularBaseRest):
    def create_object(self, request, model_obj=None, **kwargs):
        kwargs['data'] = request.POST.dict()
        kwargs['data']['clinic'] = request.session['clinic']

        return super(SanicoAngularBaseRest, self).create_object(request, model_obj, **kwargs)


class PricesRest(AngularBaseRest):
    """
    Price class Angular REST
    """
    model = TreatmentPrice
    display_fields = ['clinic', 'treatment', 'procedure', 'price', 'duration', 'position']

    def create_object(self, request, model_obj=None, **kwargs):
        data = request.POST.dict()
        data['clinic'] = request.session['clinic']
        kwargs['data'] = data
        return super(PricesRest, self).create_object(request, model_obj, **kwargs)

    def has_add_permission(self, request):
        try:
            permission = super(PricesRest, self).has_add_permission(request)
            tre = Treatment.objects.get(id=int(request.POST.get('treatment', 0)))
            return permission and bool(tre.clinic.id == request.session['clinic'])
        except:
            return False

    def has_update_permission(self, request, obj):
        try:
            permission = super(PricesRest, self).has_update_permission(request, obj)
            data = json.loads(request.body)
            tre = Treatment.objects.get(id=int(data.get('treatment', 0)))
            data_permissions = bool(tre.clinic.id == request.session['clinic'])
            return permission and data_permissions
        except:
            return False


class ProcedureRest(SanicoAngularBaseRest):
    """
    ProcedureRest class Angular REST, using in Collaborator view, Price
    """
    model = ClinicProcedure
    form = ClinicProcedureForm

    # Asign the fields requierd in the page
    #
    display_fields = [
        'clinic',
        'description',
        'procedure',
        'price',
        'duration',
        'procedure_id',
        'position'
    ]

    use_only_ajax = False

    def put(self, request, id=None, **kwargs):
        return super(ProcedureRest, self).put(request, id, **kwargs)

    def post(self, request, *args, **kwargs):
        procedures = request.POST.getlist('procedures[]')

        if procedures:
            try:
                clinic = Clinic.objects.get(id=request.session["clinic"])
            except Clinic.DoesNotExist:
                raise Http404

            for procedure in procedures:
                try:
                    cp = ClinicProcedure(
                        clinic=clinic, description="",
                        procedure=Procedure.objects.get(id=int(procedure)),
                        price="Undefined", duration="Undefined")
                    cp.save()
                except Procedure.DoesNotExist:
                    pass
            return self.get_objects(request)
        else:
            return super(ProcedureRest, self).post(request, **kwargs)

    # Get a single procedure by id
    #
    def get_object(self, request, model_obj):
        data = self.serialize(model_obj, ['id'] + self.display_fields)
        return HttpResponse(self.json_encode(data), 'application/json')

    # Get all treatmens for price section in collaborator page
    #
    def get_objects(self, request):
        clinic = Clinic.objects.get(id=request.session["clinic"])

        # If category is Hospital get all procedures, else get treatments by
        # clinic category
        #
        if clinic.category.name == "Hospital":
            categories = TreatmentCategory.objects.all()
        else:
            categories = TreatmentCategory.objects.filter(clinic_category=clinic.category)

        # Get all procedures by clinic
        #
        clinic_procedure = ClinicProcedure.objects.filter(clinic=clinic)

        # Get procedures filtered by clinic
        #
        pfiltered = lambda x: clinic_procedure.filter(procedure__category=x)

        # Get all information with a format ordenable
        #
        treatments = filter(lambda x: x, [{'category': unicode(x), 'category_id': x.id, 'list': [
            self.serialize(i, ['id'] + self.display_fields) for i in pfiltered(
                x)]} if pfiltered(x).exists() else None for x in categories])

        return HttpResponse(self.json_encode(treatments), 'application/json')


class TreatmentRest(SanicoAngularBaseRest):
    """
    Treatment class Angular REST
    """
    model = Treatment
    display_fields = ['clinic', 'name', 'position']
    use_only_ajax = False

    def get_prices(self, model_obj):
        display_fields = ['clinic', 'treatment', 'procedure', 'price', 'duration', 'position']
        ts = TreatmentPrice.objects.filter(
            clinic=model_obj.clinic.id, treatment=model_obj.id)

        return [self.serialize(obj, ['id'] + list(display_fields)) for obj in ts]

    def get_object(self, request, model_obj):
        data = self.serialize(model_obj, ['id'] + list(self.display_fields))
        data['prices'] = self.get_prices(model_obj)
        return HttpResponse(self.json_encode(data), 'application/json')

    def get_objects(self, request):
        data = []
        for obj in self.model.objects.filter(clinic=request.session["clinic"]):
            obj_data = self.serialize(obj, ['id'] + list(self.display_fields))
            obj_data['prices'] = self.get_prices(obj)
            data.append(obj_data)

        return HttpResponse(self.json_encode(data), 'application/json')

    def update_object(self, request, model_obj, **kwargs):
        model_obj.prices = self.get_prices(model_obj)
        kwargs['extra_fields'] = ['prices']

        return super(TreatmentRest, self).update_object(request, model_obj, **kwargs)


class DoctorRest(SanicoAngularBaseRest):
    model = Doctor
    form = DoctorForm
    display_fields = [
        'clinic', 'photo', 'first_name', 'last_name',
        'education', 'specialisations', 'languages', 'resume']


class OverviewRest(AngularBaseRest):
    model = Clinic

    def get(self, request, id=None, **kwargs):
        offer = False
        clinic = get_session_clinic(request)

        if clinic:
            offer = clinic.offer if clinic.offer != "" else False
            name, testimonial = clinic.get_testimonial()

        return HttpResponse(
            json.dumps(
                {
                    'offer': offer,
                    'testimonial': testimonial,
                    'testimonial_name': name
                }), "application/json")

    def put(self, request, id=None, **kwargs):
        offer = False
        if request.method == "PUT":
            clinic = get_session_clinic(request)
            if clinic:
                data = json.loads(request.body)
                clinic.offer = data.get('offer', '')
                clinic.save()
                offer = clinic.offer if clinic.offer != "" else False
        return HttpResponse(
            json.dumps({'offer': offer}), "application/json")


class ComplementaryServiceRest(SanicoAngularBaseRest):
    """
    ComplementaryService class Angular REST
    """
    model = ComplementaryService
    display_fields = ['clinic', 'name']


class SpecialPromotionRest(SanicoAngularBaseRest):
    """
    ComplementaryService class Angular REST
    """
    model = SpecialPromotion
    display_fields = ['clinic', 'name']


@csrf_protect
@login_required
def update_position(request):
    if request.method == 'POST' and request.is_ajax():
        #try:
        if True:
            if request.POST.get('type') == 'price':
                instance = TreatmentPrice.objects.get(id=int(request.POST.get('id')))
            elif request.POST.get('type') == 'treatment':
                instance = Treatment.objects.get(id=int(request.POST.get('id')))
            instance.position = int(request.POST.get('position'))
            instance.save()
        #except:
            #raise Http404
    return HttpResponse(json.dumps({'success': True}))


@csrf_protect
@login_required
def testimonial(request):
    if request.method == 'POST' and request.is_ajax():
        selected = request.POST.get('review_selected')
        clinic = get_session_clinic(request)
        try:
            for review in Review.objects.filter(clinic=request.session['clinic']):
                if review.id == int(selected):
                    review.for_testimonial = True
                else:
                    review.for_testimonial = False
                review.save()
            testimonial_name, testimonial = clinic.get_testimonial()
        except:
            testimonial_name, testimonial = ('', '')
        return HttpResponse(json.dumps(
            {
                'testimonial': testimonial,
                'testimonial_name': testimonial_name
            }), "application/json")
    else:
        raise Http404
