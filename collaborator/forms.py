from django import forms
from django.forms import ModelForm
from profile.models import Clinic


class CollaboratorAboutForm(ModelForm):
    class Meta:
        model = Clinic
        fields = [
            'about', 'open_since', 'personal_checks', 'traveler_checks',
            'cash', 'mastercard', 'visa', 'american_express', 'discover',
            'paypal', 'money_order', 'wire_transfer', 'cashier_check',
            'english', 'spanish', 'french', 'japanese', 'italian', 'russian',
            'mandarin', 'portuguese', 'german']

    about = forms.CharField(required=False)
    open_since = forms.CharField(required=False)
