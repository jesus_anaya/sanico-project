from django.views.decorators.csrf import csrf_protect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.conf import settings
from sanico.models import SanicoUser
from profile.models import OpeningHour, ClinicPhoto, Review, Location, Doctor, Clinic
from profile.models import TreatmentPrice, Treatment
from libs.spamfilter.filters import text_filter
from .helpers import get_session_clinic, return_result, send_forviden_words
from .forms import CollaboratorAboutForm


@csrf_protect
@login_required(login_url='/login/')
def change_password(request):
    result = False
    if request.method == 'POST':
        try:
            user = SanicoUser.objects.get(id=request.user.id)
            user.set_password(request.POST.get('password'))
            user.save()
            result = True
        except SanicoUser.DoesNotExist:
            pass
    return return_result(result)


@csrf_protect
@login_required(login_url='/login/')
def add_to_front(request):
    result = False
    if request.method == 'POST':
        try:
            photo = ClinicPhoto.objects.get(
                clinic=request.session['clinic'],
                id=request.POST.get('image'))
            photo.in_front = bool(request.POST.get('in_front'))
            photo.save()
            result = True
        except:
            pass
    return return_result(result)


@csrf_protect
@login_required(login_url='/login/')
def render_photos(request):
    clinic = get_session_clinic(request)
    context = {
        'images': ClinicPhoto.objects.filter(clinic=clinic.id)
    }

    return render_to_response(
        "collaborator/tags/gallery.html", context,
        context_instance=RequestContext(request))


@csrf_protect
@login_required(login_url='/login/')
def add_photo(request):
    result = False
    print request.FILES
    if request.method == 'POST':
        photo = ClinicPhoto(
            clinic=get_session_clinic(request),
            photo=request.FILES.get('photo'))
        photo.save()
        result = True
    return return_result(result)


@csrf_protect
@login_required(login_url='/login/')
def del_photo(request):
    result = False
    clinic = get_session_clinic(request)

    if request.method == 'POST':
        try:
            image_id = request.POST.get('image')
            ClinicPhoto.objects.get(
                id=image_id, clinic=clinic.id).delete()
            result = True
        except:
            pass
    return return_result(result)


@csrf_protect
@login_required(login_url='/login/')
def alter_about(request):
    result = False
    clinic = get_session_clinic(request)

    if request.method == 'POST' and clinic:

        text_filter.set_text(request.POST.get('about', ''))

        if text_filter.is_invalid_text():
            send_forviden_words(clinic, text_filter.get_forbidden_words())

        # save hours data
        open_hours = request.POST.getlist('hour_open')[::-1]
        close_hours = request.POST.getlist('hour_close')[::-1]

        for day in OpeningHour.objects.filter(clinic=clinic.id):
            day.hour_open = open_hours.pop()
            day.hour_close = close_hours.pop()
            if day.hour_open == '':
                day.hour_open = None
            if day.hour_close == '':
                day.hour_close = None
            day.save()

        # save clinic about
        about_form = CollaboratorAboutForm(request.POST, instance=clinic)
        if about_form.is_valid():
            about_form.save()
            result = True
    return return_result(result)


@csrf_protect
@login_required(login_url='/login/')
def save_images(request):
    result = False
    clinic = get_session_clinic(request)

    if request.method == 'POST' and clinic:
        if request.FILES.get('header_image'):
            clinic.image_header = request.FILES.get('header_image')
        if request.FILES.get('profile_image'):
            clinic.image_profile = request.FILES.get('profile_image')
        clinic.save()
        result = True
    return return_result(result)


@login_required(login_url='/login/')
def render_overview(request):
    if request.is_ajax():
        reviews_num = Review.objects.filter(
            clinic=request.session['clinic'], enable=True).count()
        photos_num = ClinicPhoto.objects.filter(
            clinic=request.session['clinic']).count()

        context = {
            'clinic': get_session_clinic(request),
            'MEDIA_URL': settings.MEDIA_URL,
            'reviews_num': reviews_num,
            'photos_num': photos_num
        }

        return render_to_response(
            "collaborator/tags/render_overview.html", context,
            context_instance=RequestContext(request))
    return Http404


@login_required(login_url='/login/')
def render_heade_images(request):
    if request.is_ajax():
        context = {
            'clinic': get_session_clinic(request),
            'MEDIA_URL': settings.MEDIA_URL
        }

        return render_to_response(
            "collaborator/tags/overview_header_images.html", context,
            context_instance=RequestContext(request))
    return Http404


## Save testimonail select in Overview profile_image
@login_required(login_url='/login/')
def select_testimonial(request):
    result = False

    if request.method == 'POST' and request.is_ajax():
        selected = request.POST.get('selected')
        try:
            for review in Review.objects.filter(clinic=request.session['clinic']):
                if review.id == int(selected):
                    review.for_testimonial = True
                else:
                    review.for_testimonial = False
                review.save()
            result = True
        except:
            pass
    return return_result(result)


## Save testimonail select in Overview profile_image
@login_required(login_url='/login/')
def save_overview(request):
    result = False

    if request.method == 'POST' and request.is_ajax():
        clinic = get_session_clinic(request)
        clinic.offer = request.POST.get('offer', '')
        clinic.save()
        result = True
    return return_result(result)


## Save location select in Overview profile_image
@csrf_protect
@login_required(login_url='/login/')
def save_location(request):
    result = False

    if request.method == 'POST' and request.is_ajax():
        try:
            location = Location.objects.get_or_create(clinic=get_session_clinic(request))[0]
            location.link = request.POST.get('link', '')
            if request.FILES.get('image_location_1'):
                location.image_map_1 = request.FILES.get('image_location_1')
            if request.FILES.get('image_location_2'):
                location.image_map_2 = request.FILES.get('image_location_2')
            location.save()
            result = True
        except:
            print "Error save location from collaborator page"
            raise
    return return_result(result)


## Save doctors select in Overview profile_image
@csrf_protect
@login_required(login_url='/login/')
def delete_doctor(request):
    result = False
    if request.method == 'POST' and request.is_ajax():
        if request.POST.get('doctor_id'):
            doctor = Doctor.objects.get(
                id=int(request.POST.get('doctor_id')),
                clinic=request.session['clinic'])
            doctor.delete()
        result = True

    return return_result(result)


## Save doctors select in Overview profile_image
@csrf_protect
@login_required(login_url='/login/')
def save_doctor(request):
    result = False

    if request.method == 'POST' and request.is_ajax():
        try:
            if request.POST.get('doctor_id'):
                doctor = Doctor.objects.get(
                    id=int(request.POST.get('doctor_id')),
                    clinic=request.session['clinic'])
            else:
                doctor = Doctor()
                doctor.clinic = Clinic.objects.get(
                    id=request.session['clinic'])

            doctor.first_name = request.POST.get('first_name')
            doctor.last_name = request.POST.get('last_name')
            doctor.education = request.POST.get('education')
            doctor.specialisations = request.POST.get('specialisations')
            doctor.languages = request.POST.get('languages')

            if request.FILES.get('photo'):
                doctor.photo = request.FILES.get('photo')

            doctor.save()
            result = True
        except:
            pass

    return return_result(result)


@login_required(login_url='/login/')
def render_doctors(request):
    if request.is_ajax():
        context = {
            'doctors': Doctor.objects.filter(
                clinic=request.session["clinic"])
        }
        return render_to_response(
            "collaborator/tags/render_doctors.html", context,
            context_instance=RequestContext(request))
    return Http404


## Save and delete treatments
@csrf_protect
@login_required(login_url='/login/')
def save_treatment(request):
    result = False

    if request.method == 'POST' and request.is_ajax():
        try:
            if request.POST.get('treatment_id'):
                treatment = Treatment.objects.get(
                    id=int(request.POST.get('treatment_id')),
                    clinic=request.session['clinic'])
            else:
                treatment = Treatment()
                treatment.clinic = Clinic.objects.get(
                    id=request.session['clinic'])

            treatment.name = request.POST.get('name', 'Nuevo Tratamiento')
            treatment.save()
            result = treatment.id
        except:
            pass

    return return_result(result)


@csrf_protect
@login_required(login_url='/login/')
def delete_treatment(request):
    result = False
    clinic_id = request.session['clinic']

    if request.method == 'POST' and request.is_ajax():
        try:
            if request.POST.get('treatment_id'):
                treatment = Treatment.objects.get(
                    id=int(request.POST.get('treatment_id')),
                    clinic=clinic_id)

                for price in TreatmentPrice.objects.filter(clinic=clinic_id, treatment=treatment.id):
                    price.delete()

                treatment.delete()
                result = True
        except:
            pass

    return return_result(result)


## Save and delete prices
@csrf_protect
@login_required(login_url='/login/')
def save_price(request):
    result = False

    if request.method == 'POST' and request.is_ajax():
        try:
            treatment = Treatment.objects.get(
                id=int(request.POST.get('treatment_id')),
                clinic=request.session['clinic'])

            if request.POST.get('price_id'):
                price = TreatmentPrice.objects.get(
                    id=int(request.POST.get('price_id')),
                    clinic=request.session['clinic'],
                    treatment=treatment.id)
            else:
                price = TreatmentPrice()
                price.treatment = treatment
                price.clinic = Clinic.objects.get(
                    id=request.session['clinic'])

            price.procedure = request.POST.get('procedure', '')
            price.price = request.POST.get('price', 0.0)
            price.duration = request.POST.get('duration', '')
            price.save()
            result = price.id
        except:
            pass

    return return_result(result)


@csrf_protect
@login_required(login_url='/login/')
def delete_price(request):
    result = False

    if request.method == 'POST' and request.is_ajax():
        try:
            if request.POST.get('treatment_id'):
                treatment = Treatment.objects.get(
                    id=int(request.POST.get('treatment_id')),
                    clinic=request.session['clinic'])

                if request.POST.get('price_id'):
                    price = TreatmentPrice.objects.get(
                        id=int(request.POST.get('price_id')),
                        clinic=request.session['clinic'],
                        treatment=treatment.id)
                    price.delete()
                    result = True
        except:
            pass

    return return_result(result)
