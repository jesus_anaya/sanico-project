"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from mezzanine.utils.tests import TestCase
#from profile.models import ClinicProcedure


class SimpleTest(TestCase):
    fixtures = ['clinics.json']

    def setUp(self):

        self.assertTrue(self.client.login(username="beto", password="1234"))

        session = self.client.session
        session['clinic'] = 1
        session.save()

        self.assertEqual(self.client.session['clinic'], 1)

    def test_rest_procedure_change(self):
        """
        Test rest procedure change
        """

        response = self.client.get('/collaborator/rest/procedure')
        # {
        #     "description": "Description 1",
        #     "price": "3000",
        #     "duration": "3 days",
        # })

        self.assertEqual(response.status_code, 201)
