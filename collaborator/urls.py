from django.contrib.auth.decorators import login_required
from django.conf.urls import patterns, url
from .rest import PricesRest, TreatmentRest, DoctorRest, OverviewRest, ProcedureRest
from .rest import ComplementaryServiceRest, SpecialPromotionRest


#Collaborator url resolves
urlpatterns = patterns(
    "collaborator.views",
    url(r"^$", 'general', name='general'),
    url(r"^overview/$", 'overview', name='overview'),
    url(r"^gallery/$", 'gallery', name='gallery'),
    url(r"^location/$", 'location', name='location'),
    url(r"^doctors/$", 'doctors', name='doctors'),
    url(r"^prices/$", 'prices', name='prices'),
    url(r"^about/$", 'about', name='about'),
    url(r"^select_clinic/(?P<id>\d+)/$", 'select_clinic', name='select_clinic'),
    url(r"^change_clinic/$", 'change_clinic', name='change_clinic'),
    url(r"^change_required/$", 'send_change_required', name='change_required'),
    url(r"^add-treatment/$", 'add_treatment', name='add_treatment'),
)

#API responses
urlpatterns += patterns(
    "collaborator.api",
    url(r"^change_password/$", 'change_password', name='change_password'),
    url(r"^add_photo/$", 'add_photo', name='add_photo'),
    url(r"^del_photo/$", 'del_photo', name='del_photo'),
    url(r"^alter_about/$", 'alter_about', name='alter_about'),
    url(r"^api/render_photos/$", 'render_photos', name='render_photos'),
    url(r"^api/add_to_front/$", 'add_to_front', name='add_to_front'),
    url(r"^api/save_images/$", 'save_images', name='save_images'),
    url(r"^api/render_heade_images/$", 'render_heade_images', name='render_heade_images'),
    url(r"^api/render_overview/$", 'render_overview', name='render_overview'),
    url(r"^api/select_testimonial/$", 'select_testimonial', name='select_testimonial'),
    url(r"^api/save_overview/$", 'save_overview', name='save_overview'),
    url(r"^api/save_location/$", 'save_location', name='save_location'),
    url(r"^api/save_doctor/$", 'save_doctor', name='save_doctor'),
    url(r"^api/delete_doctor/$", 'delete_doctor', name='delete_doctor'),
    url(r"^api/render_doctors/$", 'render_doctors', name='render_doctors'),
    url(r"^api/save_treatment/$", 'save_treatment', name='save_treatment'),
    url(r"^api/delete_treatment/$", 'delete_treatment', name='delete_treatment'),
    url(r"^api/save_price/$", 'save_price', name='save_price'),
    url(r"^api/delete_price/$", 'delete_price', name='delete_price'),
)

#RESTful url responses
urlpatterns += patterns(
    "",
    url(r"^rest/price/(?P<id>\d+)$", login_required(PricesRest.as_view()), name='price_details'),
    url(r"^rest/price$", login_required(PricesRest.as_view()), name='price'),

    url(r"^rest/treatment/(?P<id>\d+)$", login_required(TreatmentRest.as_view()), name='treatment_details'),
    url(r"^rest/treatment$", login_required(TreatmentRest.as_view()), name='treatment'),

    url(r"^rest/procedure/(?P<id>\d+)$", login_required(ProcedureRest.as_view()), name='procedure_details'),
    url(r"^rest/procedure$", login_required(ProcedureRest.as_view()), name='procedure'),

    url(r"^rest/doctor/(?P<id>\d+)$", login_required(DoctorRest.as_view()), name='doctor_details'),
    url(r"^rest/doctor$", login_required(DoctorRest.as_view()), name='doctor'),

    url(r"^rest/overview$", login_required(OverviewRest.as_view()), name='rest_overview'),

    url(r"^rest/complementary_service/(?P<id>\d+)$", login_required(ComplementaryServiceRest.as_view()), name='complementary_service'),
    url(r"^rest/complementary_service$", login_required(ComplementaryServiceRest.as_view()), name='complementary_service'),

    url(r"^rest/special_promotion/(?P<id>\d+)$", login_required(SpecialPromotionRest.as_view()), name='special_promotion'),
    url(r"^rest/special_promotion$", login_required(SpecialPromotionRest.as_view()), name='special_promotion'),

    url(r"^rest/testimonial$", "collaborator.rest.testimonial", name='testimonial'),

    url(r"^rest/update_position$", "collaborator.rest.update_position", name='update_position'),

)
