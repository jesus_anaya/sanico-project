from django.views.generic import View
from django.utils import simplejson as json
from django.core.serializers.json import DjangoJSONEncoder
from django.shortcuts import get_object_or_404
from django.http import HttpResponse, HttpResponseForbidden, Http404, HttpResponseBadRequest
from django.forms.models import modelform_factory


class AngularBaseRest(View):
    """
    Angular REST Base Class
    """
    model = None
    display_fields = []
    fields = []
    form = None
    use_only_ajax = False

    def __init__(self, *args, **kwargs):
        super(AngularBaseRest, self).__init__(*args, **kwargs)

    def get(self, request, id=None, **kwargs):
        # validate ajax if is necessary
        if self.use_only_ajax and not request.is_ajax():
            raise Http404

        if id:
            model_obj = get_object_or_404(self.model, id=id)
            return self.get_object(request, model_obj)
        else:
            return self.get_objects(request)

    def get_object(self, request, model_obj):
        data = self.serialize(model_obj, ['id'] + list(self.display_fields))
        return HttpResponse(self.json_encode(data), 'application/json')

    def get_objects(self, request):
        qs = self.model.objects.filter(clinic=request.session["clinic"])
        data = [self.serialize(obj, ['id'] + list(self.display_fields)) for obj in qs]
        return HttpResponse(self.json_encode(data), 'application/json')

    def post(self, request, id=None, **kwargs):
        # validate ajax if is necessary
        if self.use_only_ajax and not request.is_ajax():
            raise Http404

        if id:
            model_obj = get_object_or_404(self.model, id=id)
        else:
            model_obj = None
        return self.create_object(request, model_obj, **kwargs)

    def create_object(self, request, model_obj=None, **kwargs):
        if not self.has_add_permission(request):
            return HttpResponseForbidden(
                'You do not have permission to perform this action.')
        else:
            if request.method == "POST":
                if kwargs.get('data'):
                    data = kwargs.get('data')
                else:
                    data = request.POST.dict()

                if model_obj is not None:
                    form = self.get_form_instance(
                        request, data=data, files=request.FILES, instance=model_obj)
                else:
                    form = self.get_form_instance(
                        request, data=data, files=request.FILES, instance=self.model())

                if form.is_valid():
                    obj = form.save()

                    response = self.get_object(request, obj)
                    response.status_code = 201
                    return response
                return HttpResponseBadRequest(
                    self.json_encode(form.errors), content_type='application/json')

    def put(self, request, id=None, **kwargs):
        # validate ajax if is necessary
        if self.use_only_ajax and not request.is_ajax():
            raise Http404

        model_obj = get_object_or_404(self.model, id=id)
        return self.update_object(request, model_obj, **kwargs)

    def update_object(self, request, model_obj, **kwargs):
        if id and request.method == "PUT":
            try:
                if kwargs.get('data'):
                    data = kwargs.get('data')
                else:
                    data = json.loads(request.body)
            except ValueError:
                return HttpResponseBadRequest('Unable to parse JSON request body.')

            if not self.has_update_permission(request, model_obj):
                return HttpResponseForbidden(
                    'You do not have permission to perform this action.')
            else:
                form = self.get_form_instance(
                    request, data=data, instance=model_obj)

                if form.is_valid():
                    fields = ['id'] + list(self.display_fields)

                    # get extra field names if is necessary
                    if kwargs.get('extra_fields'):
                        fields += kwargs.get('extra_fields')

                    # serialize object
                    obj_serial = self.serialize(form.save(), fields)

                    return HttpResponse(self.json_encode(obj_serial), "application/json")
                else:
                    return HttpResponseBadRequest(self.json_encode(form.errors))
        return HttpResponseBadRequest("Initial bad request")

    def delete(self, request, id=None, **kwargs):
        # validate ajax if is necessary
        if self.use_only_ajax and not request.is_ajax():
            raise Http404

        return self.delete_object(request, id, **kwargs)

    def delete_object(self, request, id=None, **kwargs):
        """
        Delete validation, only if you have permissions for this
        """
        if id:
            model_obj = get_object_or_404(self.model, id=id)
            if self.has_delete_permission(request, model_obj):
                model_obj.delete()
                return HttpResponse(status=204)
            else:
                return HttpResponseForbidden('You not have permissions for delete')
        else:
            return HttpResponseBadRequest()

    # permissions for add, update and remove
    def clinic_permission(self, request, obj):
        try:
            return bool(obj.clinic.id == request.session['clinic'])
        except:
            return False

    def has_add_permission(self, request):
        try:
            return bool(request.session['clinic'] is not None)
        except:
            return False

    def has_update_permission(self, request, obj):
        data = json.loads(request.body)
        is_clinic = int(data.get('clinic', 0)) == request.session["clinic"]
        return self.clinic_permission(request, obj) and is_clinic

    def has_delete_permission(self, request, obj):
        return self.clinic_permission(request, obj)

    def get_form_instance(self, request, data=None, files=None, instance=None):
        defaults = {}
        if self.form:
            defaults['form'] = self.form
        if self.fields:
            defaults['fields'] = self.fields

        if files is not None:
            model_form = modelform_factory(
                self.model, **defaults)(data, files, instance=instance)
        else:
            model_form = modelform_factory(
                self.model, **defaults)(data=data, instance=instance)

        return model_form

    def serialize(self, obj, fields):
        data = {}
        for field in fields:
            try:
                #print
                if isinstance(getattr(obj, field), (int, long, float, complex)):
                    data[field] = getattr(obj, field)
                elif getattr(obj, field).__class__.__name__.lower() == "clinic":
                    data[field] = getattr(obj, field).id
                elif getattr(obj, field).__class__.__name__.lower() == "list":
                    data[field] = getattr(obj, field)
                else:
                    data[field] = unicode(getattr(obj, field))
            except AttributeError:
                data[field] = getattr(obj, field)

        return data

    def json_encode(self, data):
        params = {'sort_keys': True, 'indent': 2}
        return json.dumps(data, cls=DjangoJSONEncoder, **params)
