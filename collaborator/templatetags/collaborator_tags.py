from django import template
from profile.models import ClinicPhoto, Treatment, TreatmentPrice, Doctor
from profile.models import TreatmentCategory, Procedure
register = template.Library()


@register.simple_tag
def number_format(number):
    """
    Return a number fill with right zeros.
    """
    return "%06d" % number


@register.inclusion_tag('collaborator/tags/treatments_modal.html')
def treatments_modal(clinic):
    if clinic.category.name == "Hospital":
        tc = TreatmentCategory.objects.all()
    else:
        tc = TreatmentCategory.objects.filter(clinic_category=clinic.category)

    pr = Procedure.objects.filter(category__in=tc)
    return {
        'clinic': clinic,
        'treatments': tc,
        'procedures': pr
    }


@register.inclusion_tag('collaborator/tags/treatment.html')
def render_treatments(clinic):
    if clinic.category.name == "Hospital":
        tc = TreatmentCategory.objects.all()
    else:
        tc = TreatmentCategory.objects.filter(clinic_category=clinic.category)

    treatments = [{'name': str(x), 'list': [i for i in Procedure.objects.filter(
                                                        category=x)]} for x in tc]

    return {'treatments': treatments}


@register.inclusion_tag('collaborator/tags/about_photos.html', takes_context=True)
def render_coll_about_photos(context, clinic):
    return {
        'MEDIA_URL': context['MEDIA_URL'],
        'clinic': clinic,
        'images': ClinicPhoto.objects.filter(clinic=clinic.id, in_front=True)[:3]
    }


@register.inclusion_tag('collaborator/tags/gallery.html', takes_context=True)
def render_coll_photos(context, clinic):
    return {
        'MEDIA_URL': context['MEDIA_URL'],
        'images': ClinicPhoto.objects.filter(clinic=clinic.id)
    }


@register.inclusion_tag('collaborator/tags/price.html')
def render_prices(treatment):
    return {
        'prices': TreatmentPrice.objects.filter(treatment=treatment.id),
        'treatment': treatment
    }
