from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.utils import simplejson as json
from mezzanine.pages.models import Page
from profile.models import Clinic, OpeningHour, ClinicPhoto, Review, Location
from sanico.models import SanicoUser
from sanico.email import SendEmail
from .helpers import get_session_clinic


def clinic_session(function_request):
    def clinic_session_callback(request):
        try:
            if not request.session['clinic']:
                clinics = Clinic.objects.filter(user=request.user.id)

                if clinics.count() == 1:
                    request.session['clinic'] = list(clinics)[0].id
                else:
                    request.session['clinic'] = None
        except KeyError:
            request.session['clinic'] = None
        return function_request(request)
    return clinic_session_callback


def collaborator_view(request, template, context={}):
    try:
        page = Page.objects.with_ascendants_for_slug("collaborator")[0]
    except:
        raise Http404

    user = SanicoUser.objects.get(id=request.user.id)

    context["user"] = user
    context["clinic"] = get_session_clinic(request)
    context["clinics"] = Clinic.objects.filter(user=user.id)

    extra_context = RequestContext(request)
    setattr(extra_context, "page", page)
    return render_to_response(
        template, context, context_instance=extra_context)


@login_required(login_url='/login/')
@clinic_session
def about(request):
    opening_hours = OpeningHour.objects.filter(
        clinic=request.session['clinic'])

    return collaborator_view(
        request, "collaborator/about.html", {
            'opening_hours': opening_hours
        })


@login_required(login_url='/login/')
@clinic_session
def general(request):
    return collaborator_view(request, 'collaborator/general.html')


@login_required(login_url='/login/')
@clinic_session
def overview(request):
    reviews = Review.objects.filter(
        clinic=request.session['clinic'], enable=True)

    reviews_num = reviews.count()
    photos_num = ClinicPhoto.objects.filter(
        clinic=request.session['clinic']).count()

    return collaborator_view(
        request, 'collaborator/overview.html', {
            'reviews': reviews,
            'reviews_num': reviews_num,
            'photos_num': photos_num
        })


@login_required(login_url='/login/')
@clinic_session
def gallery(request):
    context = {
        'photos': ClinicPhoto.objects.filter(
            clinic=request.session['clinic'])
    }
    return collaborator_view(request, 'collaborator/gallery.html', context)


@login_required(login_url='/login/')
@clinic_session
def location(request):
    try:
        location = Location.objects.get(clinic=request.session['clinic'])
    except:
        location = None
    context = {'location': location}
    return collaborator_view(request, 'collaborator/location.html', context)


@login_required(login_url='/login/')
@clinic_session
def doctors(request):
    return collaborator_view(request, 'collaborator/doctors.html')


@login_required(login_url='/login/')
@clinic_session
def prices(request):
    try:
        location = Location.objects.get(clinic=request.session['clinic'])
    except:
        location = None
    context = {'location': location}
    return collaborator_view(request, 'collaborator/prices.html', context)


@login_required(login_url='/login/')
def select_clinic(request, id):
    try:
        request.session['clinic'] = Clinic.objects.get(user=request.user.id, id=id).id
    except Clinic.DoesNotExist:
        pass
    return HttpResponseRedirect(reverse("collaborator:general"))


@login_required(login_url='/login/')
def change_clinic(request):
    try:
        request.session['clinic'] = None
    except Clinic.DoesNotExist:
        pass
    return HttpResponseRedirect(reverse("collaborator:general"))


@login_required(login_url='/login/')
def add_treatment(request):
    email = SendEmail()
    email.set_recipes(['accounts@sanimedicaltourism.com'])
    email.notify_message(
        "[CINIC TREATMENT REQUIRED]",
        "emails/required_treatment.html", {
            'clinic': get_session_clinic(request),
            'name': request.GET.get('name'),
            'description': request.GET.get('description')
        })

    success = email.send()
    return HttpResponse(
        json.dumps({'success': success}), mimetype="application/json")


@login_required(login_url='/login/')
@clinic_session
def send_change_required(request):
    email = SendEmail()
    email.set_recipes(['accounts@sanimedicaltourism.com'])
    email.notify_message(
        "[PROFILE CHANGE REQUIRED]",
        "emails/profile_change_required.html",
        {'clinic': get_session_clinic(request)})

    success = email.send()
    return HttpResponse(
        json.dumps({'success': success}), mimetype="application/json")
