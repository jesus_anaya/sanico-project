from django.db.models import Q
from .models import Clinic, ClinicProcedure


def search_results(query, where=None):

    #lambda method to ger procedures by name
    pget = lambda x: ClinicProcedure.objects.filter(procedure__name=x)

    # Get all clinics list from query
    list_clinics = list(set([x.clinic.id for x in pget(query)]))

    # filter enable clinics only
    queryset = Clinic.objects.filter(enable=True)

    # If where field is sprecified, use this as filter
    if where:
        queryset = queryset.filter(city__icontains=where)

    # Filter clinics by element
    queryset = queryset.filter(
        # Serach by procedure, compare ir the clinic is in the list that contains
        Q(id__in=list_clinics) \

        # Search by clinic name
        | Q(name__iexact=query) \

        # Search by category name
        | Q(category_index__iexact=query) \

        # Search by tag name in categories
        | Q(category__tags__name__iexact=query))

    return list(queryset)


def get_cities(query):
    # Get only enabled clinics
    queryset = Clinic.objects.filter(enable=True)

    # Get cities list
    return queryset.filter(
        category_index__iexact=query).values_list('city', flat=True).distinct()
