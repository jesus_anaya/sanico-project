from django.contrib import admin
from django.conf import settings
from mezzanine.core.admin import TabularDynamicInlineAdmin, StackedDynamicInlineAdmin
from models import Country, Clinic, Doctor, ClinicPhoto, Location, City, Review, ClinicTag
from models import OpeningHour, ClinicCategory, ClinicProcedure, Procedure, TreatmentCategory
from .helpers import hide_model
from .forms import DoctorForm, ClinicForm, ClinicPhotoForm, LocationForm
from .widgets import SelectWithGroup

from libs.admin_tabs.helpers import TabbedModelAdmin, TabbedPageConfig, Config


class DoctorInline(StackedDynamicInlineAdmin):
    form = DoctorForm
    model = Doctor


class OpeningHourInline(admin.TabularInline):
    model = OpeningHour
    extra = 7
    max_num = 7


# Procedure tag in Clinic admin
class ProcedureInline(TabularDynamicInlineAdmin):
    model = ClinicProcedure
    fields = ['procedure', 'description', 'price', 'duration']

    # Create list of procedures in groups by categories
    def formfield_for_dbfield(self, field, **kwargs):
        if field.name == 'procedure':

            # Get the current clinic using path id
            clinic = self.get_object(kwargs['request'], Clinic)

            # Get the current Treatment categories and Procedurer relationship with our clinic
            # If the category name is Hospital get all categories
            if clinic:
                if clinic.category.name == 'Hospital':
                    categories = TreatmentCategory.objects.all()
                else:
                    categories = TreatmentCategory.objects.filter(clinic_category=clinic.category)
            else:
                categories = TreatmentCategory.objects.all()

            # Shortcut for get filtered procedures
            pfiltered = lambda x : Procedure.objects.filter(category=x)

            # Create a filedset using the categories and procedures
            # Example result:
            # fieldset = (
            #    ("Sirugy", ((1,'procedure 1'), (2, 'procedure 2'))),
            #    ('Others', ((1, 'procedure 1'), (1, 'procedure orange'))),
            # )
            fieldset = [(x.name, [(i.id, i.name) for i in pfiltered(x)]) for x in categories]

            # Using a custom widget for add groups to the final select
            kwargs['widget'] = SelectWithGroup(fieldset_choices=fieldset)

        return super(ProcedureInline, self).formfield_for_dbfield(field, **kwargs)

    # Get current clinic object by path id
    def get_object(self, request, model):
        object_id = request.META['PATH_INFO'].strip('/').split('/')[-1]
        try:
            object_id = int(object_id)
        except ValueError:
            return None
        return model.objects.get(pk=object_id)


class ClinicPhotoInline(TabularDynamicInlineAdmin):
    model = ClinicPhoto
    form = ClinicPhotoForm


class LocationInline(admin.TabularInline):
    fields = ('link', 'image_map_1', 'image_map_2')
    form = LocationForm
    model = Location
    extra = 1
    max_num = 1


class ClinicPageConfig(TabbedPageConfig):
    class FieldsetsConfig:
        clinic = Config(fields=[
            'user', 'created', 'update_created', 'extra_position', 'name', 'enable', 'image_header',
            'image_profile', 'category', 'open_since', 'about', 'country', 'city', 'state',
            'zip_code', 'email', 'phone', 'address'], name="Clinic")

        paiment = Config(fields=[
            'personal_checks', 'traveler_checks', 'cash', 'mastercard', 'visa',
            'american_express', 'discover', 'paypal'],
            name="Paiment")

        languages = Config(fields=['english', 'spanish', 'other'], name="Languages")

        legal_info = Config(fields=['company_name', 'rfc', 'legal_address'], name="Datos de facturacion")

        fiscal_info = Config(fields=[
            'fiscal_first_name', 'fiscal_last_name',
            'fiscal_email', 'fiscal_phone'], name="Fiscal Information")

        contact_info = Config(fields=[
            'contact_first_name', 'contact_last_name',
            'contact_email', 'contact_phone'], name="Contact Information")

        doctors = Config(name="Doctors", inline="DoctorInline")
        procedures = Config(name="Procedures", inline="ProcedureInline")
        opening_hour = Config(name="Opening Hour", inline="OpeningHourInline")
        photo = Config(name="Clinic Photo", inline="ClinicPhotoInline")
        location = Config(name="Location", inline="LocationInline")

    class ColsConfig:
        clinic_col = Config(name="Clinic", fieldsets=["clinic"], css_classes=["col2"])
        paiment_col = Config(name="Paiment", fieldsets=["paiment"], css_classes=["col1"])
        languages_col = Config(name="Languages", fieldsets=["languages"], css_classes=["col1"])
        legal_info_col = Config(name="Legal Info", fieldsets=["legal_info"], css_classes=["col2"])
        fiscal_info_col = Config(name="Fiscal Info", fieldsets=["fiscal_info"], css_classes=["col2"])
        contact_info_col = Config(name="Contact Info", fieldsets=["contact_info"], css_classes=["col2"])
        doctors_col = Config(name="Doctors", fieldsets=["doctors"], css_classes=["col2"])
        procedures_col = Config(name="procedures", fieldsets=["procedures"], css_classes=["col2"])
        opening_hour_col = Config(name="Opening Hour", fieldsets=["opening_hour"], css_classes=["col1"])
        photo_col = Config(name="Clinic Photo", fieldsets=["photo"], css_classes=["col2"])
        location_col = Config(name="Location", fieldsets=["location"], css_classes=["col2"])

    class TabsConfig:
        main_tab = Config(name="Clinic", cols=["clinic_col", "legal_info_col", "fiscal_info_col", "contact_info_col"])
        options_tab = Config(name="Clinic Options", cols=[
            "paiment_col", "languages_col", "opening_hour_col"])
        doctors_tab = Config(name="Doctors", cols=["doctors_col"])
        treatments_tab = Config(name="Procedures", cols=["procedures_col"])
        gallery_tab = Config(name="Gallery", cols=["photo_col"])
        location_tab = Config(name="Location", cols=["location_col"])


class ClinicAdmin(TabbedModelAdmin):
    class Media:
        css = {
            "all": (
                "tabs/css/jquery-ui-1.8.22.custom.css",
                "tabs/css/tabs.css",
                "sanico/css/kendo/kendo.common.min.css",
                "sanico/css/kendo/kendo.default.min.css",
                "sanico/css/admin/profile.css")}
        js = (
            "tabs/js/jquery-ui-1.8.22.custom.min.js",
            "https://maps.googleapis.com/maps/api/js?sensor=false",
            "sanico/js/admin/profile.js",
            "sanico/js/kendo/kendo.all.min.js")

    form = ClinicForm
    page_config_class = ClinicPageConfig
    list_display = (
        'profile_image', 'enable', 'user', 'category', 'extra_position',
        'name', 'city', 'address', 'country', 'email', 'phone', 'view_on_site')
    list_editable = ('enable', 'user', 'extra_position')
    list_filter = ('name', 'city', 'address', 'category')

    inlines = [
        DoctorInline, OpeningHourInline, ProcedureInline,
        ClinicPhotoInline, LocationInline]

    change_form_template = "tabs/change_form.html"

    def profile_image(self, obj):
        media = settings.MEDIA_URL
        return '<img src="%s%s" width="60" />' % (media, obj.image_profile)
    profile_image.allow_tags = True

    def view_on_site(self, obj):
        return '<a href="%s" target="_blank">View on site</a>' % obj.get_absolute_url()
    view_on_site.allow_tags = True


class CountryAdmin(admin.ModelAdmin):
    list_display = ('name', 'shortcut')


class DoctorAdmin(admin.ModelAdmin):
    def in_menu(self):
        return hide_model('profile.Doctor')


class ClinicCategoryAdmin(admin.ModelAdmin):
    def in_menu(self):
        return hide_model('profile.ClinicCategory')


class ClinicTagAdmin(admin.ModelAdmin):
    def in_menu(self):
        return hide_model('profile.ClinicTag')


class CityAdmin(admin.ModelAdmin):
    def in_menu(self):
        return hide_model('profile.City')


class ProcedureAdmin(admin.ModelAdmin):
    list_display = ("name", "category")
    list_filter = ("category",)

    def in_menu(self):
        return hide_model('profile.Procedure')


class TreatmentCategoryAdmin(admin.ModelAdmin):
    list_display = ("name", "clinic_category")
    list_filter = ("clinic_category",)

    def in_menu(self):
        return hide_model('profile.TreatmentCategory')


class ReviewAdmin(admin.ModelAdmin):
    class Media:
        css = {
            "all": (
                "sanico/css/results.css",)}
        js = (
            "tabs/js/jquery-ui-1.8.22.custom.min.js",
            "sanico/js/admin/review.js")

    list_display = ('clinic', 'enable', 'qualification', 'user_name', 'datetime')
    list_editable = ('enable',)
    exclude = ('qualification', 'for_testimonial')

    def user_name(self, obj):
        return obj.get_full_name()

    def in_menu(self):
        return hide_model('profile.Review')


admin.site.register(ClinicTag, ClinicTagAdmin)
admin.site.register(Country, CountryAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(Clinic, ClinicAdmin)
admin.site.register(Doctor, DoctorAdmin)
admin.site.register(ClinicCategory, ClinicCategoryAdmin)
admin.site.register(Review, ReviewAdmin)
admin.site.register(Procedure, ProcedureAdmin)
admin.site.register(TreatmentCategory, TreatmentCategoryAdmin)
