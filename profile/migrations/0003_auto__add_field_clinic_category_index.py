# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Clinic.category_index'
        db.add_column(u'profile_clinic', 'category_index',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=50, db_index=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Clinic.category_index'
        db.delete_column(u'profile_clinic', 'category_index')


    models = {
        u'profile.clinic': {
            'Meta': {'ordering': "['-created']", 'object_name': 'Clinic'},
            'about': ('mezzanine.core.fields.RichTextField', [], {}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'cash': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.ClinicCategory']"}),
            'category_index': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '50', 'db_index': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_index': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Country']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 4, 3, 0, 0)', 'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'enable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'english': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_header': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'image_profile': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'mastercard': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_index': 'True'}),
            'open_since': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'other': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'personal_checks': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'slug': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '150'}),
            'spanish': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'state': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'traveler_checks': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'visa': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'zip_code': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True'})
        },
        u'profile.cliniccategory': {
            'Meta': {'object_name': 'ClinicCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'profile.clinicphoto': {
            'Meta': {'object_name': 'ClinicPhoto'},
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Clinic']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_front': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        },
        u'profile.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'shortcut': ('django.db.models.fields.CharField', [], {'max_length': '5'})
        },
        u'profile.doctor': {
            'Meta': {'object_name': 'Doctor'},
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Clinic']"}),
            'education': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'languages': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'default': "''", 'max_length': '100'}),
            'specialisations': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'profile.location': {
            'Meta': {'object_name': 'Location'},
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Clinic']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_map_1': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'image_map_2': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'map_iframe': ('django.db.models.fields.TextField', [], {})
        },
        u'profile.openinghour': {
            'Meta': {'ordering': "['id']", 'object_name': 'OpeningHour'},
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Clinic']"}),
            'day': ('django.db.models.fields.IntegerField', [], {}),
            'hour_close': ('django.db.models.fields.TimeField', [], {}),
            'hour_open': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'profile.review': {
            'Meta': {'ordering': "['-datetime']", 'object_name': 'Review'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'cleanliness': ('django.db.models.fields.IntegerField', [], {}),
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Clinic']"}),
            'comfort': ('django.db.models.fields.IntegerField', [], {}),
            'comment': ('django.db.models.fields.TextField', [], {}),
            'communication': ('django.db.models.fields.IntegerField', [], {}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'date_appointment': ('django.db.models.fields.DateField', [], {}),
            'datetime': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 4, 3, 0, 0)', 'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'enable': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'location': ('django.db.models.fields.IntegerField', [], {}),
            'price': ('django.db.models.fields.IntegerField', [], {}),
            'qualification': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'quality': ('django.db.models.fields.IntegerField', [], {}),
            'satisfaction': ('django.db.models.fields.IntegerField', [], {}),
            'service': ('django.db.models.fields.IntegerField', [], {}),
            'treatment': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        u'profile.treatment': {
            'Meta': {'object_name': 'Treatment'},
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Clinic']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'})
        },
        u'profile.treatmentprice': {
            'Meta': {'object_name': 'TreatmentPrice'},
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Clinic']", 'null': 'True'}),
            'duration': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {}),
            'procedure': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'treatment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Treatment']", 'null': 'True'})
        }
    }

    complete_apps = ['profile']