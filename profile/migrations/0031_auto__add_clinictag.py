# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ClinicTag'
        db.create_table(u'profile_clinictag', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=80)),
        ))
        db.send_create_signal(u'profile', ['ClinicTag'])

        # Adding M2M table for field tags on 'ClinicCategory'
        db.create_table(u'profile_cliniccategory_tags', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('cliniccategory', models.ForeignKey(orm[u'profile.cliniccategory'], null=False)),
            ('clinictag', models.ForeignKey(orm[u'profile.clinictag'], null=False))
        ))
        db.create_unique(u'profile_cliniccategory_tags', ['cliniccategory_id', 'clinictag_id'])


    def backwards(self, orm):
        # Deleting model 'ClinicTag'
        db.delete_table(u'profile_clinictag')

        # Removing M2M table for field tags on 'ClinicCategory'
        db.delete_table('profile_cliniccategory_tags')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'profile.city': {
            'Meta': {'object_name': 'City'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Country']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'map_image': ('django.db.models.fields.files.ImageField', [], {'default': "''", 'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'profile.clinic': {
            'Meta': {'ordering': "['-extra_position', '-qualification', '-created']", 'object_name': 'Clinic'},
            'about': ('mezzanine.core.fields.RichTextField', [], {'default': "''"}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'american_express': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cash': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'cashier_check': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.ClinicCategory']"}),
            'category_index': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '50', 'db_index': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_index': 'True'}),
            'cleanliness': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'comfort': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'communication': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'company_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'contact_email': ('django.db.models.fields.EmailField', [], {'default': "''", 'max_length': '75'}),
            'contact_first_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '60'}),
            'contact_last_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '60'}),
            'contact_phone': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '30'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Country']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {}),
            'discover': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'enable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'english': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'extra_position': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'fiscal_email': ('django.db.models.fields.EmailField', [], {'default': "''", 'max_length': '75'}),
            'fiscal_first_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '60'}),
            'fiscal_last_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '60'}),
            'fiscal_phone': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '30'}),
            'french': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'german': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_header': ('django.db.models.fields.files.ImageField', [], {'default': "'uploads/images/clinic-default-cover.jpg'", 'max_length': '100', 'blank': 'True'}),
            'image_profile': ('django.db.models.fields.files.ImageField', [], {'default': "'uploads/images/clinic-default-profile.jpg'", 'max_length': '100', 'blank': 'True'}),
            'italian': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'japanese': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'legal_address': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'location': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'mandarin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mastercard': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'money_order': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_index': 'True'}),
            'offer': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200'}),
            'open_since': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '6'}),
            'other': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'paypal': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'personal_checks': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'portuguese': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'price': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'qualification': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'quality': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'rfc': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '30'}),
            'russian': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'satisfaction': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'service': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'slug': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '150'}),
            'spanish': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'state': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'traveler_checks': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sanico.SanicoUser']", 'null': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'visa': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'wire_transfer': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'zip_code': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True'})
        },
        u'profile.cliniccategory': {
            'Meta': {'object_name': 'ClinicCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['profile.ClinicTag']", 'symmetrical': 'False'})
        },
        u'profile.clinicphoto': {
            'Meta': {'object_name': 'ClinicPhoto'},
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Clinic']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_front': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        },
        u'profile.clinicprocedure': {
            'Meta': {'ordering': "['procedure']", 'object_name': 'ClinicProcedure'},
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Clinic']"}),
            'description': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'duration': ('django.db.models.fields.CharField', [], {'default': "'Undefined'", 'max_length': '60'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.CharField', [], {'default': "'Undefined'", 'max_length': '40'}),
            'procedure': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Procedure']", 'blank': 'True'})
        },
        u'profile.clinictag': {
            'Meta': {'object_name': 'ClinicTag'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80'})
        },
        u'profile.complementaryservice': {
            'Meta': {'object_name': 'ComplementaryService'},
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Clinic']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'profile.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'shortcut': ('django.db.models.fields.CharField', [], {'max_length': '5'})
        },
        u'profile.doctor': {
            'Meta': {'ordering': "['position']", 'object_name': 'Doctor'},
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Clinic']"}),
            'education': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'languages': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'default': "''", 'max_length': '100'}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'resume': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'specialisations': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'profile.location': {
            'Meta': {'object_name': 'Location'},
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': u"orm['profile.Clinic']"}),
            'coords': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '40'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_map_1': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'image_map_2': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'})
        },
        u'profile.openinghour': {
            'Meta': {'ordering': "['id']", 'object_name': 'OpeningHour'},
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Clinic']"}),
            'day': ('django.db.models.fields.IntegerField', [], {}),
            'hour_close': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'hour_open': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'profile.procedure': {
            'Meta': {'ordering': "['name']", 'object_name': 'Procedure'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.TreatmentCategory']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'})
        },
        u'profile.review': {
            'Meta': {'ordering': "['-datetime']", 'object_name': 'Review'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'cleanliness': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Clinic']"}),
            'comfort': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'comment': ('django.db.models.fields.TextField', [], {}),
            'communication': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'date_appointment': ('django.db.models.fields.DateField', [], {}),
            'datetime': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 8, 13, 0, 0)', 'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'enable': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'for_testimonial': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'location': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'price': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'qualification': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'quality': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'satisfaction': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'service': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'treatment': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'value': ('django.db.models.fields.IntegerField', [], {'default': '10'})
        },
        u'profile.specialpromotion': {
            'Meta': {'object_name': 'SpecialPromotion'},
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Clinic']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'profile.treatment': {
            'Meta': {'ordering': "['position']", 'object_name': 'Treatment'},
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Clinic']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'profile.treatmentcategory': {
            'Meta': {'ordering': "['name']", 'object_name': 'TreatmentCategory'},
            'clinic_category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.ClinicCategory']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'})
        },
        u'profile.treatmentprice': {
            'Meta': {'ordering': "['position']", 'object_name': 'TreatmentPrice'},
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Clinic']", 'null': 'True'}),
            'duration': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'price': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'procedure': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'treatment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Treatment']", 'null': 'True'})
        },
        u'sanico.sanicouser': {
            'Meta': {'object_name': 'SanicoUser', '_ormbases': [u'auth.User']},
            'telephone': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '25', 'blank': 'True'}),
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True', 'primary_key': 'True'})
        }
    }

    complete_apps = ['profile']