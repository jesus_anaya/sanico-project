# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Review'
        db.create_table(u'profile_review', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('clinic', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['profile.Clinic'])),
            ('quality', self.gf('django.db.models.fields.IntegerField')()),
            ('service', self.gf('django.db.models.fields.IntegerField')()),
            ('communication', self.gf('django.db.models.fields.IntegerField')()),
            ('cleanliness', self.gf('django.db.models.fields.IntegerField')()),
            ('price', self.gf('django.db.models.fields.IntegerField')()),
            ('comfort', self.gf('django.db.models.fields.IntegerField')()),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
            ('location', self.gf('django.db.models.fields.IntegerField')()),
            ('satisfaction', self.gf('django.db.models.fields.IntegerField')()),
            ('qualification', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('datetime', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2013, 4, 2, 0, 0), auto_now_add=True, blank=True)),
            ('enable', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('treatment', self.gf('django.db.models.fields.CharField')(default='', max_length=100)),
            ('date_appointment', self.gf('django.db.models.fields.DateField')()),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('country', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('comment', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'profile', ['Review'])

        # Adding model 'Country'
        db.create_table(u'profile_country', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('shortcut', self.gf('django.db.models.fields.CharField')(max_length=5)),
        ))
        db.send_create_signal(u'profile', ['Country'])

        # Adding model 'ClinicCategory'
        db.create_table(u'profile_cliniccategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'profile', ['ClinicCategory'])

        # Adding model 'Clinic'
        db.create_table(u'profile_clinic', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('personal_checks', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('traveler_checks', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('cash', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('mastercard', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('visa', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('english', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('spanish', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('other', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('slug', self.gf('django.db.models.fields.CharField')(default='', max_length=150)),
            ('image_header', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('image_profile', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['profile.ClinicCategory'])),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['profile.Country'])),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('state', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('zip_code', self.gf('django.db.models.fields.CharField')(max_length=15, null=True)),
            ('open_since', self.gf('django.db.models.fields.CharField')(max_length=6)),
            ('about', self.gf('mezzanine.core.fields.RichTextField')()),
            ('created', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2013, 4, 2, 0, 0), auto_now_add=True, blank=True)),
            ('enable', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'profile', ['Clinic'])

        # Adding model 'Doctor'
        db.create_table(u'profile_doctor', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('clinic', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['profile.Clinic'])),
            ('photo', self.gf('django.db.models.fields.files.ImageField')(default='', max_length=100)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('education', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('specialisations', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('languages', self.gf('django.db.models.fields.CharField')(max_length=60)),
        ))
        db.send_create_signal(u'profile', ['Doctor'])

        # Adding model 'Treatment'
        db.create_table(u'profile_treatment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('clinic', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['profile.Clinic'], null=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=100)),
        ))
        db.send_create_signal(u'profile', ['Treatment'])

        # Adding model 'TreatmentPrice'
        db.create_table(u'profile_treatmentprice', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('clinic', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['profile.Clinic'], null=True)),
            ('treatment', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['profile.Treatment'], null=True)),
            ('procedure', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('price', self.gf('django.db.models.fields.FloatField')()),
            ('duration', self.gf('django.db.models.fields.CharField')(max_length=60)),
        ))
        db.send_create_signal(u'profile', ['TreatmentPrice'])

        # Adding model 'ClinicPhoto'
        db.create_table(u'profile_clinicphoto', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('clinic', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['profile.Clinic'], null=True)),
            ('photo', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('in_front', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'profile', ['ClinicPhoto'])

        # Adding model 'OpeningHour'
        db.create_table(u'profile_openinghour', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('clinic', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['profile.Clinic'])),
            ('day', self.gf('django.db.models.fields.IntegerField')()),
            ('hour_open', self.gf('django.db.models.fields.TimeField')()),
            ('hour_close', self.gf('django.db.models.fields.TimeField')()),
        ))
        db.send_create_signal(u'profile', ['OpeningHour'])

        # Adding model 'Location'
        db.create_table(u'profile_location', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('clinic', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['profile.Clinic'])),
            ('map_iframe', self.gf('django.db.models.fields.TextField')()),
            ('image_map_1', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('image_map_2', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal(u'profile', ['Location'])


    def backwards(self, orm):
        # Deleting model 'Review'
        db.delete_table(u'profile_review')

        # Deleting model 'Country'
        db.delete_table(u'profile_country')

        # Deleting model 'ClinicCategory'
        db.delete_table(u'profile_cliniccategory')

        # Deleting model 'Clinic'
        db.delete_table(u'profile_clinic')

        # Deleting model 'Doctor'
        db.delete_table(u'profile_doctor')

        # Deleting model 'Treatment'
        db.delete_table(u'profile_treatment')

        # Deleting model 'TreatmentPrice'
        db.delete_table(u'profile_treatmentprice')

        # Deleting model 'ClinicPhoto'
        db.delete_table(u'profile_clinicphoto')

        # Deleting model 'OpeningHour'
        db.delete_table(u'profile_openinghour')

        # Deleting model 'Location'
        db.delete_table(u'profile_location')


    models = {
        u'profile.clinic': {
            'Meta': {'ordering': "['-created']", 'object_name': 'Clinic'},
            'about': ('mezzanine.core.fields.RichTextField', [], {}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'cash': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.ClinicCategory']"}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Country']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 4, 2, 0, 0)', 'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'enable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'english': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_header': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'image_profile': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'mastercard': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'open_since': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'other': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'personal_checks': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'slug': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '150'}),
            'spanish': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'state': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'traveler_checks': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'visa': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'zip_code': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True'})
        },
        u'profile.cliniccategory': {
            'Meta': {'object_name': 'ClinicCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'profile.clinicphoto': {
            'Meta': {'object_name': 'ClinicPhoto'},
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Clinic']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_front': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        },
        u'profile.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'shortcut': ('django.db.models.fields.CharField', [], {'max_length': '5'})
        },
        u'profile.doctor': {
            'Meta': {'object_name': 'Doctor'},
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Clinic']"}),
            'education': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'languages': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'default': "''", 'max_length': '100'}),
            'specialisations': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'profile.location': {
            'Meta': {'object_name': 'Location'},
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Clinic']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_map_1': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'image_map_2': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'map_iframe': ('django.db.models.fields.TextField', [], {})
        },
        u'profile.openinghour': {
            'Meta': {'ordering': "['id']", 'object_name': 'OpeningHour'},
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Clinic']"}),
            'day': ('django.db.models.fields.IntegerField', [], {}),
            'hour_close': ('django.db.models.fields.TimeField', [], {}),
            'hour_open': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'profile.review': {
            'Meta': {'ordering': "['-datetime']", 'object_name': 'Review'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'cleanliness': ('django.db.models.fields.IntegerField', [], {}),
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Clinic']"}),
            'comfort': ('django.db.models.fields.IntegerField', [], {}),
            'comment': ('django.db.models.fields.TextField', [], {}),
            'communication': ('django.db.models.fields.IntegerField', [], {}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'date_appointment': ('django.db.models.fields.DateField', [], {}),
            'datetime': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 4, 2, 0, 0)', 'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'enable': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'location': ('django.db.models.fields.IntegerField', [], {}),
            'price': ('django.db.models.fields.IntegerField', [], {}),
            'qualification': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'quality': ('django.db.models.fields.IntegerField', [], {}),
            'satisfaction': ('django.db.models.fields.IntegerField', [], {}),
            'service': ('django.db.models.fields.IntegerField', [], {}),
            'treatment': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        u'profile.treatment': {
            'Meta': {'object_name': 'Treatment'},
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Clinic']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'})
        },
        u'profile.treatmentprice': {
            'Meta': {'object_name': 'TreatmentPrice'},
            'clinic': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Clinic']", 'null': 'True'}),
            'duration': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {}),
            'procedure': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'treatment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profile.Treatment']", 'null': 'True'})
        }
    }

    complete_apps = ['profile']