# -*- coding: UTF-8 -*-
from django.conf import settings
from sanico.email import SendEmail
import datetime


def serialize(obj, fields):
    data = {}
    for field in fields:
        try:
            #print
            if isinstance(getattr(obj, field), (int, long, float, complex)):
                data[field] = getattr(obj, field)
            elif getattr(obj, field).__class__.__name__.lower() == "clinic":
                data[field] = getattr(obj, field).id
            elif getattr(obj, field).__class__.__name__.lower() == "list":
                data[field] = getattr(obj, field)
            else:
                data[field] = unicode(getattr(obj, field))
        except AttributeError:
            data[field] = getattr(obj, field)

    return data


def hide_model(model_name):
    """
    Hide from the admin menu unless explicitly set in ``ADMIN_MENU_ORDER``.
    """
    for (name, items) in settings.ADMIN_MENU_ORDER:
        if model_name in items:
            return True
    return False


def send_email(subject, template, context):
    email = SendEmail()
    email.notify_message(subject, template, context)
    return email.send()


def send_mail_after_save_review(link_ok, link_cancel, review):
    subject = "[REVIEW SEND]"
    template = "emails/review_send.html"
    context = {
        'link_ok': link_ok,
        'link_cancel': link_cancel,
        'review': review
    }
    return send_email(subject, template, context)


def send_review_report(review, ok_link, cancel_link):
    subject = "Clinic review reported"
    template = "emails/review-reported.html"
    context = {
        'review': review,
        'ok_link': ok_link,
        'cancel_link': cancel_link}

    email = SendEmail()
    email.set_recipes(settings.EMAIL_INFORMATION)
    email.notify_message(subject, template, context)

    return email.send()


def send_review_summary(clinic, review, link):
    subject = u"Alguien acaba de escribir una reseña sobre tu clinica"
    template = "emails/review-summary.html"
    context = {
        'datetime': datetime.datetime.now(),
        'review': review,
        'link': link}

    email = SendEmail()
    email.set_recipes([clinic.contact_email], bcc=settings.EMAIL_INFORMATION)
    email.notify_message(subject, template, context)

    return email.send()


def send_free_quote(contact):
    subject = "%s Requested treatment information" % contact.get('full_name')

    template_1 = "emails/send_contact_clinic.html"
    template_2 = "emails/clinic-contact-freequote.html"
    context = {'contact': contact}
    email = SendEmail()
    email.set_recipes(settings.EMAIL_INFORMATION)
    email.notify_message(subject, template_1, context)
    email.to_user_message(subject, template_2, contact.get('email'))

    return email.send()


def send_contact_clinic(contact):
    #Create sumbject
    subject = "%s Requested clinic information" % contact.get('full_name')

    template_1 = "emails/send_contact_clinic.html"
    template_2 = "emails/clinic-contact.html"
    context = {'contact': contact}
    email = SendEmail()
    email.set_recipes(settings.EMAIL_INFORMATION)
    email.notify_message(subject, template_1, context)
    email.to_user_message(subject, template_2, contact.get('email'))

    return email.send()
