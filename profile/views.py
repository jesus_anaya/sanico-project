from django.shortcuts import render_to_response
from django.template import RequestContext
from django.shortcuts import get_object_or_404
from django.http import HttpResponse, Http404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.utils import simplejson as json
from django.views.decorators.csrf import csrf_protect
from .models import Clinic, OpeningHour, Doctor, Location, Review, ClinicProcedure
from .models import ComplementaryService, SpecialPromotion, TreatmentCategory, ClinicRelationTreatment
from .forms import ReviewForm
from .helpers import send_mail_after_save_review, send_contact_clinic, send_free_quote
from .helpers import serialize
from mezzanine.pages.models import Page
from libs.validationlink.link import code as link_code
from .forms import GetFreeQuoteForm


def get_clinic_id(slug):
    return get_object_or_404(Clinic, slug=slug).id


def profile_view(request, slug, template, context={}):
    try:
        page = Page.objects.with_ascendants_for_slug("profile")[0]
    except IndexError:
        raise Http404

    clinic = get_object_or_404(Clinic, slug=slug)

    if not request.user.is_staff and not request.user.is_superuser:
        if not clinic.enable and request.user.id != clinic.user.id:
            raise Http404

    context['clinic'] = clinic
    context['num_reviews'] = Review.objects.filter(
        enable=True, clinic=context['clinic'].id).count()

    extra_context = RequestContext(request)
    extra_context["page"] = page

    return render_to_response(
        template, context, context_instance=extra_context)


def about(request, slug):
    opening_hours = OpeningHour.objects.filter(clinic=get_clinic_id(slug))
    complementary_sevices = ComplementaryService.objects.filter(clinic=get_clinic_id(slug))
    special_promotion = SpecialPromotion.objects.filter(clinic=get_clinic_id(slug))
    return profile_view(
        request, slug, "profile/about.html", {
            'opening_hours': opening_hours,
            'complementary_sevices': complementary_sevices,
            'special_promotions': special_promotion
        })


def contact_clinic(request, slug):
    return profile_view(
        request, slug, "profile/contact_clinic.html")


def doctors(request, slug):
    doctors = Doctor.objects.filter(clinic=get_clinic_id(slug))
    return profile_view(
        request, slug, "profile/doctors.html", {'doctors': doctors})


def gallery(request, slug):
    return profile_view(request, slug, "profile/gallery.html")


def location(request, slug):
    clinic = get_object_or_404(Clinic, slug=slug)
    try:
        location = get_object_or_404(Location, clinic=clinic.id)
    except:
        location = None
    return profile_view(
        request, slug, "profile/location.html", {
            'location': location, 'clinic': clinic})


def prices(request, slug):
    clinic = get_object_or_404(Clinic, slug=slug)
    display_fields = ['clinic', 'description', 'procedure', 'price', 'duration', 'procedure_id']

    # Get all precedure categories relationship with the current clinic ordering by prosition field
    #
    categories = [x.category for x in ClinicRelationTreatment.objects.filter(clinic=clinic)]

    # If category is Hospital get all procedures, else get treatments by
    # clinic category
    #
    # if clinic.category.name == "Hospital":
    #     categories = TreatmentCategory.objects.all()
    # else:
    #     categories = TreatmentCategory.objects.filter(clinic_category=clinic.category)

    # Get all procedures by clinic
    #
    clinic_procedure = ClinicProcedure.objects.filter(clinic=clinic)

    # Get procedures filtered by clinic
    #
    pfiltered = lambda x: clinic_procedure.filter(procedure__category=x)

    # Get all information with a format ordenable
    #
    treatments = filter(lambda x: x, [{'category': str(x), 'category_id': x.id, 'list': [
        serialize(i, ['id'] + display_fields) for i in pfiltered(
            x)]} if pfiltered(x).exists() else None for x in categories])

    return profile_view(request, slug, "profile/prices.html", {
        'treatments': treatments
    })


def reviews(request, slug):
    reviews = Review.objects.filter(enable=True, clinic=get_clinic_id(slug))
    return profile_view(request, slug, "profile/reviews.html", {
        'reviews': reviews[:3]
    })


def single_review(request, slug, id):
    review = get_object_or_404(Review, id=id)
    return profile_view(request, slug, "profile/single_review.html", {
        'review': review
    })


def write_review(request, slug):
    return profile_view(request, slug, "profile/write_review.html")


def contact_thanks(request):
    return render_to_response(
        "profile/contact-thanks.html", context_instance=RequestContext(request))


def review_thanks(request):
    return render_to_response(
        "profile/review-thanks.html", context_instance=RequestContext(request))


def reviews_list(request, slug):
    review_list = Review.objects.filter(enable=True, clinic=get_clinic_id(slug))
    # Show 25 contacts per page
    paginator = Paginator(review_list, 15)

    page = request.GET.get('page', 1)
    try:
        reviews = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        reviews = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        reviews = paginator.page(paginator.num_pages)
    return profile_view(request, slug, "profile/reviews_list.html", {
        'reviews': reviews
    })


# Get about in clinic profile, this is for that google not
# punish for duplicate content
def get_about(request, clinic_id):
    clinic = get_object_or_404(Clinic, id=int(clinic_id))
    return HttpResponse("%s" % clinic.about)


## Send email after contact clinic form is send
@csrf_protect
def send_contact(request):
    from sanico.models import WaitContacEmail
    sended = False
    if request.method == 'POST':
        contact = GetFreeQuoteForm(request.POST)
        if contact.is_valid():
            sended = send_contact_clinic(contact.cleaned_data)
            email = WaitContacEmail()
            email.name = contact.cleaned_data.get('full_name')
            email.email = contact.cleaned_data.get('email')
            email.save()

    return HttpResponse(json.dumps({'success': sended}), mimetype="application/json")


## Send email after Get Free Quote form is
@csrf_protect
def send_freequote(request):
    from sanico.models import WaitContacEmail
    sended = False
    if request.method == 'POST':
        contact = GetFreeQuoteForm(request.POST)
        if contact.is_valid():
            sended = send_free_quote(contact.cleaned_data)
            email = WaitContacEmail()
            email.name = contact.cleaned_data.get('full_name')
            email.email = contact.cleaned_data.get('email')
            email.save()

    return HttpResponse(json.dumps({'success': sended}), mimetype="application/json")


@csrf_protect
def send_review(request, clinic_id):
    sended = False

    form = ReviewForm(request.POST, instance=Review())
    print "is form valid? ", form.is_valid()
    if form.is_valid():
        review = form.save()
        link = "%s&review=%d" % (link_code.generate_link('reviews', True, False), review.id)
        ok_link = "%s&yesno=1" % link
        cancel_link = "%s&yesno=0" % link

        sended = send_mail_after_save_review(
            #generate link to confirmate ok
            ok_link,
            #generate link to cancel
            cancel_link,
            #set instance
            review
        )

    return HttpResponse(json.dumps({'success': sended}), mimetype="application/json")


def test_email_review(request):
    review = Review.objects.get(id=11)
    return render_to_response(
        'emails/review-summary.html', {'review': review}, context_instance=RequestContext(request))
