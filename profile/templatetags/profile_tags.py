from django import template
from profile.models import ClinicPhoto, TreatmentPrice, OpeningHour, City, Clinic
register = template.Library()


@register.inclusion_tag('profile/tags/gallery.html', takes_context=True)
def render_gallery(context, clinic):
    return {
        'MEDIA_URL': context['MEDIA_URL'],
        'images': ClinicPhoto.objects.filter(clinic=clinic.id)
    }


@register.inclusion_tag('profile/tags/about_photos.html', takes_context=True)
def render_about_photos(context, clinic):
    return {
        'MEDIA_URL': context['MEDIA_URL'],
        'clinic': clinic,
        'images': ClinicPhoto.objects.filter(clinic=clinic.id, in_front=True)[:3]
    }


@register.inclusion_tag('profile/tags/prices.html')
def render_prices(clinic, treatment):
    return {
        'prices': TreatmentPrice.objects.filter(
            treatment=treatment.id, clinic=clinic.id)
    }


@register.inclusion_tag('profile/tags/profile_right_bar.html')
def render_profile_right_bar(clinic):
    return {
        'clinic': clinic,
        'days': OpeningHour.objects.filter(clinic=clinic.id)
    }


@register.inclusion_tag('profile/tags/image_map.html', takes_context=True)
def render_image_map(context, clinic):
    try:
        city = City.objects.get(name=clinic.city)
    except City.DoesNotExist:
        city = None

    return {
        'city': city,
        'MEDIA_URL': context.get('MEDIA_URL')
    }


@register.inclusion_tag("profile/tags/modified_log.html", takes_context=True)
def modified_log(context):
    return {'clinics': Clinic.objects.all().order_by('-modified')[:10]}
