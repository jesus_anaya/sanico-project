from django import forms
from mezzanine.core.forms import DynamicInlineAdminForm
from django.core.exceptions import ValidationError
from mezzanine.core.forms import TinyMceWidget
from .widgets import AdminImageWidget
from .models import Review, Clinic, ClinicProcedure
import datetime


class ClinicProcedureForm(forms.ModelForm):
    class Meta:
        model = ClinicProcedure

    def __init__(self, *args, **kwargs):
        super(ClinicProcedureForm, self).__init__(*args, **kwargs)
        self.fields['procedure'].required = False
        self.fields['position'].required = False

    description = forms.CharField(required=False)
    position = forms.IntegerField(required=False, initial=0)
    price = forms.CharField(required=False, initial="Undefined")
    duration = forms.CharField(required=False, initial="Undefined")


class DoctorForm(forms.ModelForm):
    """
    Doctor Form
    validate that the doctor resume will be a PDF or Doc file, and that the resume size
    is not greater that 4 MB.
    """
    photo = forms.ImageField(label='photo', widget=AdminImageWidget)
    resume = forms.FileField(required=False)

    def clean_resume(self):
        valid_formats = [
            'application/pdf', 'application/msword',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document']

        resume = self.cleaned_data.get('resume')

        if resume and hasattr(resume, 'content_type'):
            if not resume.content_type in valid_formats:
                raise ValidationError(u'Only *.pdf, *.doc or *.docx files are allowed.')

            if resume._size > 4 * 1024 * 1024:
                raise ValidationError("File too large ( > 4mb )")
        return resume


class ClinicForm(forms.ModelForm):
    class Meta:
        model = Clinic

    image_header = forms.ImageField(label='Image Header', widget=AdminImageWidget, required=False)
    image_profile = forms.ImageField(label='Image Profile', widget=AdminImageWidget, required=False)
    about = forms.CharField(widget=TinyMceWidget, required=False)
    open_since = forms.CharField(required=False)
    created = forms.DateTimeField(widget=forms.HiddenInput(), required=False)
    update_created = forms.BooleanField(initial=False, required=False)

    def clean(self):
        cleaned_data = super(ClinicForm, self).clean()
        update_created = cleaned_data.get('update_created')

        if update_created:
            cleaned_data['created'] = datetime.datetime.now()
        return cleaned_data


class ClinicPhotoForm(DynamicInlineAdminForm):
    photo = forms.ImageField(label='photo', widget=AdminImageWidget)


class LocationForm(forms.ModelForm):
    image_map_1 = forms.ImageField(
        label='Image map 1', widget=AdminImageWidget, required=False)
    image_map_2 = forms.ImageField(
        label='Image map 2', widget=AdminImageWidget, required=False)
    link = forms.CharField(required=True)


class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review

    datetime = forms.DateTimeField(required=False)
    enable = forms.BooleanField(required=False)
    treatment = forms.CharField(required=False)


class TreatmentPriceForm(forms.ModelForm):
    pass


class GetFreeQuoteForm(forms.Form):
    clinic = forms.CharField(required=False)
    full_name = forms.CharField()
    email = forms.EmailField()
    phone = forms.CharField()
    birth = forms.DateField(required=False)
    residence = forms.CharField(required=False)
    state = forms.CharField(required=False)
    desired_city_of_destination = forms.CharField(required=False)
    desire_appointment_date = forms.DateField()
    desire_appointment_time = forms.CharField()
    treatment = forms.CharField(required=False)
    best_to_contact = forms.CharField()
    find = forms.CharField(required=False)
    acquaintance = forms.CharField(required=False)
    comment = forms.CharField()
