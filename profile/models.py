# -*- encoding: utf-8 -*-
from django.db import models
from django.db.models import Avg
from django.template.defaultfilters import slugify
from django.contrib.sites.models import Site
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from mezzanine.core.fields import RichTextField
from sanico.helpers import upload_to, clinic_upload_to
from sanico.models import SanicoUser
from sanico import bitly
from .managers import ProfileSearchableManager, ProceduresManager, ClinicManager
import datetime


#################################################
#
#
#
#################################################
class Country(models.Model):
    """
    Here can be submit the counties list to use.
    """
    class Meta:
        verbose_name_plural = 'Countries'

    name = models.CharField(max_length=100)
    shortcut = models.CharField(max_length=5, help_text="Use 2 characters")

    def __unicode__(self):
        return unicode(self.name)


#################################################
#
#
#
#################################################
class City(models.Model):
    """
    City name
    """
    class Meta:
        verbose_name_plural = "Cities"

    country = models.ForeignKey('Country')
    name = models.CharField(max_length=100)
    map_image = models.ImageField(
        verbose_name="City Map", upload_to=upload_to("uploads/clinics/"), default="")

    def __unicode__(self):
        return unicode(self.name)


#################################################
#
#  Abstract Classes
#
#################################################
class Paiment(models.Model):
    """
    Paiment class in an abstract classes
    """
    class Meta:
        abstract = True

    personal_checks = models.BooleanField(default=False)
    traveler_checks = models.BooleanField(default=False)
    cash = models.BooleanField(default=False)
    mastercard = models.BooleanField(default=False)
    visa = models.BooleanField(default=False)
    american_express = models.BooleanField(default=False, verbose_name="American Express")
    discover = models.BooleanField(default=False)
    paypal = models.BooleanField(default=False)
    money_order = models.BooleanField(default=False)
    wire_transfer = models.BooleanField(default=False)
    cashier_check = models.BooleanField(default=False)


#################################################
#
#
#
#################################################
class Languages(models.Model):
    """
    Languages class in an abstract classes
    """
    class Meta:
        abstract = True

    english = models.BooleanField(default=False)
    spanish = models.BooleanField(default=False)
    french = models.BooleanField(default=False)
    japanese = models.BooleanField(default=False)
    italian = models.BooleanField(default=False)
    russian = models.BooleanField(default=False)
    mandarin = models.BooleanField(default=False)
    portuguese = models.BooleanField(default=False)
    german = models.BooleanField(default=False)
    other = models.CharField(max_length=20, blank=True, null=True)


#################################################
#
#
#
#################################################
class Review(models.Model):
    '''
    Clinic reviews
    '''
    class Meta:
        ordering = ['-datetime']

    clinic = models.ForeignKey('Clinic')
    quality = models.IntegerField(default=10)
    service = models.IntegerField(default=10)
    communication = models.IntegerField(default=10)
    price = models.IntegerField(default=10)
    location = models.IntegerField(default=10)
    qualification = models.IntegerField(default=0, blank=True)

    datetime = models.DateTimeField(
        auto_now_add=True, verbose_name="Register Date")

    for_testimonial = models.BooleanField(default=False)
    enable = models.BooleanField(default=False)

    first_name = models.CharField(max_length=60)
    last_name = models.CharField(max_length=60)
    email = models.EmailField()
    treatment = models.CharField(max_length=100, default="")
    date_appointment = models.DateField()
    city = models.CharField(
        max_length=60, verbose_name="City of Residence")

    country = models.CharField(
        max_length=60, verbose_name="Country of Residence")
    comment = models.TextField(verbose_name="Your Commments")

    def get_full_name(self):
        return u"%s %s" % (self.first_name, self.last_name)

    def save(self, *args, **kwargs):
        sub_total = self.quality
        sub_total += self.service
        sub_total += self.communication
        sub_total += self.price
        sub_total += self.location
        self.qualification = int(round(sub_total / 5))
        result = super(Review, self).save(*args, **kwargs)

        #calculate requests average for clinic
        if self.enable:
            self.save_clinic()

        return result

    def __prom(self, avg):
        try:
            return int(round(avg))
        except TypeError:
            return 0

    def get_url(self):
        api = bitly.Api(login='sanico', apikey='R_391663d2e2c1dc0b36f53fe9fa2ceaba')
        site = Site.objects.get_current()
        return api.shorten(u"https://%s/profile/%s/single-review/%d/" % (site, self.clinic.slug, self.id))

    def save_clinic(self):
        self.clinic.quality = self.__prom(Review.objects.filter(
            enable=True, clinic=self.clinic.id).aggregate(Avg('quality')).values()[0])

        self.clinic.service = self.__prom(Review.objects.filter(
            enable=True, clinic=self.clinic.id).aggregate(Avg('service')).values()[0])

        self.clinic.communication = self.__prom(Review.objects.filter(
            enable=True, clinic=self.clinic.id).aggregate(Avg('communication')).values()[0])

        self.clinic.price = self.__prom(Review.objects.filter(
            enable=True, clinic=self.clinic.id).aggregate(Avg('price')).values()[0])

        self.clinic.location = self.__prom(Review.objects.filter(
            enable=True, clinic=self.clinic.id).aggregate(Avg('location')).values()[0])

        self.clinic.qualification = self.__prom(Review.objects.filter(
            enable=True, clinic=self.clinic.id).aggregate(Avg('qualification')).values()[0])

        self.clinic.save()

    def __unicode__(self):
        return u"for %s, by %s %s" % (
            self.clinic.name, self.first_name, self.last_name)



#################################################
#
#
#
#################################################
class ClinicTag(models.Model):
    name = models.CharField(max_length=80)

    def __unicode__(self):
        return u"%s" % self.name


#################################################
#
#
#
#################################################
class ClinicCategory(models.Model):
    """
    This class invisible in admin list, an is only used into of Clinic panel.
    """

    name = models.CharField(max_length=100)
    tags = models.ManyToManyField(ClinicTag)

    def __unicode__(self):
        return unicode(self.name)


#################################################
#
#
#
#################################################
class Clinic(Paiment, Languages):
    """
    This is the main class, the clinic administration panel is based in this class
    and the rest of classes will use a foreign key point here.
    """
    class Meta:
        ordering = ['-extra_position', '-qualification', '-created']

    STATES = (
        (0, u"Aguascalientes"),
        (1, u"Baja California"),
        (2, u"Baja California Sur"),
        (3, u"Campeche"),
        (4, u"Chiapas"),
        (5, u"Chihuahua"),
        (6, u"Coahuila"),
        (7, u"Colima"),
        (8, u"Distrito Federal"),
        (9, u"Durango"),
        (10, u"Guanajuato"),
        (11, u"Guerrero"),
        (12, u"Hidalgo"),
        (13, u"Jalisco"),
        (14, u"Estado de México"),
        (15, u"Michoacán"),
        (16, u"Morelos"),
        (17, u"Nayarit"),
        (18, u"Nuevo León"),
        (19, u"Oaxaca"),
        (20, u"Puebla"),
        (21, u"Querétaro"),
        (22, u"Quintana Roo"),
        (23, u"San Luis Potosí"),
        (24, u"Sinaloa"),
        (25, u"Sonora"),
        (26, u"Tabasco"),
        (27, u"Tamaulipas"),
        (28, u"Tlaxcala"),
        (29, u"Veracruz"),
        (30, u"Yucatán"),
        (31, u"Zacatecas")
    )

    objects = ClinicManager()

    user = models.ForeignKey(SanicoUser, null=True)

    name = models.CharField(max_length=100, db_index=True)
    city = models.CharField(max_length=100, db_index=True)

    slug = models.CharField(max_length=150, default="")

    # Clinic Images
    image_header = models.ImageField(
        verbose_name="Image Header", upload_to=clinic_upload_to("uploads/clinics/"),
        default="uploads/images/clinic-default-cover.jpg", blank=True)
    image_profile = models.ImageField(
        verbose_name="Image Profile", upload_to=clinic_upload_to("uploads/clinics/"),
        default="uploads/images/clinic-default-profile.jpg", blank=True)

    # Category idexed as string
    category = models.ForeignKey('ClinicCategory')
    category_index = models.CharField(
        max_length=50, default="", db_index=True)

    # Legal info
    company_name = models.CharField(max_length=100, default="", verbose_name="Legal Name")
    rfc = models.CharField(max_length=30, default="", verbose_name="RFC")
    legal_address = models.CharField(
        max_length=200, default="", verbose_name="Address", blank=True)

    country = models.ForeignKey('Country')
    email = models.EmailField()
    phone = models.CharField(max_length=20)
    address = models.CharField(max_length=200)
    state = models.IntegerField(choices=STATES, default=1)
    zip_code = models.CharField(max_length=15, null=True)

    open_since = models.CharField(max_length=6, default="")
    about = RichTextField("About", default="")

    offer = models.CharField(max_length=200, default="")
    enable = models.BooleanField(default=True)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    # Extra ranking position
    extra_position = models.IntegerField(default=0, blank=True)

    # Search
    objects = ProfileSearchableManager()
    search_fields = {"name": 6, "category_index": 4, "city": 3}

    # Reviews data
    quality = models.IntegerField(default=0)
    service = models.IntegerField(default=0)
    communication = models.IntegerField(default=0)
    price = models.IntegerField(default=0)
    location = models.IntegerField(default=0)
    qualification = models.IntegerField(default=0)

    # Fiscal
    fiscal_first_name = models.CharField(
        max_length=60, default='', verbose_name="First name")
    fiscal_last_name = models.CharField(
        max_length=60, default='', verbose_name="Last name")
    fiscal_email = models.EmailField(
        default='', verbose_name="Email")
    fiscal_phone = models.CharField(
        max_length=30, default='', verbose_name="Phone")

    # Contact
    contact_first_name = models.CharField(
        max_length=60, default='', verbose_name="First name")
    contact_last_name = models.CharField(
        max_length=60, default='', verbose_name="Last name")
    contact_email = models.EmailField(
        default='', verbose_name="Email")
    contact_phone = models.CharField(
        max_length=30, default='', verbose_name="Phone")

    def get_fiscal_name(self):
        return "%s %s" % (self.fiscal_first_name, self.fiscal_last_name)

    def get_contact_name(self):
        return "%s %s" % (self.contact_first_name, self.contact_last_name)

    def get_testimonial(self):
        reviews = Review.objects.filter(
            clinic=self.id, enable=True, for_testimonial=True)
        if reviews.count() > 0:
            return (reviews[0].get_full_name(), reviews[0].comment)
        return ("", "")

    def get_state(self):
        return self.STATES[self.state][1]

    @models.permalink
    def get_absolute_url(self):
        return ('profile_about', [str(self.slug)])

    def __unicode__(self):
        return unicode("%s - %s, %s, %s" % (
            self.name, self.address, self.city, self.country))

    def get_quality(self):
        return self.quality

    def get_service(self):
        return self.service

    def get_communication(self):
        return self.communication

    def get_price(self):
        return self.price

    def get_location(self):
        return self.location

    def get_qualification(self):
        return self.qualification

    def save(self, *args, **kwargs):
        # update slug ever that clinic is mofified
        self.slug = slugify(self.name)
        self.category_index = slugify(unicode(self.category.name).replace('&', 'and'))
        super(Clinic, self).save(*args, **kwargs)


#################################################
#
#
#
#################################################
class Doctor(models.Model):
    class Meta:
        ordering = ['position']

    clinic = models.ForeignKey('Clinic')
    photo = models.ImageField(
        verbose_name="Photo",
        upload_to=upload_to("uploads/doctors/"), default="")
    first_name = models.CharField(max_length=40)
    last_name = models.CharField(max_length=50)
    education = models.CharField(max_length=100)
    specialisations = models.CharField(max_length=255, blank=True, null=True)
    languages = models.CharField(max_length=60)
    position = models.IntegerField(default=0)
    resume = models.FileField(upload_to="uploads/doctors/resumes/", null=True, blank=True)

    def __unicode__(self):
        return unicode("%s %s" % (self.first_name, self.last_name))

    def name(self):
        return "%s %s" % (self.first_name, self.last_name)


#################################################
#
#
#
#################################################
class Treatment(models.Model):
    class Meta:
        ordering = ['position']

    clinic = models.ForeignKey('Clinic', null=True)
    name = models.CharField(max_length=100, default="")
    position = models.IntegerField(default=0)

    def __unicode__(self):
        return unicode(self.name)


#################################################
#
#
#
#################################################

class TreatmentPrice(models.Model):
    class Meta:
        ordering = ['-position']

    clinic = models.ForeignKey('Clinic', null=True)
    treatment = models.ForeignKey('Treatment', null=True)
    procedure = models.CharField(max_length=100)
    price = models.CharField(max_length=25)
    duration = models.CharField(max_length=60)
    position = models.IntegerField(default=0)


#################################################
#
# Clinic photo added to gallery for each clinic
#
#################################################
class ClinicPhoto(models.Model):
    clinic = models.ForeignKey('Clinic', null=True)
    photo = models.ImageField(upload_to=upload_to("uploads/clinics/galleries/"))
    in_front = models.BooleanField(default=False, verbose_name="Show in Front")

    def delete(self, *args, **kwargs):
        self.photo.delete()
        super(ClinicPhoto, self).delete(*args, **kwargs)


###############################################
#
# List of hourly relationship with the clinic
#
###############################################
class OpeningHour(models.Model):
    """
    Spesific the clinic hours at the week
    """
    class Meta:
        ordering = ["id"]

    # use a tuples array as day code
    DAYS = (
        (0, "Monday"),
        (1, "Tuesday"),
        (2, "Wednesday"),
        (3, "Thursday"),
        (4, "Friday"),
        (5, "Saturday"),
        (6, "Sunday")
    )

    clinic = models.ForeignKey('Clinic')
    day = models.IntegerField(choices=DAYS)
    hour_open = models.TimeField(null=True, blank=True)
    hour_close = models.TimeField(null=True, blank=True)

    def day_name(self):
        return self.DAYS[self.day][1]


######################################################################
#
# Add location using google map link using the coords of the clinic
#
######################################################################

class Location(models.Model):
    clinic = models.ForeignKey('Clinic', related_name='+')
    link = models.CharField(max_length=500, default="")
    coords = models.CharField(max_length=40, default="")
    image_map_1 = models.ImageField(
        upload_to=upload_to("uploads/clinics/locations/"), blank=True, null=True)
    image_map_2 = models.ImageField(
        upload_to=upload_to("uploads/clinics/locations/"), blank=True, null=True)

    def save(self, *args, **kwargs):
        try:
            coords = str(self.link).split('ll=')[1].split('&')[0]
            print coords
            self.coords = coords
        except:
            print "Error read link"
        super(Location, self).save(*args, **kwargs)


###################################################
#
# Complementary services added into clinic about
#
###################################################

class ComplementaryService(models.Model):
    clinic = models.ForeignKey(Clinic)
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return unicode(self.name)


################################################
#
# List of special promotion into clinic about
#
################################################

class SpecialPromotion(models.Model):
    clinic = models.ForeignKey(Clinic)
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return unicode(self.name)


#################################################
#
# Clinic Relation Treatment
#
#################################################

class ClinicRelationTreatment(models.Model):
    class Meta:
        ordering = ['position']

    clinic = models.ForeignKey(Clinic)
    category = models.ForeignKey("TreatmentCategory")
    position = models.IntegerField(default=0)

    def __unicode__(self):
        return u"%s - %s" % (self.clinic, self.category)


#################################################
#
# New treatments mode
#
#################################################
class TreatmentCategory(models.Model):
    class Meta:
        ordering = ['name']

    clinic_category = models.ForeignKey(ClinicCategory)
    name = models.CharField(max_length=100, default="")

    def __unicode__(self):
        return u"%s" % self.name


#################################################
#
# Procedures list
#
#################################################
class Procedure(models.Model):
    class Meta:
        ordering = ['name']

    category = models.ForeignKey(TreatmentCategory)
    name = models.CharField(max_length=255, default="")

    objects = ProceduresManager()

    def __unicode__(self):
        return u"%s" % self.name

    def category_name(self):
        return u"%s" % self.name


######################################################
#
# Relationshipt between clinic procedures and clinics
#
######################################################
class ClinicProcedure(models.Model):
    class Meta:
        ordering = ["position"]

    clinic = models.ForeignKey(Clinic)
    description = models.CharField(max_length=200, default="", blank=True)
    procedure = models.ForeignKey(Procedure, blank=True)
    price = models.CharField(max_length=40, default="Undefined")
    duration = models.CharField(max_length=60, default="Undefined")
    position = models.IntegerField(default=0)

    def __unicode__(self):
        return u"Procedure: '%s'" % self.procedure.name

    def procedure_id(self):
        return self.procedure.id

    def get_category(self):
        return self.procedure.category.id

    def save(self, *args, **kwargs):
        # assign the new position as last position
        if not self.id:
            self.position = ClinicProcedure.objects.filter(
                    procedure__category=self.procedure.category,
                    clinic=self.clinic).count() + 1

        super(ClinicProcedure, self).save(*args, **kwargs)


@receiver(post_save, sender=ClinicProcedure)
def save_procedure(sender, instance, **kwargs):
    ClinicRelationTreatment.objects.get_or_create(clinic=instance.clinic,
                                    category=instance.procedure.category)


@receiver(post_delete, sender=ClinicProcedure)
def delete_procedure(sender, instance, **kwargs):
    if not ClinicProcedure.objects.filter(
        procedure__category=instance.procedure.category.id).exists():
        try:
            ClinicRelationTreatment.objects.get(clinic=instance.clinic,
                        category=instance.procedure.category.id).delete()

        except ClinicRelationTreatment.DoesNotExist:
            print ("Error deleting RelationTreatment %s" % instance.clinic)

