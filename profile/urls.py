from django.conf.urls import patterns, url
from mezzanine.core.views import direct_to_template

urlpatterns = patterns(
    "profile.views",
    url(r"^(?P<slug>.*)/contact/$", 'contact_clinic', name='profile_contact'),
    url(r"^(?P<slug>.*)/doctors/$", 'doctors', name='profile_doctors'),
    url(r"^(?P<slug>.*)/gallery/$", 'gallery', name='profile_gallery'),
    url(r"^(?P<slug>.*)/location/$", 'location', name='profile_location'),
    url(r"^(?P<slug>.*)/prices/$", 'prices', name='profile_prices'),
    url(r"^(?P<slug>.*)/reviews/$", 'reviews', name='profile_reviews'),
    url(r"^(?P<slug>.*)/single-review/(?P<id>\d+)/$", 'single_review', name='profile_single_review'),
    url(r"^(?P<slug>.*)/write-review/$", 'write_review', name='profile_write_review'),
    url(r"^(?P<slug>.*)/reviews-list/$", 'reviews_list', name='profile_reviews_list'),
    url(r"^get-about/(?P<clinic_id>.*)/$", 'get_about', name='profile_get_about'),
    url(r"^send-review/(?P<clinic_id>.*)/$", 'send_review', name='profile_send_review'),
    url(r"^send-contact/$", 'send_contact', name='profile_send_contact'),
    url(r"^send-free-quote/$", 'send_freequote', name='send_free_quote'),
    url(r"^test-email-review/$", 'test_email_review', name='test_email_review'),
    url(r"^contact-thanks/$", "contact_thanks", name='contact_thanks'),
    url(r"^review-thanks/$", "review_thanks", name='review_thanks'),

    url(r"^confirmation-review/$", direct_to_template, {
        'template': 'profile/confirmation_review.html'}, name='profile_confirmation_review'),

    url(r"^(?P<slug>.*)/$", 'about', name='profile_about'),
)
