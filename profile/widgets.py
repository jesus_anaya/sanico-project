from django import forms
from django.forms.widgets import Select
from django.utils.safestring import mark_safe
from django.forms.util import flatatt


class AdminImageWidget(forms.FileInput):
    """
    A ImageField Widget for admin that shows a thumbnail.
    """

    def __init__(self, attrs={}):
        super(AdminImageWidget, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        output = []
        if value and hasattr(value, "url"):
            output.append(('<a target="_blank" href="%s">'
                           '<img src="%s" style="width: 80px;" /></a> '
                           % (value.url, value.url)))
        output.append(super(AdminImageWidget, self).render(name, value, attrs))
        return mark_safe(u''.join(output))


class SelectWithGroup(Select):

    def __init__(self, attrs=None, fieldset_choices=()):
        super(Select, self).__init__(attrs)
        self.fieldset_choices = list(fieldset_choices)

    def render(self, name, value, attrs=None, choices=()):
        from django.utils.html import escape
        from django.utils.encoding import smart_unicode
        if value is None: value = ''
        final_attrs = self.build_attrs(attrs, name=name)

        print "attrs", attrs

        output = [u'<select%s>' % flatatt(final_attrs)]

        # Init selecttion
        output.append(u'<option value="">-- --</option>')

        str_value = smart_unicode(value)

        print "str_value: ", str_value

        for group_label, group in self.fieldset_choices:
            if group_label: # should belong to an optgroup
                group_label = u"%s" % (group_label)
                output.append(u'<optgroup label="%s">' % group_label)

            for option_value, option_label in group:
                selected_html = (str(option_value) == str(str_value)) and u' selected="selected"' or ''
                output.append(u'<option value="%s"%s>%s</option>' % (
                    escape(option_value), selected_html, escape(option_label)))
            if group_label:
                output.append(u'</optgroup>')

        output.append(u'</select>')

        return mark_safe(u'\n'.join(output))
    render.allow_tags = True
