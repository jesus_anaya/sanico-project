from mezzanine.core.managers import SearchableManager
from django.db.models import Manager


class ProfileSearchableManager(SearchableManager):

    def search(self, where, *args, **kwargs):
        """
        Proxy to queryset's search method for the manager's model and
        any models that subclass from this manager's model if the
        model is abstract.
        """

        top_results = []
        normal_results = []
        queryset = self.model.objects.get_query_set()
        queryset_top = queryset.filter(
            city__icontains=where, enable=True, extra_position__gt=0)
        queryset_normal = queryset.filter(city__icontains=where, enable=True)

        top_results.extend(queryset_top.search(*args, **kwargs)[:5])
        top_results = sorted(top_results, key=lambda r: r.result_count, reverse=True)

        normal_results.extend(queryset_normal.search(*args, **kwargs))
        normal_results = [x for x in normal_results if x not in top_results]
        normal_results = sorted(normal_results, key=lambda r: r.result_count, reverse=True)

        return top_results + normal_results


class ProceduresManager(SearchableManager):
    """
    """
    def get_for_clinic(self, *args, **kwargs):
        return self.all()


class ClinicManager(Manager):
    """
    """
    def enables(self, *args, **kwargs):
        return self.filter(enable=True)