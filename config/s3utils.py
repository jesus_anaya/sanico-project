from storages.backends.s3boto import S3BotoStorage
from filebrowser_safe.storage import S3BotoStorageMixin
from django.core.files.storage import get_storage_class


class S3BotoStorageMezzanine(S3BotoStorage, S3BotoStorageMixin):
    """
    S3 data storage compatibility with Filebrowser_safe of Mezzanine.
    Necessary methods for s3 was added in S3BotoStorageMixin, and the
    mixin is included as extension of S3BotoStorage.
    """
    pass


class CachedS3BotoStorage(S3BotoStorageMezzanine):
    """
    Compress data s3 storage configuration.
    S3 storage backend that saves the files locally, too.
    """
    def __init__(self, *args, **kwargs):
        super(CachedS3BotoStorage, self).__init__(*args, **kwargs)
        self.local_storage = get_storage_class(
            "compressor.storage.CompressorFileStorage")()

    def save(self, name, content):
        name = super(CachedS3BotoStorage, self).save(name, content)
        self.local_storage._save(name, content)
        return name


CachedS3BotoStorageStatic = lambda: CachedS3BotoStorage(location='static')
MediaS3BotoStorage = lambda: S3BotoStorageMezzanine(location='media')
