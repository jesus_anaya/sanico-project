from django.core.files.storage import get_storage_class
from config.s3utils import S3BotoStorageSafe

class CachedS3BotoStorage(S3BotoStorageSafe):
    """
    Compress data s3 storage configuration.
    S3 storage backend that saves the files locally, too.
    """
    def __init__(self, *args, **kwargs):
        super(CachedS3BotoStorage, self).__init__(*args, **kwargs)
        self.local_storage = get_storage_class(
            "compressor.storage.CompressorFileStorage")()

    def save(self, name, content):
        name = super(CachedS3BotoStorage, self).save(name, content)
        self.local_storage._save(name, content)
        return name


CachedS3BotoStorageStatic = lambda: CachedS3BotoStorage(location='static')
