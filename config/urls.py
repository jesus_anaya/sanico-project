
from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from mezzanine.urls import urlpatterns as mezzanine_urlpatterns
admin.autodiscover()

urlpatterns = patterns(
    "",
    url("^$", "sanico.views.home", name="home"),

    # Change the admin prefix here to use an alternate URL for the
    # admin interface, which would be marginally more secure.
    url("^admin/", include(admin.site.urls)),
    url("^widget/", include("widget.urls")),
    url("^validate/", include("libs.validationlink.urls", namespace='validation')),
    url("^formview/", include("libs.formview.urls", namespace='formview')),

    url("^profile/", include("profile.urls")),
    url("^collaborator/", include("collaborator.urls", namespace="collaborator")),
    url("^podcast/", include("podcast.urls")),

    # Django API RESTful Framework
    url("^api/", include("api.urls")),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    # Sanico app URLS
    url("^", include('sanico.urls')),

    # Mezzanine's apps urls
    url("^", include(mezzanine_urlpatterns)),
)

if settings.DEBUG:
    urlpatterns += patterns(
        '',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    )

handler404 = "mezzanine.core.views.page_not_found"
handler500 = "mezzanine.core.views.server_error"
