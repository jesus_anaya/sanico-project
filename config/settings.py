
######################
#  SaniCo Settings   #
######################

# Controls the ordering and grouping of the admin menu.
#
ADMIN_MENU_ORDER = (
    ("Content", (
            "pages.Page",
            "blog.BlogCategory",
            "blog.BlogPost",
            "podcast.Podcast",
            "generic.ThreadedComment",
            ("Media Library", "fb_browse"),
            "widget.Widget",
            "sanico.Newsletter",
            "sanico.Sponsor",
        )
    ),
    ("Site", (
            "sites.Site",
            "redirects.Redirect",
            "conf.Setting"
        )
    ),
    ("Profiles", (
            "profile.Clinic",
            "profile.ClinicCategory",
            "sanico.SanicoUser",
            "profile.Country",
            "profile.City",
            "profile.Review",
            "profile.Procedure",
            "profile.TreatmentCategory"
        )
    ),
    ("Users", ("auth.User", "auth.Group",)),
)

# A three item sequence, each containing a sequence of template tags
# used to render the admin dashboard.
#
DASHBOARD_TAGS = (
    ("blog_tags.quick_blog", "mezzanine_tags.app_list"),
    ("comment_tags.recent_comments",),
    ("profile_tags.modified_log",),
    ("mezzanine_tags.recent_actions",),
)

EXTRA_MODEL_FIELDS = (
    (
        "mezzanine.blog.models.BlogPost.image",
        "sanico.fields.PostImageField",
        ("Image",),
        {"blank": True, "upload_to": "uploads/blog/", "default": ""},
    ),
    (
        "mezzanine.blog.models.BlogPost.image_description",
        "CharField",
        ("Image Description",),
        {"blank": True, "null": True, "max_length": 150},
    ),
)

ALLOWED_HOSTS = [
    'sanimedicaltourism.com',
    'www.sanimedicaltourism.com',
    'sanico.herokuapp.com',
    'sanicodev.herokuapp.com',
    'deploy.sanimedicaltourism.com',
]

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

WINDOWS_DEV = False
LOCAL_MODE = False

#APPEND_SLASH = False

#Home Video
VIDEO_HOME_AUTOPLAY = 1

SOUTH_AUTO_FREEZE_APP = True

GRAPPELLI_ADMIN_TITLE = "Sanico Admin System"

# Setting to turn on featured images for blog posts. Defaults to False.
#
#BLOG_SLUG = "blog"
BLOG_USE_FEATURED_IMAGE = False

# If True, the south application will be automatically added to the
# INSTALLED_APPS setting.
USE_SOUTH = True

########################
# MAIN DJANGO SETTINGS #
########################
ADMINS = (
    #('Jesus Anaya', 'jesus.anaya.dev@gmail.com'),
    ('Develop Sanico', 'developer@sanimedicaltourism.com'),
)

MANAGERS = ADMINS

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
TIME_ZONE = "America/Tijuana"

# If you set this to True, Django will use timezone-aware datetimes.
USE_TZ = True

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = "en"

# A boolean that turns on/off debug mode. When set to ``True``, stack traces
# are displayed for error pages. Should always be set to ``False`` in
# production. Best set to ``True`` in local_settings.py
DEBUG = True

# Whether a user's session cookie expires when the Web browser is closed.
SESSION_EXPIRE_AT_BROWSER_CLOSE = True

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

# Make this unique, and don't share it with anybody.
SECRET_KEY = "31bcd452-c4c8-41b8-bec5-031d91c6b88095010871-89c9-452b-9638-037511f19f49c32247d2-6995-452b-8a23-7983ec49e1ae"

# Never cache key
NEVERCACHE_KEY = "52b-9638-037511f19f49c32247d2-6995-452b-8a23-7983ec49e1ae-31bcd452v"

# Tuple of IP addresses, as strings, that:
#   * See debug comments, when DEBUG is true
#   * Receive x-headers
INTERNAL_IPS = ("127.0.0.1",)

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    "django.template.loaders.filesystem.Loader",
    "django.template.loaders.app_directories.Loader",
)

AUTHENTICATION_BACKENDS = ("mezzanine.core.auth_backends.MezzanineBackend",)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    #'django.contrib.staticfiles.finders.DefaultStorageFinder',
    "compressor.finders.CompressorFinder",
)

import os
#############
# DATABASES #
#############
#
##Parse database configuration from $DATABASE_URL
try:
    import dj_database_url
    DATABASES = {
        'default': dj_database_url.config()
    }
except:
    print "Warrning: not importing dj_database_url"


#########
# PATHS #
#########

# Full filesystem path to the project.
PROJECT_ROOT = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..")


# Every cache key will get prefixed with this value - here we set it to
# the name of the directory the project is in to try and use something
# project specific.
CACHE_MIDDLEWARE_KEY_PREFIX = "sanico_project"
CACHE_MIDDLEWARE_SECONDS = 600

# URL prefix for static files.
STATIC_URL = "/static/"

# Absolute path to the directory static files should be collected to.
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static_local/')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
MEDIA_URL = "/media/"

# Absolute filesystem path to the directory that will hold user-uploaded files.
MEDIA_ROOT = os.path.join(PROJECT_ROOT, "media/")

# Package/module name to import the root urlpatterns from for the project.
ROOT_URLCONF = "config.urls"

TEMPLATE_DIRS = (os.path.join(PROJECT_ROOT, "templates"),)

STATICFILES_DIRS = (os.path.join(PROJECT_ROOT, 'static/'),)

# Use Users Profile
AUTH_PROFILE_MODULE = "sanico.UserProfile"
ACCOUNTS_VERIFICATION_REQUIRED = True

# Login and Logout urls
LOGIN_URL = "/login/"
LOGOUT_URL = LOGIN_URL

################
# APPLICATIONS #
################

PYTHON_APPS = (
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.redirects",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.sitemaps",
    "django.contrib.staticfiles",
    "mezzanine.boot",
    "mezzanine.conf",
    "mezzanine.core",
    "mezzanine.generic",
    "mezzanine.blog",
    "mezzanine.forms",
    "mezzanine.pages",
    "mezzanine.galleries",
    "mezzanine.twitter",
    #"mezzanine.accounts",
    #"mezzanine.mobile",
    "storages",
    "rest_framework",
    "embed_video",
    "widget",
)

LOCAL_APPS = (
    "sanico",
    "api",
    "podcast",
    "profile",
    "collaborator",
)

LIBS_APPS = (
    "libs.admin_tabs",
    "libs.validationlink",
    "libs.email_admin",
)

INSTALLED_APPS = PYTHON_APPS + LOCAL_APPS + LIBS_APPS

# List of processors used by RequestContext to populate the context.
# Each one should be a callable that takes the request object as its
# only parameter and returns a dictionary to add to the context.
TEMPLATE_CONTEXT_PROCESSORS = (
    "sanico.context_processors.user_profile",
    "django.contrib.auth.context_processors.auth",
    "django.contrib.messages.context_processors.messages",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.static",
    "django.core.context_processors.media",
    "django.core.context_processors.request",
    "django.core.context_processors.tz",
    "mezzanine.conf.context_processors.settings",
)

# List of middleware classes to use. Order is important; in the request phase,
# these middleware classes will be applied in the order given, and in the
# response phase the middleware will be applied in reverse order.
MIDDLEWARE_CLASSES = (
    "sanico.middleware.WWWRedirectMiddleware",
    "sanico.middleware.SearchReferrerMiddleware",
    "mezzanine.core.middleware.UpdateCacheMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "mezzanine.core.request.CurrentRequestMiddleware",
    "mezzanine.core.middleware.RedirectFallbackMiddleware",
    "mezzanine.core.middleware.TemplateForDeviceMiddleware",
    "mezzanine.core.middleware.TemplateForHostMiddleware",
    "mezzanine.core.middleware.AdminLoginInterfaceSelectorMiddleware",
    "mezzanine.core.middleware.SitePermissionMiddleware",
    # Uncomment the following if using any of the SSL settings:
    # "mezzanine.core.middleware.SSLRedirectMiddleware",
    "mezzanine.pages.middleware.PageMiddleware",
    "mezzanine.core.middleware.FetchFromCacheMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
)

# Django Rest Framework Settings
REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',),
    #'PAGINATE_BY': 10,
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',),
}

X_FRAME_OPTIONS = 'ALLOW-FROM www.youtube.com youtube.com'

# Store these package names here as they may change in the future since
# at the moment we are using custom forks of them.
PACKAGE_NAME_FILEBROWSER = "filebrowser_safe"
PACKAGE_NAME_GRAPPELLI = "grappelli_safe"

#########################
# OPTIONAL APPLICATIONS #
#########################

# These will be added to ``INSTALLED_APPS``, only if available.
OPTIONAL_APPS = (
    "debug_toolbar",
    "django_extensions",
    "compressor",
    PACKAGE_NAME_FILEBROWSER,
    PACKAGE_NAME_GRAPPELLI,
)

###################
# EMAIL SETTINGS  #
###################

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_TLS = True
EMAIL_HOST = 'gator4080.hostgator.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'noreply@sanimedicaltourism.com'
EMAIL_HOST_PASSWORD = '5},Qo4_XGc)s'
DEFAULT_FROM_EMAIL = 'info@sanimedicaltourism.com'

EMAI_CONTACT = ["marketing@sanimedicaltourism.com"]
EMAI_CONTACT_COPY = ["promotion@sanimedicaltourism.com"]
EMAI_CONTACT_BCC = []
EMAIL_DEBUG_CONTACT = ["killeranaya@gmail.com"]
EMAIL_COMMUNITY_MANAGER = ["community@sanimedicaltourism.com"]
EMAIL_INFORMATION = ["info@sanimedicaltourism.com"]

#server email, administrator
SERVER_EMAIL = EMAIL_HOST_USER

COMMENTS_REMOVED_VISIBLE = False
COMMENTS_UNAPPROVED_VISIBLE = False
COMMENTS_NOTIFICATION_EMAILS = ",".join(EMAI_CONTACT + EMAI_CONTACT_COPY + ['developer@sanimedicaltourism.com'])

##################
# LOCAL SETTINGS #
##################

# Allow any settings to be defined in local_settings.py which should be
# ignored in your version control system allowing for settings to be
# defined per machine.
try:
    from local_settings import *
except ImportError:
    pass

if not WINDOWS_DEV:
    OPTIONAL_APPS = (
        "debug_toolbar",
        "django_extensions",
        "compressor",
        PACKAGE_NAME_FILEBROWSER,
        PACKAGE_NAME_GRAPPELLI,
        "gunicorn",
    )

DEBUG_TOOLBAR_CONFIG = {"INTERCEPT_REDIRECTS": False}


# if runs in production mode, use S3 configurations
if not LOCAL_MODE:
    try:
        from .storage_s3_settings import *
    except ImportError:
        pass

# read local settings in develop mode
try:
    from .local_settings import *
except ImportError:
    pass

#########
# Memcached settings
#########

#import pylibmc
#
## Connect to memcache.
if not WINDOWS_DEV:
    CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
            'LOCATION': '127.0.0.1:11211',
        }
    }

    CACHE_ANONYMOUS_ONLY = True
    CACHE_SET_DELAY_SECONDS = (60 * 10)


# Copressor settings
COMPRESS_ENABLED = False
# if LOCAL_MODE else True
COMPRESS_URL = STATIC_URL
COMPRESS_ROOT = STATIC_ROOT
COMPRESS_OFFLINE = False
#COMPRESS_OFFLINE_MANIFEST = "manifest.json"     # Is created in CACHE directory
#COMPRESS_STORAGE = 'config.s3utils.CachedS3BotoStorageStatic'

ADMIN_MEDIA_PREFIX = STATIC_URL + "grappelli/"

RICHTEXT_FILTER_LEVEL = 3
RICHTEXT_WIDGET_CLASS = 'sanico.forms.TinyMceWidget'


# A sequence of templates used by the ``page_menu`` template tag. Each
# item in the sequence is a three item sequence, containing a unique ID
# for the template, a label for the template, and the template path.
# These templates are then available for selection when editing which
# menus a page should appear in. Note that if a menu template is used
# that doesn't appear in this setting, all pages will appear in it.

# PAGE_MENU_TEMPLATES = (
#     (1, "Top navigation bar", "pages/menus/dropdown.html"),
#     (2, "Left-hand tree", "pages/menus/tree.html"),
#     (3, "Footer", "pages/menus/footer.html"),
# )

####################
# DYNAMIC SETTINGS #
####################

# set_dynamic_settings() will rewrite globals based on what has been
# defined so far, in order to provide some better defaults where
# applicable. We also allow this settings module to be imported
# without Mezzanine installed, as the case may be when using the
# fabfile, where setting the dynamic settings below isn't strictly
# required.

try:
    from mezzanine.utils.conf import set_dynamic_settings
except ImportError:
    pass
else:
    set_dynamic_settings(globals())
